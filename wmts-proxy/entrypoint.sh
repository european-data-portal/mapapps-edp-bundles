#!/bin/sh -e

if [ -z "${WMTS_URL}" ]; then
  echo 'No $WMTS_URL is given' >&2
  exit 1
fi

if [ -z "${EXTERNAL_URL}" ]; then
  echo 'No $EXTERNAL_URL is given' >&2
  exit 1
fi
CAPABILITIES_PATH=$(echo ${WMTS_URL} | sed -r 's|https?://[^/]+||i')

sed -e "s|%%EXTERNAL_URL%%|${EXTERNAL_URL}|g" -i /srv/capabilities.xml

sed -e "s|%%CAPABILITIES_PATH%%|${CAPABILITIES_PATH}|g" \
    -e "s|%%WMTS_URL%%|${WMTS_URL}|g" \
    -i /etc/nginx/conf.d/default.conf

exec nginx -g "daemon off;"