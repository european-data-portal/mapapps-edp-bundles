
#!/bin/bash

install_file() {
  local groupId=$1
  local artifactId=$2
  local version=$3
  local classifier="$4"

  if [ -n "$classifier" ]; then
    mvn -q install:install-file -Dfile="$(pwd)/${artifactId}-${version}-${classifier}.jar" \
      -DgroupId=${groupId} -DartifactId=${artifactId} -Dversion=${version} -Dclassifier=${classifier} -Dpackaging=jar
  else
    mvn -q install:install-file -Dfile="$(pwd)/${artifactId}-${version}.jar" \
      -DgroupId=${groupId} -DartifactId=${artifactId} -Dversion=${version} -Dpackaging=jar
  fi

}

install_file org.dojotoolkit put-selector 0.3.6
install_file org.dojotoolkit xstyle 0.3.2 
install_file org.dojotoolkit dgrid 0.3.17 
install_file org.dojotoolkit dstore 1.1.2
install_file org.dojotoolkit dojo-release 1.13.0-1
install_file de.conterra.mapapps ct-sdi-extension 4.0.3
install_file de.conterra.mapapps ct-sdi-extension 4.0.3 src
install_file de.conterra.mapapps ct-mapapps-js-api 3.10.0
install_file de.conterra.mapapps ct-mapapps-js-api 3.10.0 src
install_file de.conterra.mapapps ct-mapapps-js-api 4.5.0
install_file de.conterra.mapapps ct-mapapps-js-api 4.5.0 src