define([
  "dojo/_base/declare",
  "dojo/_base/Deferred",
  "dojo/dom-construct",
  "dojo/dom-style",
  "dojo/dom-class",
  "dojo/dom",
  "dojo/DeferredList",
  "esri/dijit/Legend",
  "dojo/_base/array",
  "dojo/_base/lang",
  "dojo/_base/connect",
  "dojo/query",
  "ct/_lang",
  "ct/_Connect",
  "ct/array"
], function(
  declare,
  Deferred,
  dom_construct,
  dom_style,
  dom_class,
  dom,
  DeferredList,
  EsriLegend,
  d_array,
  d_lang,
  d_connect,
  query,
  ct_lang,
  _Connect,
  ct_array
) {
  /*
   * COPYRIGHT 2011 con terra GmbH Germany
   */
  return declare([EsriLegend], {
    // expected to be provided
    mapModel: null,

    _layerCheckboxes: {},

    _layerLegendImageUrls: {},

    _started: false,

    _currentLegendState: null,

    _alreadyInitialised: false,

    /*
     * Patched version of CT legend widget
     */
    constructor: function() {
      this._listeners = new _Connect({
        defaultConnectScope: this
      });
      this.inherited(arguments);
    },

    /*
     * Patched version of CT legend widget
     */
    startup: function() {
      if (!this._started) {
        // ensure that layer state changes are reflected in the legend
        this.connect(
          this.mapModel,
          "onModelNodeStateChanged",
          "_refreshOnNodeStateChange"
        );
        this.connect(
          this.map,
          "onLayerAdd",
          "_refreshOnLayerAdd"
        );
        this._started = true;
      }
      this.inherited(arguments);
    },
    _refreshOnLayerAdd: function() {
      this.refresh();
    },
    _refreshOnNodeStateChange: function(event) {
      this.refresh();
    },
    /**
     * This patches the orginial method and extend it to support the showBaseLayer flag.
     */
    _isSupportedLayerType: function(layer) {
      return this.inherited(arguments) && this._showLayerInLegend(layer);
    },
    _showLayerInLegend: function(layer) {
      var node = this.mapModel.getNodeById(layer.__managed);
      var showInLegend = node && node.get("showInLegend");
      return (
        (showInLegend || showInLegend === undefined) &&
        (this.showBaseLayer ||
          (!this._isBaseLayer(layer) && !node.get("isBaseLayerReference")))
      );
    },
    _isBaseLayer: function(esriLayer) {
      var mapModel = this.mapModel;
      // Note: here we access the __managed flag of the esriLayer, which is added by the EsriLayerManager Property
      // it contains the id of the model node
      return (
        mapModel.getServiceNodes(
          mapModel.getBaseLayer(),
          function(baseLayerNode) {
            return (
              esriLayer.__managed &&
              baseLayerNode.get("id") === esriLayer.__managed
            );
          },
          this
        ).length > 0
      );
    },
    /**
     * Patch original method, to ensure that it allways returns a deferred.
     */
    _legendRequest: function() {
      var d = this.inherited(arguments);
      if (!d) {
        d = new Deferred();
        d.resolve(true);
      }
      return d;
    },
    _processCachedLayers: function() {
      if (this._cachedLayers) {
        d_array.forEach(
          this.layers,
          function(layer) {
            var inside = false;
            d_array.forEach(
              this._cachedLayers,
              function(cachedLayer) {
                if (layer.__managed === cachedLayer.__managed) {
                  inside = true;
                }
              },
              this
            );
            if (!inside) {
              this._cachedLayers.push(layer);
            }
          },
          this
        );
        this.layers = this._cachedLayers;
      }
    },
    _startLegend: function() {
      dom_style.set(this.domNode, "position", "relative");
      dom_construct.create(
        "div",
        {
          id: this.id + "_msg",
          className: "esriLegendMsg",
          innerHTML: this.NLS_creatingLegend + "..."
        },
        this.domNode
      );
    },
    _endLegend: function(entries, addedLayer) {
      if (entries.length === 0 && !addedLayer) {
        (dom.byId(this.id + "_msg").innerHTML = this.NLS_noLegend),
          this._activate();
      } else {
        new DeferredList(entries).addCallback(
          d_lang.hitch(this, function(b) {
            if (addedLayer) {
              dom.byId(this.id + "_msg").innerHTML = "";
            } else {
              dom.byId(this.id + "_msg").innerHTML = this.NLS_noLegend;
            }
            this._activate();
          })
        );
      }
    },
    /*
     * replacement for ESRI method. we build our own markup..!
     */
    _createLegend: function() {
      this._injectCheckeableLayers();
    },
    _injectCheckeableLayers: function() {
      this._clearEvents();

      var self = this;
      var legendFirstChildDiv = query(".ecLegend > *");

      if (!legendFirstChildDiv || legendFirstChildDiv.length === 0) {
        console.warn("no .ecLegend div!");
        return;
      }

      dom_construct.empty(legendFirstChildDiv[0]);

      var candidates = this.mapModel.getOperationalLayer();
      if (candidates.children) {
        candidates.children.forEach(function(layer) {
          var id = layer.id || layer.title;
          var newInner = dom_construct.create("div", {
            class: "ec-legend-entry esriLegendService"
          });

          if (!self._layerCheckboxes[id]) {
            self._layerCheckboxes[id] = "enabled";
            dom_class.add(newInner, "layerVisible");
          }
          if (
            self._layerCheckboxes[id] &&
            self._layerCheckboxes[id] === "enabled"
          ) {
            dom_class.add(newInner, "layerVisible");
          }

          var labelId = id + "_active";
          var checkBoxWithLabel = dom_construct.create("div", {
            class: "layerCheckbox",
            id: labelId
          });
          var label = dom_construct.create("label", {
            for: labelId,
            innerHTML: layer.title
          });
          var labelDiv = dom_construct.create("div");
          labelDiv.appendChild(checkBoxWithLabel);
          labelDiv.appendChild(label);

          var imageDiv = dom_construct.create("div");
          var legendImgUrl =
            self._layerLegendImageUrls[id] || self._resolveLegendUrl(id);
          if (legendImgUrl) {
            var legendImg;
            if (legendImgUrl !== "no-legend") {
              legendImg = dom_construct.create("img", { src: legendImgUrl });

              //store the legend as esri.Legend will remove the layer from this.layers
              //if it not visible
              self._layerLegendImageUrls[id] = legendImgUrl;
            } else {
              legendImg = dom_construct.create("div", {
                class: "ecLegend-no-image"
              });
              legendImg.innerHTML = "&nbsp;";
            }

            d_connect.connect(
              legendImg,
              "onerror",
              function() {
                //the image could not be loaded, replace it with fallback
                dom_construct.destroy(legendImg);
                var noLegendDiv = dom_construct.create("div", {
                  class: "ecLegend-no-image"
                });
                noLegendDiv.innerHTML = "&nbsp;";
                imageDiv.appendChild(noLegendDiv);

                //replace the legendURL so we do not try adding it again
                self._layerLegendImageUrls[id] = "no-legend";
              }
            );

            imageDiv.appendChild(legendImg);
          } else {
            console.warn("could not resolve legend image url..");
          }

          newInner.appendChild(labelDiv);
          newInner.appendChild(imageDiv);

          self._createClickEvent(newInner, layer);

          legendFirstChildDiv[0].appendChild(newInner);
        });
      }
    },
    _resolveLegendUrl: function(layerId) {
      var result;

      d_array.forEach(this.layers, function(l) {
        if (l.__managed === layerId) {
          var layerInfos = l.layerInfos;
          if (layerInfos) {
            d_array.forEach(layerInfos, function(li) {
              if (layerId === li.title) {
                result = li.legendURL;
              }
            });
          }
        }
      });

      return result;
    },
    _clearEvents: function() {
      this._listeners.disconnect();
    },
    _createClickEvent: function(elem, layer) {
      var self = this;
      this._listeners.connect(
        elem,
        "onclick",
        d_lang.hitch(this, function() {
          var id = layer.__managed || layer.id;
          console.info("CALLED " + id);
          var mapModel = this.mapModel;
          var layerNode = this.mapModel.getNodeById(id);
          var checked = layerNode.enabled;
          if (checked) {
            dom_class.remove(elem, "layerVisible");
            layerNode.enabled = false;
            self._layerCheckboxes[id] = "disabled";
          } else {
            dom_class.add(elem, "layerVisible");
            layerNode.enabled = true;
            self._layerCheckboxes[id] = "enabled";
          }
          this.mapModel.fireModelNodeStateChanged({
            source: {
              type: "user-clicked-legend-layer",
              layer: layer,
              isVisible: layerNode.enabled
            }
          });
        })
      );
    },
    _checkLayerVisibility: function(layer) {
      var mapModelLayer = this.mapModel.getNodeById(layer.__managed);
      if (mapModelLayer) {
        return mapModelLayer.enabled;
      }
      return true;
    },
    _replaceLayerInfoBySubLayerInfos: function(layerInfo, layer) {
      var subLayers = layerInfo.subLayers || [];
      if (!subLayers.length) {
        return [layer.getLayerInfo(layerInfo.name)];
      }
      var subLayerInfos = [];
      d_array.forEach(
        subLayers,
        function(subLayer) {
          subLayerInfos = subLayerInfos.concat(
            this._replaceLayerInfoBySubLayerInfos(subLayer, layer)
          );
        },
        this
      );
      return subLayerInfos;
    }
  });
});
