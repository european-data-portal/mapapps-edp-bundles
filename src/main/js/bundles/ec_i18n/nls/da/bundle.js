define({
	contentviewer : {
		bundleName : "Content Viewer",
		bundleDescription : "Viser forskellige former for indhold.",
		ui : {
			unknownContentError : "Indholdet er ukendt.",
			graphicinfotool : {
				title : "Element info",
				desc : "Element info"
			},
			content : {
				defaultTitle : "Element",
				grid : {
					detailView : "Vis detaljer",
					key : "Ejendom",
					value : "Value"
				},
				customTemplate : {
					detailView : "Vis detaljer"
				},
				attachment : {
					noAttachmentSupport : "Dette lag underst\u00f8tter ikke vedh\u00e6ftede filer",
					detailView : "Vis detaljer"
				},
				AGSDetail : {
					title : "Se detaljer",
					print : "Print",
					serviceMetadata : "Tjenesten metadata",
					serviceURL : "URL",
					serviceCopyrights : "Ophavsret",
					serviceTitle : "Titel",
					pager : {
						pageSizeLabel : "Feature ${currentPage} p\u00e5 ${endPage}"
					},
					key : "Ejendom",
					value : "Value",
					detailView : "Vis detaljer"
				}
			}
		}
	},
	coordinatetransformer : {
		bundleName : "Koordinere transformer",
		bundleDescription : "--KERNFUNKTIONALIT\u00c4T-- Den koordinerer transformer omdanner geometrier fra \u00e9t koordinatsystem til et andet."
	},
	featureinfo : {
		bundleName : "FeatureInfo",
		bundleDescription : "FeatureInfo viser oplysninger om funktioner til aktive lag.",
		ui : {
			featureInfo : {
				noQueryLayersFound : "Ingen s\u00f8gbar lag tilg\u00e6ngelig!",
				contentInfoWindowTitle : "Identificer",
				noResultsFound : "Ingen resultater fundet.",
				loadingInfoText : "Foresp\u00f8rger lag ${layerName} (${layerIndex} p\u00e5 ${layersTotal}).",
				featureInfoTool : "Identificer",
				layer : "Layer",
				feature : "Objekt"
			},
			wms : {
				emptyResult : "Fundet nogen resultater p\u00e5 WMS lag '${layerTitle}'."
			}
		}
	},
	geometryservice : {
		bundleName : "GeometryService",
		bundleDescription : "Pakken indeholder en central geometri service."
	},
	geojson : {
		bundleName : "GeoJSON",
		bundleDescription : "Denne pakke indeholder en konvertering service, som kan behandle omfatter GeoJSON og velkendte tekst (WKT)."
	},
	infoviewer : {
		bundleName : "Information Display",
		bundleDescription : "Displayet viser brugeren betydningsfulde oplysninger om et sted p\u00e5 kortet i et vindue.",
		ui : {
			title : "Info Viewer",
			tools : {
				closeInfoWindowMapdeskTool : "Luk",
				closeInfoWindowMapTool : "Luk",
				focusMapTool : "Center i kort",
				attachToGeorefTool : "Vedh\u00e6ft til position",
				mainActivationTool : "Oplysninger"
			}
		}
	},
	languagetoggler : {
		bundleName : "Sprog switcher",
		bundleDescription : "Gennem sproget switcher gr\u00e6nsefladesprog kan \u00e6ndres.",
		ui : {
			title : "Sprog"
		}
	},
	map : {
		bundleName : "Kort",
		bundleDescription : "Det Map bundt administrerer vigtigste kort og alle oplysninger kortindhold. Klienten kan abstrakte de tjenester, der anvendes i en hierarkisk Kort model, s\u00e5 de kan vises p\u00e5 forskellige m\u00e5der for brugeren.",
		ui : {
			nodeUpdateError : {
				info : "Opdatering af tjenesteydelsen '${title} misbrugt! Fejl: ${errorMsg}",
				detail : "Opdatering af tjenesteydelsen '${title} misbrugt! Fejl: ${url} ${errorMsg} - Detaljer: ${error}"
			},
			sliderLabels : {
				country : "Land",
				region : "Region",
				town : "By"
			}
		},
		drawTooltips : {
			addPoint : "Klik p\u00e5 kortet",
			addMultipoint : "Klik for at starte",
			finishLabel : "Finish"
		}
	},
	mobiletoc : {
		bundleName : "Mobil niveau kontrol",
		bundleDescription : "Denne pakke indeholder en mobil styring klar",
		tool : {
			title : "Kort indhold",
			tooltip : "T\u00e6nd / sluk Oversigt Indhold"
		},
		ui : {
			basemaps : "Kort Baggrund",
			operationalLayers : "Emner niveauer",
			switchUI : {
				noTitle : "uden titel",
				leftLabel : "P\u00e5",
				rightLabel : "Af"
			}
		}
	},
	mobileview : {
		bundleName : "Mobile View",
		bundleDescription : "Indeholder specialklasser, der fasts\u00e6tter nogle sp\u00f8rgsm\u00e5l vedr\u00f8rende dojox.mobile"
	},
	notifier : {
		bundleName : "Meddelelser",
		bundleDescription : "Alle status, udvikling eller fejlmeddelelser vises til brugeren i en pop op-meddelelse, s\u00e5 det er klart, hvad der sker i programmet.",
		ui : {
			title : "Notifier",
			tooltips : {
				close : "Luk Meddelelse",
				glue : "Vedh\u00e6ft besked"
			}
		}
	},
	parametermanager : {
		bundleName : "Parameter h\u00e5ndtering",
		bundleDescription : "Den Parameter Manager er ansvarlig for at uddelegere parametre fra URL til efter komponenter p\u00e5 opstart.",
		ui : {
			encoderBtn : "Link v\u00e6rkt\u00f8j",
			encoderBtnTooltip : "Link v\u00e6rkt\u00f8j",
			sendMail : "E-mail",
			refresh : "Opdater",
			linkBoxTitle : "Link URL",
			size : "(Pixels)",
			codeBoxTitle : "Kode at indlejre i HTML",
			qrCode : "QRCode",
			mailBody : "${url}",
			mailSubject : "Kig til tider dette kort!",
			options : {
				small : "lille (480 x 320)",
				medium : "medium (640 x 480)",
				large : "store (1280 x 1024)",
				custom : "brugerdefineret"
			}
		}
	},
	printing : {
		bundleName : "Print",
		bundleDescription : "Denne pakke indeholder et print v\u00e6rkt\u00f8j til at udskrive kortet.",
		tool : {
			title : "Print",
			tooltip : "Print",
			back : "Tilbage"
		},
		resultWin : {
			title : "Print"
		}
	},
	qrcode : {
		bundleName : "QRCode",
		bundleDescription : "Denne pakke indeholder en tjeneste, der kan bruges til at oprette QR-koder.",
		errorMessage : "QR code generation ikke lykkedes."
	},
	splashscreen : {
		bundleName : "Startsk\u00e6rmen",
		bundleDescription : "Startsk\u00e6rmen viser en statuslinje, mens programmet startes.",
		loadTitle : "Start ans\u00f8gning '{appName}'",
		loadBundle : "{percent}% - {startedBundles}/{maxBundlesToStart} - Opladning {name}"
	},
	system : {
		bundleName : "System",
		bundleDescription : "--KERNFUNKTIONALIT\u00c4T-- Systemet beskriver de centrale funktioner i map.apps."
	},
	templatelayout : {
		bundleName : "Vis Layout",
		bundleDescription : "--KERNFUNKTIONALIT\u00c4T-- Template Layout gennemf\u00f8rer arrangementet af alle elementer i brugergr\u00e6nsefladen baseret p\u00e5 foruddefinerede skabeloner."
	},
	templates : {
		bundleName : "Udsigt",
		bundleDescription : "--KERNFUNKTIONALIT\u00c4T-- Views bruges af Template Layout.",
		templates : {
			desktop : {
				title : "Desktop",
				desc : "En desktop-orienteret layout."
			},
			modern : {
				title : "Moderne",
				desc : "En moderne stilfuld layout."
			},
			minimal : {
				title : "Minimal",
				desc : "En minimalistisk layout"
			}
		},
		ui : {
			selectorLabelTitle : "Udsigt"
		}
	},
	themes : {
		bundleName : "Stil",
		bundleDescription : "--KERNFUNKTIONALIT\u00c4T-- Temaerne bundt styrer alle CSS oplysninger s\u00e5som farver, skrifttyper, baggrundsbilleder osv Brugerne kan skifte mellem forskellige udseender af brugergr\u00e6nsefladen elementer ved at v\u00e6lge forskellige stilarter.",
		themes : {
			pure : {
				desc : "De map.apps 'rene' stil."
			},
			night : {
				desc : "De map.apps \u00bbnat\u00ab stil."
			}
		},
		ui : {
			selectorLabelTitle : "Stil"
		}
	},
	windowmanager : {
		bundleName : "Ledelse vindue",
		bundleDescription : "--KERNFUNKTIONALIT\u00c4T-- Vinduet manager er ansvarlig for forvaltningen al dialogvinduet.",
		ui : {
			defaultWindowTitle : "Vindue",
			closeBtn : {
				title : "Luk"
			},
			minimizeBtn : {
				title : "Minimer"
			},
			maximizeBtn : {
				title : "Maksimer"
			},
			restoreBtn : {
				title : "Gendan"
			},
			opacityBtn : {
				title : "Skift gennemsigtighed"
			},
			collapseBtn : {
				title : "Skjul indhold"
			},
			loading : {
				title : "Vent venligst!",
				message : "Indl\u00e6ser ..."
			},
			okcancel : {
				title : "Sp\u00f8rgsm\u00e5l",
				okButton : "Ok",
				cancelButton : "Afbestille"
			}
		}
	},
	ec_legend : {
		bundleName : "Legend",
		bundleDescription : "Denne pakke giver ESRI legende.",
		tool : {
			title : "Legend",
			tooltip : "Legend"
		}
	},
	ec_parameter_interceptor : {
		bundleName : "ec-parameter-interceptor",
		bundleDescription : "ec-parameter-interceptor",
		errors : {
			missingQueryParameter : "Missing query parameters",
			missingTypeQueryParameter : "Missing type query parameter",
			missingDatasetOrUrlQueryParameter : "Missing dataset or url query parameter",
			noComponentContextProvided : "No componentContext provided",
			noBundleContextProvided : "No bundleContext provided",
			noQueryParametersProvided : "No query parameters provided",
			datasetTypeNotSupported : "The type '${type}' is not supported",
			invalidGeoJSON : "Invalid GeoJSON",
			unsupportedCRSType : "Only named CRS are supported",
			canNotParseNamedCRS : "Can not parse named CRS to WKID: ${name}",
			mixingOfDifferentCRS : "Mixing of different CRS is not supported",
			invalidGeoJSONType : "Unknown GeoJSON type attribute: ${type}",
			typeNotFoundInResource : "No appropriate format for ${type} found in resource",
			couldNotLoadDataset : "Could not load dataset: ${cause}",
			unableToAddDataset : "Unable to add dataset: ${cause}",
			unableToParseCapabilities : "Unable to parse WMS Capabilities response",
			canNotDetermineUrl : "Could not determine the download URL of the resource"
		}
	},
	ec_feature_info : {
		tool : {
			title : "Informationer om funktion",
			tooltip : "Informationer om funktion"
		}
	},
	ec_map_loading_screen : {
		bundleName : "ec-map-loading-screen",
		bundleDescription : "ec-map-loading-screen",
		messages : {
			initializingMap : "Initialiserer kort. Vent venligst!"
		}
	},
	ec_wms_layer_selector : {
		bundleName : "ec-wms-layer-selector",
		bundleDescription : "ec-wms-layer-selector",
		ui : {
			windowTitle : "V\u00E6lg lag",
			hint : "Den valgte WMS-tjeneste indeholder ${countToProvideSelection}-lag. V\u00E6lg venligst alle de lag, du \u00F8nsker vist p\u00E5 kortet.",
			selectAll : "V\u00E6lg alle",
			closeButton : "OK"
		},
		tool : {
			title : "Lag",
			tooltip : "Lag"
		}
	},
	ec_error_messages : {
		bundleName : "ec-error-messages",
		bundleDescription : "ec-error-messages",
		general : {
			serviceNotAvailable : "Den anmodede tjeneste er ikke tilg\u00E6ngelig",
			serviceNotSupported : "Den anmodede tjeneste underst\u00F8ttes ikke",
			invalidResource : "Der blev fundet en ugyldig ressource p\u00E5 den angivne URL",
			unsupportedResource : "Der blev fundet en ikke-underst\u00F8ttet ressource p\u00E5 den angivne URL",
			errorSource : "F\u00F8lgende fejlkilde blev fundet",
			requestedUrl : "Forespurgt URL",
			unsupportedServiceType : "The provided service type is not supported",
			technicalSupport : "Hvis problemet forts\u00E6tter, bedes du kontakte teknisk support",
			technicalSupportCreateTicket : "Create support ticket",
			weWillContactProvider : "We will contact the provider of the service and report the issue.",
			closeButton : "OK"
		},
		detailedErrors : {
			crs : "Denne tjeneste udbyder ikke et underst\u00F8ttet koordinatreferencesystem",
			encoding : "Denne tjeneste udbyder ikke en underst\u00F8ttet tegns\u00E6t",
			metadata : "Der er fejl i metadatadokumentet til denne tjeneste",
			fileFormat : "Dataformatet for den angivne URL er ugyldigt",
			dataRequest : "Der blev under foresp\u00F8rgslen efter data fra denne tjeneste modtaget et ugyldigt svar. Det kan f.eks. skyldes en intern serverfejl, en forkert konfiguration af tjenesten, at tjenesten ikke f\u00F8lger standarden, osv."
		},
		httpIssues : {
			303 : "Svaret p\u00E5 foresp\u00F8rgslen kan findes under en anden URI",
			305 : "Den forespurgte ressource er kun tilg\u00E6ngelig via en proxy-server",
			400 : "Serveren kan eller vil ikke behandle foresp\u00F8rgslen p\u00E5 grund af en client fejl",
			401 : "Den forespurgte ressource kr\u00E6ver tilladesle",
			403 : "Den forespurgte ressource er ikke offentligt tilg\u00E6ngelig",
			404 : "Den forespurgte ressource kunne ikke findes",
			405 : "Ressourcen blev efterspurgt via en ikke-godkendt HTTP-metode",
			406 : "Der blev anmodet om uacceptabelt indhold fra ressourcen",
			407 : "Ressourcen kr\u00E6ver forh\u00E5ndsgodkendelse fra en proxy-tjeneste",
			500 : "Ressourcen er ikke tilg\u00E6ngelig p\u00E5 grund af en intern serverfejl",
			501 : "Ressourcen er ikke tilg\u00E6ngelig p\u00E5 grund af en ukendt foresp\u00F8rgselsmetode",
			502 : "Ressourcen er ikke tilg\u00E6ngelig p\u00E5 grund af en forkert konfiguration af gatewayen",
			503 : "Ressourcen er ikke tilg\u00E6ngelig for \u00F8jeblikket p\u00E5 grund af overbelastning eller vedligeholdelse",
			504 : "Ressourcen er ikke tilg\u00E6ngelig. Gatewayen er enten forkert konfigureret eller svarer ikke",
			505 : "Ressourcen underst\u00F8tter ikke den angivne HTTP-version",
			generic : "Der opstod en serverfejl i forbindelse med foresp\u00F8rgslen efter den angivne ressource"
		}
	},
	ec_user_tutorial : {
		bundleName : "ec-user-tutorial",
		bundleDescription : "ec-user-tutorial",
		externalResourceDisclaimer : {
			introduction : "Denne kortviser giver adgang til eksterne tjenester og viser deres data. Disse eksterne tjenester vedligeholdes ikke af Europa-Kommissionen, og vi har derfor ingen indflydelse p\u00E5 deres tilg\u00E6ngelighed/stabilitet. F\u00F8lgende problemer kan forekomme:",
			rotatingCircle : "Den roterende cirkel i kortvisningen viser at din kortviser venter p\u00E5 svar fra en datakilde. Det kan skyldes en utilg\u00E6ngelig server eller en netv\u00E6rksfejl.",
			layerZoomLevel : "Visse lag kan ikke vises i visse zoom-niveauer. Det skyldes begr\u00E6nsninger fra den eksterne tjenesteudbyder. Du kan blive n\u00F8dt til at zoome ind eller ud for at f\u00E5 vist dataene i det p\u00E5g\u00E6ldende lag.",
			layerPanning : "Data fra en ekstern server kan v\u00E6re placeret uden for den aktuelle kortvisning. Denne kortviser fors\u00F8ger automatisk at identificere det d\u00E6kkede omr\u00E5de. I nogle tilf\u00E6lde er metadataene dog ufuldst\u00E6ndige eller forkerte, s\u00E5 du er n\u00F8dt til at tilpasse til det aktuelle kortudsnit.",
			detailInformation : "Hvis du anmoder om detaljerede oplysninger om en valgt funktion, vil din anmodning blive behandlet via en s\u00E6rskilt foresp\u00F8rgsel til kortserveren. Hvis svaret p\u00E5 din foresp\u00F8rgsel er forkert eller tomt (f.eks. p\u00E5 grund af en serverfejl), vil der ikke blive vist nogen informationer, men kun et tomt vindue.",
			mapScaling : "De baggrundskort, som anvendes i dette program, stilles til r\u00E5dighed af Den Europ\u00E6iske Unions Statistiske Kontor (Eurostat). Disse kort findes i en opl\u00F8sning p\u00E5 op til 1:50.000. Der er ved at blive udarbejdet baggrundskort i en finere opl\u00F8sning (f.eks. for at underst\u00F8tte visningen af data inde fra en by). Det betyder, at ikke alle datakilder kan blive vist i et optimalt baggrundskort endnu.",
			technicalSupport : "Hvis du st\u00F8der p\u00E5 andre problemer, skal du henvende dig til teknisk support:",
			technicalSupportContactLink : "Kontakt teknisk support",
			mapLoading : "Kort hentes...",
			donotshow : "Vis ikke denne velkomstsk\u00E6rm igen."
		},
		legalNotice : "De betegnelser og materialepr\u00E6sentationer, som anvendes p\u00E5 dette sitemap, er p\u00E5 ingen m\u00E5de udtryk for Den Europ\u00E6iske Unions holdninger vedr\u00F8rende den retlige status for et land, territorium, by eller omr\u00E5de eller dets myndigheder, eller for dens holdninger til dets gr\u00E6nsedragninger. Kosovo*: denne betegnelse er med forbehold for holdningerne til retsstilling og er i overenstemmelse med UNSCR 1244/1999 og ICJ's udtalelser om Kosovos uafh\u00E6ngighedserkl\u00E6ring. Pal\u00E6stina*: denne betegnelse skal ikke fortolkes som et udtryk for anerkendelse af en pal\u00E6stinensisk stat og er med forbehold for medlemsstaterens individuelle holdninger til dette emne.",
		legalNoticeHeader : "Ansvarsfraskrivelse",
		tutorial : {
			welcome : "Velkommen til Den Europ\u00E6iske Dataportals kortviser!",
			familiarise : "Denne lille introduktion har til form\u00E5l at s\u00E6tte dig ind i kortviserens forskellige elementer og funktioner.",
			navigation : "Klik og hold venstre museknap ned for at panorere hen over kortet.",
			zoom : "Disse knapper \u00E6ndrer zoom-indstillingen p\u00E5 kortet. Du kan ogs\u00E5 bruge musehjulet til at zoome ind eller ud.",
			features : "Disse knapper giver adgang til flere funktioner. De fungerer som til/fra-knapper og aktiverer eller deaktiverer en funktion.",
			legend : "Forklaringen kan bruges til at unders\u00F8ge de tilg\u00E6ngelig kortlag og aktivere eller deaktivere deres aktuelle visning p\u00E5 kortet. Navnene hentes direkte fra den eksterne tjeneste.",
			transparency : "Du kan ogs\u00E5 justere et lags gennemsigtighed.",
			featureinfo : "Der er adgang til yderligere oplysninger om visse data. Hvis denne funktion er aktiveret, kan du klikke p\u00E5 kortet for at foresp\u00F8rge om yderligere oplysninger.",
			done : "F\u00E6rdig",
			okButton : "OK",
			closeButton : "Luk",
			skip : "Spring over",
			next : "N\u00E6ste",
			back : "Tilbage"
		}
	}
});