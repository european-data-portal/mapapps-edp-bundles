define({
	contentviewer : {
		bundleName : "Content Viewer",
		bundleDescription : "Geeft verschillende soorten content.",
		ui : {
			unknownContentError : "De inhoud is onbekend.",
			graphicinfotool : {
				title : "Elementinformatie",
				desc : "Elementinformatie"
			},
			content : {
				defaultTitle : "Element",
				grid : {
					detailView : "Toon details",
					key : "Eigenschap",
					value : "Waarde"
				},
				customTemplate : {
					detailView : "Toon details"
				},
				attachment : {
					noAttachmentSupport : "Deze laag is niet bijlagen ondersteunen",
					detailView : "Toon details"
				},
				AGSDetail : {
					title : "Bekijk details",
					print : "Afdruk",
					serviceMetadata : "Dienst metadata",
					serviceURL : "URL",
					serviceCopyrights : "Auteursrechten",
					serviceTitle : "Titel",
					pager : {
						pageSizeLabel : "Feature ${currentPage} van ${endPage}"
					},
					key : "Eigenschap",
					value : "Waarde",
					detailView : "Toon details"
				}
			}
		}
	},
	coordinatetransformer : {
		bundleName : "Co\u00f6rdinaattransformator",
		bundleDescription : "--Basisfunctionaliteit-- Het co\u00f6rdineren transformator transformeert geometrie\u00ebn van het ene co\u00f6rdinatenstelsel naar het andere."
	},
	featureinfo : {
		bundleName : "FeatureInfo",
		bundleDescription : "FeatureInfo geeft informatie over de mogelijkheden voor actieve lagen.",
		ui : {
			featureInfo : {
				noQueryLayersFound : "Geen doorzoekbaar lagen beschikbaar!",
				contentInfoWindowTitle : "Identificeren",
				noResultsFound : "Geen resultaten gevonden.",
				loadingInfoText : "Bevragen laag ${layerName} (${layerIndex} van ${layersTotal}).",
				featureInfoTool : "Identificeren",
				layer : "Laag",
				feature : "Object"
			},
			wms : {
				emptyResult : "Geen resultaten gevonden op de WMS-laag '${layerTitle}'."
			}
		}
	},
	geometryservice : {
		bundleName : "GeometryService",
		bundleDescription : "De bundel biedt een centrale geometrie service."
	},
	geojson : {
		bundleName : "GeoJSON",
		bundleDescription : "Deze bundel biedt een conversie dienst, die GeoJSON en bekende tekst (WKT) kan verwerken."
	},
	infoviewer : {
		bundleName : "Information Display",
		bundleDescription : "De boodschap display geeft de gebruiker inhoudelijke informatie over een plaats op de kaart in een venster.",
		ui : {
			title : "Info Viewer",
			tools : {
				closeInfoWindowMapdeskTool : "Dicht",
				closeInfoWindowMapTool : "Dicht",
				focusMapTool : "Center in Kaart",
				attachToGeorefTool : "Hechten aan de positie",
				mainActivationTool : "Locatie informatie"
			}
		}
	},
	languagetoggler : {
		bundleName : "Taal switcher",
		bundleDescription : "Door middel van de taal switcher interfacetaal kan worden gewijzigd.",
		ui : {
			title : "Taal"
		}
	},
	map : {
		bundleName : "Kaart",
		bundleDescription : "De Kaart bundel beheert de belangrijkste kaart en al kaartinhoud informatie. De klant kan de samenvatting in een hi\u00ebrarchische Kaart model zodat ze kunnen worden weergegeven op verschillende manieren aan de gebruiker diensten.",
		ui : {
			nodeUpdateError : {
				info : "Actualisering van de dienst \"${title} 'mislukt! Fout: ${errorMsg}",
				detail : "Actualisering van de dienst \"${title} 'mislukt! Fout: ${url} ${errorMsg} - Meer informatie: ${error}"
			},
			sliderLabels : {
				country : "Land",
				region : "Regio",
				town : "Stad"
			}
		},
		drawTooltips : {
			addPoint : "Klik op de kaart",
			addMultipoint : "Klik om te starten",
			finishLabel : "Afwerking"
		}
	},
	mobiletoc : {
		bundleName : "Mobiele niveauregeling",
		bundleDescription : "Deze bundel biedt een mobiele besturing klaar",
		tool : {
			title : "Kaart inhoud",
			tooltip : "Schakel aan / uit Kaart inhoud"
		},
		ui : {
			basemaps : "Basiskaarten",
			operationalLayers : "Onderwerpen niveaus",
			switchUI : {
				noTitle : "geen titel",
				leftLabel : "Op",
				rightLabel : "Af"
			}
		}
	},
	mobileview : {
		bundleName : "Mobile View",
		bundleDescription : "Bevat speciale klassen dat sommige kwesties met betrekking tot dojox.mobile lossen"
	},
	notifier : {
		bundleName : "Bekendmakingen",
		bundleDescription : "All-status, voortgang en foutberichten worden weergegeven aan de gebruiker in een pop-up bericht zodat het duidelijk is wat er gebeurt in de toepassing.",
		ui : {
			title : "Notifier",
			tooltips : {
				close : "Sluiten Bericht",
				glue : "Hechten bericht"
			}
		}
	},
	parametermanager : {
		bundleName : "Parameter handling",
		bundleDescription : "De Parameter Manager is verantwoordelijk voor de parameters delegeren van de URL naar de volgens onderdelen bij het opstarten.",
		ui : {
			encoderBtn : "Link hulpmiddel",
			encoderBtnTooltip : "Link hulpmiddel",
			sendMail : "E-mail",
			refresh : "Verversen",
			linkBoxTitle : "Link URL",
			size : "Maat (beeldpunten)",
			codeBoxTitle : "Code insluiten in HTML",
			qrCode : "QRCode",
			mailBody : "${url}",
			mailSubject : "Kijk op keer deze kaart!",
			options : {
				small : "klein (480 x 320)",
				medium : "medium (640 x 480)",
				large : "grote (1280 x 1024)",
				custom : "gebruiker gedefinieerde"
			}
		}
	},
	printing : {
		bundleName : "Afdruk",
		bundleDescription : "Deze bundel biedt een afdruk hulpmiddel om de kaart af te drukken.",
		tool : {
			title : "Afdruk",
			tooltip : "Afdruk",
			back : "Terug"
		},
		resultWin : {
			title : "Afdruk"
		}
	},
	qrcode : {
		bundleName : "QRCode",
		bundleDescription : "Deze bundel een dienst die kan worden gebruikt om QR-codes cre\u00ebren.",
		errorMessage : "QR code generatie was niet succesvol."
	},
	splashscreen : {
		bundleName : "Beginscherm",
		bundleDescription : "Het startscherm wordt een voortgangsbalk weergegeven terwijl de toepassing wordt gestart.",
		loadTitle : "Start applicatie '{appName}\"",
		loadBundle : "{percent}% - {startedBundles}/{maxBundlesToStart} - Het opladen van {name}"
	},
	system : {
		bundleName : "Systeem",
		bundleDescription : "--Basisfunctionaliteit-- Het systeem beschrijft de basis core-functionaliteit (compileerbaar skelet) van map.apps."
	},
	templatelayout : {
		bundleName : "View Layout",
		bundleDescription : "--Basisfunctionaliteit-- De Template Layout implementeert de rangschikking van alle UI-elementen op basis van vooraf gedefinieerde templates."
	},
	templates : {
		bundleName : "Uitzicht",
		bundleDescription : "--Basisfunctionaliteit-- Bekeken worden gebruikt door de Template Layout.",
		templates : {
			desktop : {
				title : "Desktop",
				desc : "A-desktop-geori\u00ebnteerde lay-out."
			},
			modern : {
				title : "Modern",
				desc : "Een moderne stijlvolle lay-out."
			},
			minimal : {
				title : "Minimaal",
				desc : "Een minimalistische layout"
			}
		},
		ui : {
			selectorLabelTitle : "Uitzicht"
		}
	},
	themes : {
		bundleName : "Stijl",
		bundleDescription : "--Basisfunctionaliteit-- De Thema bundel beheert alle CSS-informatie, zoals kleuren, lettertypes, achtergrondafbeeldingen enz. Gebruikers kunnen schakelen tussen verschillende looks van de gebruikersinterface-elementen door het selecteren van verschillende stijlen.",
		themes : {
			pure : {
				desc : "De map.apps 'pure' stijl."
			},
			night : {
				desc : "De map.apps 'nacht' stijl."
			}
		},
		ui : {
			selectorLabelTitle : "Stijl"
		}
	},
	windowmanager : {
		bundleName : "Venster voor het beheer",
		bundleDescription : "--Basisfunctionaliteit-- De window manager is verantwoordelijk voor het beheer van al het dialoogvenster.",
		ui : {
			defaultWindowTitle : "Venster",
			closeBtn : {
				title : "Dicht"
			},
			minimizeBtn : {
				title : "Verkleinen"
			},
			maximizeBtn : {
				title : "Maximaliseren"
			},
			restoreBtn : {
				title : "Herstellen"
			},
			opacityBtn : {
				title : "Transparantie verandering"
			},
			collapseBtn : {
				title : "Inhoud verbergen"
			},
			loading : {
				title : "Wacht alstublieft!",
				message : "Laden ..."
			},
			okcancel : {
				title : "Vraag",
				okButton : "OK",
				cancelButton : "Annuleren"
			}
		}
	},
	ec_legend : {
		bundleName : "Legend",
		bundleDescription : "This bundle provides the Esri legend.",
		tool : {
			title : "Legende",
			tooltip : "Legende"
		}
	},
	ec_parameter_interceptor : {
		bundleName : "ec-parameter-interceptor",
		bundleDescription : "ec-parameter-interceptor",
		errors : {
			missingQueryParameter : "Missing query parameters",
			missingTypeQueryParameter : "Missing type query parameter",
			missingDatasetOrUrlQueryParameter : "Missing dataset or url query parameter",
			noComponentContextProvided : "No componentContext provided",
			noBundleContextProvided : "No bundleContext provided",
			noQueryParametersProvided : "No query parameters provided",
			datasetTypeNotSupported : "The type '${type}' is not supported",
			invalidGeoJSON : "Invalid GeoJSON",
			unsupportedCRSType : "Only named CRS are supported",
			canNotParseNamedCRS : "Can not parse named CRS to WKID: ${name}",
			mixingOfDifferentCRS : "Mixing of different CRS is not supported",
			invalidGeoJSONType : "Unknown GeoJSON type attribute: ${type}",
			typeNotFoundInResource : "No appropriate format for ${type} found in resource",
			couldNotLoadDataset : "Could not load dataset: ${cause}",
			unableToAddDataset : "Unable to add dataset: ${cause}",
			unableToParseCapabilities : "Unable to parse WMS Capabilities response",
			canNotDetermineUrl : "Could not determine the download URL of the resource"
		}
	},
	ec_feature_info : {
		tool : {
			title : "Feature Info",
			tooltip : "Feature Info"
		}
	},
	ec_map_loading_screen : {
		bundleName : "ec-map-loading-screen",
		bundleDescription : "ec-map-loading-screen",
		messages : {
			initializingMap : "Initialiseren Map. Gelieve te staan!"
		}
	},
	ec_wms_layer_selector : {
		bundleName : "ec-wms-layer-selector",
		bundleDescription : "ec-wms-layer-selector",
		ui : {
			windowTitle : "Kies layer",
			hint : "De geselecteerde WMS bevat ${countToProvideSelection} layer, kies de layers uit, die op de kaart getoond moeten worden.",
			selectAll : "Alles kiezen",
			closeButton : "OK"
		},
		tool : {
			title : "Layer",
			tooltip : "Layer"
		}
	},
	ec_error_messages : {
		bundleName : "ec-error-messages",
		bundleDescription : "ec-error-messages",
		general : {
			serviceNotAvailable : "De gevraagde dienst staat niet ter beschikking",
			serviceNotSupported : "De gevraagde dienst staat niet ter ondersteund",
			invalidResource : "In de aangegeven URL werd een een ongeldige ressource gevonden",
			unsupportedResource : "In de aangegeven URL werd een een ressource gevonden die niet ondersteund wordt",
			errorSource : "De volgende foutenbron werd vastgesteld",
			requestedUrl : "Opgevraagde URL",
			unsupportedServiceType : "De gevraagde dienst staat niet ter ondersteund",
			technicalSupport : "Indien het probleem in de toekomst verder optreedt, neem dan contact op met de technische hulpdienst",
			technicalSupportCreateTicket : "Cre\u00ebren support ticket",
			weWillContactProvider : "Wij zullen met de dienstverlener contact op nemen en de fout melden.",
			closeButton : "OK"
		},
		detailedErrors : {
			crs : "De dienst biedt geen gesteund co\u00f6rdinatenreferentiesysteem ",
			encoding : "De dienst biedt geen gesteunde tek encodering",
			metadata : "Het metadata document van de dienst is foutief",
			fileFormat : "Een ongeldig gegevensformaat werd door de aangegeven URL ontvangen",
			dataRequest : "Tijdens de gegevensaanvraag bij de dienst werd een ongeldig antwoord ontvangen. Redenen daarvoor kunnen o.a. zijn: interne server fout, foute configuratie van de dienst; een niet standaard-conforme reactie van de dienst, enz."
		},
		httpIssues : {
			303 : "Het antwoord op de aanvraag kan onder een andere URI gevonden worden ",
			305 : "De aangevraagde ressource kan alleen via een proxy ter beschikking gesteld worden ",
			400 : "De server kan of wil de aanvraag niet verwerken op basis van een waargenomen client fout",
			401 : "De aangevraagde ressource verlangd naar autorisatie",
			403 : "De aangevraagde ressource is openbaar niet toegankelijk",
			404 : "De aagevraagde ressource kon niet gevonden worden",
			405 : "De ressource werd aangevraagd door een niet-toegestane HTTP methode",
			406 : "Een niet-acceptabele inhoud werd van de ressource aangevraagd",
			407 : "De ressource vereist vooraf een authenticatie door een proxy dienst",
			500 : "De ressource staat vanwege een interne server fout niet ter beschikking ",
			501 : "De ressource staat vanwege een niet herkende aanvraagmethode niet ter beschikking ",
			502 : "De ressource staat  vanwege een fout geconfigureerde gateway niet ter beschikking",
			503 : "De ressource staat vanwege overbelasting  of onderhoud niet ter beschikking",
			504 : "De ressource staat  vanwege een fout geconfigureerde of een niet-reagerende gateway niet ter beschikking",
			505 : "De ressource ondersteunt de gegeven HTTP versie niet",
			generic : "Een server fout is opgetreden tijdens de ressource aanvraag"
		}
	},
	ec_user_tutorial : {
		bundleName : "ec-user-tutorial",
		bundleDescription : "ec-user-tutorial",
		externalResourceDisclaimer : {
			introduction : "De Map viewer maakt gebruik van externe diensten en toont deze. Deze externe diensten worden niet door de Europese Commissie verzorgd en daarom hebben wij geen invloed op de beschikbaarheid/stabiliteit. De volgende problemen kunnen optreden:",
			rotatingCircle : "Een roterende cirkel op de kaart duidt aan dat de viewer op een antwoord van de gegevensbron wacht. Dit kan veroorzaakt worden door een niet-beschikbare server of door problemen met het netwerk",
			layerZoomLevel : "Sommige layers kunnen op bepaalde zoom levels niet getoond worden. Dit hangt samen met de door de externe dienstverlener opgelegde beperkingen. Het zou kunnen zijn dat u in- of uit- moet zoomen om de gegevens te kunnen zien. ",
			layerPanning : "Gegevens van een externe server kunnen buiten het actuele kaart aanzicht liggen. Deze Map viewer probeert het gebied automatisch af te dekken. In sommige gevallen zijn echter de metadata niet compleet of onjuist zodat een grafische aanpassing nodig is.",
			detailInformation : "Indien u bijkomende informatie voor een gekozen object nodig hebt, wordt deze door een specifieke aanvraag aan de kaarten server behandeld. Indien het antwoord op deze aanvraag fout of leeg is (bijv. door een server fout), kan de informatie niet getoond worden en een leeg venster verschijnt.",
			mapScaling : "De basiskaarten in deze applicatie worden door het statistisch bureau van de Europese Unie (Eurostat) ter beschikking gesteld. De kaarten hebben een resolutie van max. 1:50.000. Kaarten met een hogere resolutie zijn in voorbereiding (om bijv. het visualiseren van gegevens binnen een stad mogelijk te maken). Sommige gegevens zullen dus niet optimaal op een kaart kunnen worden weergegeven.",
			technicalSupport : "Indien u verder nog problemen tegenkomt, neem dan contact op met de technische hulpdienst:",
			technicalSupportContactLink : "Neem contact op met de technische hulpdienst",
			donotshow : "Dit welkomscherm niet nog eens tonen.",
			mapLoading : "Kaart wordt geladen..."
		},
		legalNotice : "De presentatievorm en de benaming op deze kaart zijn vrij van elk oordeel of opinie van de Europese Commissie m. b. t. de wettelijke status van een land, territorium, stad of gebied; zijn instanties of de begrenzingen. Kosovo*:   Deze verwijzing laat de standpunten over de status van Kosovo onverlet, en is in overeenstemming met Resolutie 1244/1999 van de VN-Veiligheidsraad en het advies van het Internationaal Gerechtshof over de onafhankelijkheidsverklaring van Kosovo. Palestina*:  Deze benaming mag niet worden uitgelegd als een erkenning van de staat Palestina en laat de afzonderlijke standpunten van de lidstaten ter zake onverlet.",
		legalNoticeHeader : "Juridische mededeling",
		tutorial : {
			welcome : "Welkom bij de Map viewer van het European Data Portal",
			familiarise : "Deze korte inleiding moet u met de elementen en de functionaliteit van de Map viewer vertrouwd maken.",
			navigation : "Klik de linker mousebutton en houd deze ingedrukt om de kaart te verschuiven ",
			zoom : "Met deze buttons kunt u de kaart in- en uitzoomen. Als alternatief kunt u ook het muiswieltje gebruiken. ",
			features : "Bijkomende functionaliteit met deze buttons. Ze fungeren als wisseltoets en activeren of deactiveren de aangegeven functionaliteit. ",
			legend : "De legende kan gebruikt worden om te zien welke kaartenlayers ter beschikking staan en om ze op de kaart te activeren of te deactiveren.De namen zijn direct van de externe dienst afgeleid.",
			transparency : "U kunt ook de transparantie van een layer instellen.",
			featureinfo : "Sommige gegevens kunnen ook nader bekeken worden. Wanneer deze functie actief is kunt u de kaart aanklikken om nadere informatie op te roepen.",
			okButton : "OK",
			closeButton : "Sluiten",
			next : "Verder",
			back : "Vorige",
			skip : "Overslaan",
			done : "Klaar"
		}
	}
});