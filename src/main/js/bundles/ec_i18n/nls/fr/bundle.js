define({
	contentviewer : {
		bundleName : "Content Viewer",
		bundleDescription : "Affiche diff\u00e9rents types de contenu.",
		ui : {
			unknownContentError : "Le contenu est inconnu.",
			graphicinfotool : {
				title : "El\u00e9ment d'informations",
				desc : "El\u00e9ment d'informations"
			},
			content : {
				defaultTitle : "\u00c9l\u00e9ment",
				grid : {
					detailView : "Voir les d\u00e9tails",
					key : "Propri\u00e9t\u00e9",
					value : "Valeur"
				},
				customTemplate : {
					detailView : "Voir les d\u00e9tails"
				},
				attachment : {
					noAttachmentSupport : "Cette couche ne supporte pas les pi\u00e8ces jointes",
					detailView : "Voir les d\u00e9tails"
				},
				AGSDetail : {
					title : "Voir les d\u00e9tails",
					print : "Imprimer",
					serviceMetadata : "m\u00e9tadonn\u00e9es de service",
					serviceURL : "URL",
					serviceCopyrights : "Droits d'auteur",
					serviceTitle : "Titre",
					pager : {
						pageSizeLabel : "Caract\u00e9ristique ${currentPage} de ${endPage}"
					},
					key : "Propri\u00e9t\u00e9",
					value : "Valeur",
					detailView : "Voir les d\u00e9tails"
				}
			}
		}
	},
	coordinatetransformer : {
		bundleName : "Transformateur de coordonn\u00e9es",
		bundleDescription : "--FONCTIONNALIT\u00c9 principales-- Le transformateur de coordonn\u00e9es transforme g\u00e9om\u00e9tries \u00e0 partir de l'un syst\u00e8me de coordonn\u00e9es \u00e0 l'autre."
	},
	featureinfo : {
		bundleName : "FeatureInfo",
		bundleDescription : "FeatureInfo affiche des informations sur les caract\u00e9ristiques des couches actives.",
		ui : {
			featureInfo : {
				noQueryLayersFound : "Aucune couche consultable disponible!",
				contentInfoWindowTitle : "Identifier",
				noResultsFound : "Aucun r\u00e9sultat trouv\u00e9.",
				loadingInfoText : "Interrogation couche ${layerName} (${layerIndex} de ${layersTotal}).",
				featureInfoTool : "Identifier",
				layer : "Niveau",
				feature : "Info visualis\u00e9e"
			},
			wms : {
				emptyResult : "Aucun r\u00e9sultat trouv\u00e9 sur la couche '${layerTitle} WMS."
			}
		}
	},
	geometryservice : {
		bundleName : "GeometryService",
		bundleDescription : "Le faisceau fournit un service de g\u00e9om\u00e9trie central."
	},
	geojson : {
		bundleName : "GeoJSON",
		bundleDescription : "Cet ensemble offre un service de conversion, qui peut traiter GeoJSON et le texte bien connu (wkt)."
	},
	infoviewer : {
		bundleName : "Affichage des informations",
		bundleDescription : "L'affichage de message indique \u00e0 l'utilisateur des informations de fond sur un lieu sur la carte dans une fen\u00eatre.",
		ui : {
			title : "Info Viewer",
			tools : {
				closeInfoWindowMapdeskTool : "Fermer",
				closeInfoWindowMapTool : "Fermer",
				focusMapTool : "Centrer sur la Carte",
				attachToGeorefTool : "Fixer \u00e0 la position",
				mainActivationTool : "Information sur le lieu"
			}
		}
	},
	languagetoggler : {
		bundleName : "S\u00e9lecteur de langue",
		bundleDescription : "Gr\u00e2ce \u00e0 la langue de l'interface des commutateurs de langue peut \u00eatre chang\u00e9.",
		ui : {
			title : "Langue"
		}
	},
	map : {
		bundleName : "Carte",
		bundleDescription : "Le Plan paquet g\u00e8re la carte principale et toutes les informations de carte de contenu. Le client les services utilis\u00e9s dans un mod\u00e8le hi\u00e9rarchique de la carte de sorte qu'ils peuvent \u00eatre affich\u00e9es de diff\u00e9rentes fa\u00e7ons \u00e0 l'utilisateur peut abstrait.",
		ui : {
			nodeUpdateError : {
				info : "Mise \u00e0 jour du service '${title}' a \u00e9chou\u00e9! Erreur: ${errorMsg}",
				detail : "Mise \u00e0 jour du service '${title}' a \u00e9chou\u00e9! Erreur: ${url} ${errorMsg} - D\u00e9tail: ${error}"
			},
			sliderLabels : {
				country : "Pays",
				region : "R\u00e9gion",
				town : "Ville"
			}
		},
		drawTooltips : {
			addPoint : "Cliquez sur la carte",
			addMultipoint : "Cliquez pour d\u00e9marrer",
			finishLabel : "Finition"
		}
	},
	mobiletoc : {
		bundleName : "Contr\u00f4le de niveau mobiles",
		bundleDescription : "Cet ensemble offre un r\u00e9gulateur de niveau portable pr\u00eat",
		tool : {
			title : "Carte contenu",
			tooltip : "Activer / d\u00e9sactiver Carte contenu"
		},
		ui : {
			basemaps : "Les cartes de base",
			operationalLayers : "niveaux Sujets",
			switchUI : {
				noTitle : "pas de titre",
				leftLabel : "Sur",
				rightLabel : "De"
			}
		}
	},
	mobileview : {
		bundleName : "Voir mobile",
		bundleDescription : "Contient des classes sp\u00e9ciales qui fixent certaines questions concernant dojox.mobile"
	},
	notifier : {
		bundleName : "Notifications",
		bundleDescription : "Tous les messages d'\u00e9tat, les progr\u00e8s ou erreur sont affich\u00e9s \u00e0 l'utilisateur dans un message pop-up de sorte qu'il est clair ce qui se passe dans l'application.",
		ui : {
			title : "Notifier",
			tooltips : {
				close : "Fermer Message",
				glue : "Joindre le message"
			}
		}
	},
	parametermanager : {
		bundleName : "la manipulation de param\u00e8tres",
		bundleDescription : "Le gestionnaire des param\u00e8tres est responsable de d\u00e9l\u00e9guer les param\u00e8tres de l'URL pour les composants selon au d\u00e9marrage.",
		ui : {
			encoderBtn : "outil Lien",
			encoderBtnTooltip : "outil Lien",
			sendMail : "Email",
			refresh : "Rafra\u00eechir",
			linkBoxTitle : "Lien URL",
			size : "Taille (pixels)",
			codeBoxTitle : "Code HTML pour l'int\u00e9grer sur",
			qrCode : "QRCode",
			mailBody : "${url}",
			mailSubject : "Regardez fois cette carte!",
			options : {
				small : "petite (480 x 320)",
				medium : "moyen (640 x 480)",
				large : "grande (1280 x 1024)",
				custom : "d\u00e9fini par l'utilisateur"
			}
		}
	},
	printing : {
		bundleName : "Imprimer",
		bundleDescription : "Ce paquet fournit un outil d'impression pour imprimer la carte.",
		tool : {
			title : "Imprimer",
			tooltip : "Imprimer",
			back : "Pr\u00e9c\u00e9dent"
		},
		resultWin : {
			title : "Imprimer"
		}
	},
	qrcode : {
		bundleName : "QRCode",
		bundleDescription : "Cet ensemble fournit un service qui peut \u00eatre utilis\u00e9 pour cr\u00e9er des codes QR.",
		errorMessage : "QR g\u00e9n\u00e9ration de code ne \u00e9tait pas r\u00e9ussi."
	},
	splashscreen : {
		bundleName : "\u00e9cran d'accueil",
		bundleDescription : "L'\u00e9cran d'accueil affiche une barre de progression pendant l'application est d\u00e9marr\u00e9e.",
		loadTitle : "Lancer l'application \u00ab{appName}\u00bb",
		loadBundle : "{percent}% - {startedBundles}/{maxBundlesToStart} - Charge {name}"
	},
	system : {
		bundleName : "Syst\u00e8me",
		bundleDescription : "--FONCTIONNALIT\u00c9 principales-- Le syst\u00e8me d\u00e9crit la fonctionnalit\u00e9 de base de base (squelette compilable) de map.apps."
	},
	templatelayout : {
		bundleName : "Voir Mise en page",
		bundleDescription : "--FONCTIONNALIT\u00c9 principales-- Le mod\u00e8le de mise en \u0153uvre la disposition de tous les \u00e9l\u00e9ments de l'interface utilisateur sur la base de mod\u00e8les pr\u00e9d\u00e9finis."
	},
	templates : {
		bundleName : "Vue",
		bundleDescription : "--FONCTIONNALIT\u00c9 principales-- Vues sont utilis\u00e9s par le mod\u00e8le de disposition.",
		templates : {
			desktop : {
				title : "Bureau",
				desc : "Une mise en page orient\u00e9e bureau."
			},
			modern : {
				title : "Moderne",
				desc : "Une mise en page moderne et \u00e9l\u00e9gant."
			},
			minimal : {
				title : "Minimal",
				desc : "Une mise en page minimaliste"
			}
		},
		ui : {
			selectorLabelTitle : "Vue"
		}
	},
	themes : {
		bundleName : "Style",
		bundleDescription : "--FONCTIONNALIT\u00c9 principales-- les th\u00e8mes paquet g\u00e8re toutes les informations de CSS comme les couleurs, les styles de police, images de fond, etc. Les utilisateurs peuvent basculer entre les diff\u00e9rents regards des \u00e9l\u00e9ments de l'interface utilisateur en s\u00e9lectionnant des styles diff\u00e9rents.",
		themes : {
			pure : {
				desc : "Style pur \u00bbLes map.apps."
			},
			night : {
				desc : "De la nuit \u00bbLes map.apps de style."
			}
		},
		ui : {
			selectorLabelTitle : "Style"
		}
	},
	windowmanager : {
		bundleName : "fen\u00eatre Gestion",
		bundleDescription : "--FONCTIONNALIT\u00c9 principales-- Le gestionnaire de fen\u00eatres est responsable de la gestion de l'ensemble de la fen\u00eatre de dialogue.",
		ui : {
			defaultWindowTitle : "Fen\u00eatre",
			closeBtn : {
				title : "Fermer"
			},
			minimizeBtn : {
				title : "Minimiser"
			},
			maximizeBtn : {
				title : "Maximiser"
			},
			restoreBtn : {
				title : "Restaurer"
			},
			opacityBtn : {
				title : "Changer la transparence"
			},
			collapseBtn : {
				title : "Masquer contenu"
			},
			loading : {
				title : "S'il vous pla\u00eet attendez!",
				message : "Chargement ..."
			},
			okcancel : {
				title : "Question",
				okButton : "OK",
				cancelButton : "Annuler"
			}
		}
	},
	ec_legend : {
		bundleName : "L\u00e9gende",
		bundleDescription : "Ce paquet fournit la l\u00e9gende Esri.",
		tool : {
			title : "L\u00e9gende",
			tooltip : "L\u00e9gende"
		}
	},
	ec_parameter_interceptor : {
		bundleName : "ec-parameter-interceptor",
		bundleDescription : "ec-parameter-interceptor",
		errors : {
			missingQueryParameter : "Missing query parameters",
			missingTypeQueryParameter : "Missing type query parameter",
			missingDatasetOrUrlQueryParameter : "Missing dataset or url query parameter",
			noComponentContextProvided : "No componentContext provided",
			noBundleContextProvided : "No bundleContext provided",
			noQueryParametersProvided : "No query parameters provided",
			datasetTypeNotSupported : "The type '${type}' is not supported",
			invalidGeoJSON : "Invalid GeoJSON",
			unsupportedCRSType : "Only named CRS are supported",
			canNotParseNamedCRS : "Can not parse named CRS to WKID: ${name}",
			mixingOfDifferentCRS : "Mixing of different CRS is not supported",
			invalidGeoJSONType : "Unknown GeoJSON type attribute: ${type}",
			typeNotFoundInResource : "No appropriate format for ${type} found in resource",
			couldNotLoadDataset : "Could not load dataset: ${cause}",
			unableToAddDataset : "Unable to add dataset: ${cause}",
			unableToParseCapabilities : "Unable to parse WMS Capabilities response",
			canNotDetermineUrl : "Could not determine the download URL of the resource"
		}
	},
	ec_feature_info : {
		tool : {
			title : "Info visualis\u00e9e",
			tooltip : "Info visualis\u00e9e"
		}
	},
	ec_map_loading_screen : {
		bundleName : "ec-map-loading-screen",
		bundleDescription : "ec-map-loading-screen",
		messages : {
			initializingMap : "Chargement de la carte..."
		}
	},
	ec_wms_layer_selector : {
		bundleName : "ec-wms-layer-selector",
		bundleDescription : "ec-wms-layer-selector",
		ui : {
			windowTitle : "Choisir niveau",
			hint : "Le service WMS s\u00e9lectionn\u00e9 contient ${countToProvideSelection} niveaux, veuillez choisir ceux que vous voulez visualiser sur la carte graphique.",
			selectAll : "Choisir tout",
			closeButton : "OK"
		},
		tool : {
			title : "Niveau",
			tooltip : "Niveau"
		}
	},
	ec_error_messages : {
		bundleName : "ec-error-messages",
		bundleDescription : "ec-error-messages",
		general : {
			serviceNotAvailable : "Le service demand\u00e9 ne sont pas disponibles",
			serviceNotSupported : "Le service demand\u00e9 n'est pas pris en charge",
			unsupportedServiceType : "Le service demand\u00e9 n'est pas pris en charge",
			technicalSupportCreateTicket : "Contacter support technique",
			closeButton : "OK",
			invalidResource : "Une ressource non valide a \u00e9t\u00e9 trouv\u00e9 \u00e0 l'URL donn\u00e9e",
			unsupportedResource : "Une ressource non pris en charge a \u00e9t\u00e9 trouv\u00e9 \u00e0 l'URL donn\u00e9e",
			errorSource : "La source d'erreur suivant a \u00e9t\u00e9 d\u00e9termin\u00e9",
			requestedUrl : "URL demand\u00e9e",
			technicalSupport : "Si le probl\u00e8me persiste \u00e0 l'avenir, s'il vous pla\u00eet contacter le support technique",
			weWillContactProvider : "Nous allons contacter le fournisseur du service pour l\u2019informer du probl\u00e8me."
		},
		detailedErrors : {
			crs : "Le service ne fournit pas un syst\u00e8me de r\u00e9f\u00e9rence pris en charge la coordination",
			encoding : "Le service ne fournit pas un codage de caract\u00e8res pris en charge",
			metadata : "Le document de m\u00e9tadonn\u00e9es du service est erron\u00e9e",
			fileFormat : "Un format de donn\u00e9es non valide a \u00e9t\u00e9 re\u00e7u \u00e0 l'URL donn\u00e9e",
			dataRequest : "Bien que l'interrogation des donn\u00e9es du service une r\u00e9ponse non valide a \u00e9t\u00e9 re\u00e7u. Les raisons peuvent \u00eatre, par exemple: erreur de serveur interne, mauvaise configuration du service, non-conforme \u00e0 la norme comportement du service, etc."
		},
		httpIssues : {
			303 : "La r\u00e9ponse \u00e0 la demande peut \u00eatre trouv\u00e9 sous un autre URI",
			305 : "La ressource demand\u00e9e est disponible uniquement via un proxy",
			400 : "Le serveur ne peut pas ou ne veut pas traiter la requ\u00eate en raison d'une erreur de perception client",
			401 : "La ressource demand\u00e9e n\u00e9cessitait l'autorisation",
			403 : "La ressource demand\u00e9e est pas accessible au public",
			404 : "La ressource demand\u00e9e n'a pas pu \u00eatre trouv\u00e9",
			405 : "La ressource a \u00e9t\u00e9 demand\u00e9 par une m\u00e9thode HTTP non autoris\u00e9e",
			406 : "Un contenu pas acceptable a \u00e9t\u00e9 demand\u00e9 \u00e0 la ressource",
			407 : "La ressource n\u00e9cessite une authentification pr\u00e9alable \u00e0 un service de proxy",
			500 : "La ressource ne sont pas disponibles en raison d'une erreur de serveur interne",
			501 : "La ressource ne sont pas disponibles en raison d'une m\u00e9thode de requ\u00eate non reconnue",
			502 : "La ressource est pas disponible en raison d'une passerelle \u00e0 tort configur\u00e9",
			503 : "La ressource est actuellement indisponible pour cause de surcharge ou de maintenance",
			504 : "La ressource est pas disponible en raison d'une passerelle configur\u00e9 \u00e0 tort ou ne r\u00e9pond pas",
			505 : "La ressource ne supporte pas la version HTTP donn\u00e9",
			generic : "Une erreur de serveur tout en demandant la ressource donn\u00e9e"
		}
	},
	ec_user_tutorial : {
		bundleName : "ec-user-tutorial",
		bundleDescription : "ec-user-tutorial",
		externalResourceDisclaimer : {
			introduction : "Ce visualisateur de cartes graphiques utilise des services externes et affiche leurs donn\u00e9es. La maintenance de ces services n\u2019est pas assur\u00e9e par la Commission Europ\u00e9enne - c\u2019est pourquoi nous n\u2019avons pas d\u2019influence sur leur disponibilit\u00e9 et/ou stabilit\u00e9. Les erreurs suivantes peuvent appara\u00eetre:",
			rotatingCircle : "Un cercle en rotation indique que le visualisateur attend une r\u00e9ponse depuis la source des donn\u00e9es. Ceci peut \u00eatre caus\u00e9 par un serveur hors service ou par des probl\u00e8mes de r\u00e9seau",
			layerZoomLevel : "Certaines couches graphiques ne sont pas visibles pour certains niveaux d\u2019agrandissement. Ceci peut r\u00e9sulter d\u2019une restriction de la part du fournisseur du service externe. Essayez d\u2019autres niveaux d\u2019agrandissement pour afficher les donn\u00e9es de la couche graphique",
			layerPanning : "Des donn\u00e9es provenant d\u2019un serveur externe peuvent \u00eatre repr\u00e9sent\u00e9es en dehors de la couche graphique actuelle. Le visualisateur essaye d\u2019identifier automatiquement l\u2019espace occup\u00e9. Pourtant, des cas peuvent exister o\u00f9 les m\u00e9tadonn\u00e9es sont incompl\u00e8tes ou erron\u00e9es, de sorte que l\u2019utilisateur doit naviguer sur la couche graphique actuelle",
			detailInformation : "Si vous demandez des informations d\u00e9taill\u00e9es pour la fonctionnalit\u00e9 s\u00e9lectionn\u00e9e, celle-ci sera trait\u00e9e par le serveur externe. Dans le cas o\u00f9 la r\u00e9ponse sera malform\u00e9e ou vide (p.ex. due \u00e0 une erreur serveur), le syst\u00e8me n\u2019affichera aucune donn\u00e9e et une page vide sera visible.",
			technicalSupport : "Si vous rencontrer d\u2019autres probl\u00e8mes, n\u2019h\u00e9sitez pas \u00e0 contacter le support technique:",
			technicalSupportContactLink : "Contacter support technique",
			donotshow : "Ne plus afficher ce nouveau.",
			mapLoading : "Chargement de la carte...",
			mapScaling : "Les cartes graphiques utilis\u00e9es dans cette application sont mises \u00e0 disposition par l'Office statistique de l'Union europ\u00e9enne (Eurostat). Celles-ci sont affich\u00e9es jusqu'\u00e0 l'\u00e9chelle 1:50.000. Des r\u00e9solutions plus fines permettant de visualiser p.ex. des donn\u00e9es d\u00e9taill\u00e9es au sein d'une ville, sont actuellement en pr\u00e9paration. C'est pourquoi certaines sources de donn\u00e9es ne peuvent pas encore \u00eatre affich\u00e9es sur une carte graphique de fa\u00e7on optimale."
		},
		legalNotice : "Les d\u00e9nominations utilis\u00e9es et la pr\u00e9sentation du contenu de cette carte n'expriment en aucun cas un avis de l'Union europ\u00e9enne sur le statut juridique d'un pays, d'un territoire, d'une ville, d'une r\u00e9gion ou de ses autorit\u00e9s, ou sur le trac\u00e9 de ses fronti\u00e8res ou limites. Kosovo*: Cette d\u00e9signation est sans pr\u00e9judice des positions sur le statut et est conforme \u00e0 la r\u00e9solution 1244 du Conseil de s\u00e9curit\u00e9 des Nations unies ainsi qu'\u00e0 l'avis de la CIJ sur la d\u00e9claration d'ind\u00e9pendance du Kosovo. Palestine*: Cette d\u00e9nomination ne saurait \u00eatre interpr\u00e9t\u00e9e comme une reconnaissance d'un \u00c9tat de Palestine et est sans pr\u00e9judice de la position de chaque \u00c9tat membre sur cette question.",
		legalNoticeHeader : "Avis juridique",
		tutorial : {
			welcome : "Bienvenue au visualiseur de cartes graphiques du Portail europ\u00e9en de donn\u00e9es",
			familiarise : "Cette petite introduction a pour but de vous familiariser avec les \u00e9l\u00e9ments et les fonctionnalit\u00e9s du visualiseur de cartes graphiques.",
			navigation : "Cliquez et maintenez le bouton gauche de la souris pour vous d\u00e9placer sur la carte.",
			zoom : "Ces boutons changent le niveau de zoom sur la carte. Vous pouvez \u00e9galement utiliser la molette de la souris pour ajuster le zoom.",
			features : "Une fonctionnalit\u00e9 suppl\u00e9mentaire est disponible par le biais de ces boutons. Ils agissent comme bascule pour activer ou d\u00e9sactiver la fonctionnalit\u00e9 s\u00e9lectionn\u00e9e.",
			legend : "La l\u00e9gende peut \u00eatre utilis\u00e9e pour examiner les couches cartographiques disponibles et activer ou d\u00e9sactiver l'affichage actuel sur la carte. Les noms sont directement d\u00e9riv\u00e9es du service acc\u00e9d\u00e9 \u00e0 l'ext\u00e9rieur.",
			transparency : "Vous pouvez \u00e9galement ajuster la transparence d'une couche.",
			featureinfo : "Certaines donn\u00e9es peuvent \u00eatre examin\u00e9es m\u00eame plus en d\u00e9tail. Lorsque cette fonction est activ\u00e9e, vous pouvez cliquer sur la carte pour demander des informations suppl\u00e9mentaires.",
			okButton : "OK",
			closeButton : "Fermer",
			next : "Suivant",
			back : "Pr\u00e9c\u00e9dent",
			skip : "Sauter",
			done : "Termin\u00e9"
		}
	}
});