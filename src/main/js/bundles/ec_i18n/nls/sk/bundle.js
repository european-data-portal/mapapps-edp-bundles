define({
	contentviewer : {
		bundleName : "Prehliada\u010d obsahu",
		bundleDescription : "Zobrazuje r\u00f4zne druhy obsahu.",
		ui : {
			unknownContentError : "Obsah je nezn\u00e1ma.",
			graphicinfotool : {
				title : "Element inform\u00e1cie",
				desc : "Element inform\u00e1cie"
			},
			content : {
				defaultTitle : "Prvok",
				grid : {
					detailView : "Zobrazi\u0165 detaily",
					key : "Nehnute\u013enos\u0165",
					value : "Hodnota"
				},
				customTemplate : {
					detailView : "Zobrazi\u0165 detaily"
				},
				attachment : {
					noAttachmentSupport : "T\u00e1to vrstva nepodporuje pr\u00edlohy",
					detailView : "Zobrazi\u0165 detaily"
				},
				AGSDetail : {
					title : "Zobrazi\u0165 detaily",
					print : "Tla\u010d",
					serviceMetadata : "Metad\u00e1ta Service",
					serviceURL : "URL",
					serviceCopyrights : "Autorsk\u00e9 pr\u00e1va",
					serviceTitle : "N\u00e1zov",
					pager : {
						pageSizeLabel : "Funkcia ${currentPage} z ${endPage}"
					},
					key : "Nehnute\u013enos\u0165",
					value : "Hodnota",
					detailView : "Zobrazi\u0165 detaily"
				}
			}
		}
	},
	coordinatetransformer : {
		bundleName : "S\u00faradn\u00edc transform\u00e1tor",
		bundleDescription : "--CORE Funk\u010dnos\u0165-- Koordinova\u0165 transform\u00e1tor transformuje geometria z jedn\u00e9ho s\u00faradnicov\u00e9ho syst\u00e9mu do druh\u00e9ho."
	},
	featureinfo : {
		bundleName : "FeatureInfo",
		bundleDescription : "FeatureInfo zobrazuje inform\u00e1cie o funkci\u00e1ch pre akt\u00edvnej vrstvy.",
		ui : {
			featureInfo : {
				noQueryLayersFound : "No vyh\u013ead\u00e1vania vrstiev k dispoz\u00edcii!",
				contentInfoWindowTitle : "Identifikova\u0165",
				noResultsFound : "Neboli n\u00e1jden\u00e9 \u017eiadne v\u00fdsledky.",
				loadingInfoText : "Dotazovanie vrstvy ${layerName} (${layerIndex} z ${layersTotal}).",
				featureInfoTool : "Identifikova\u0165",
				layer : "Vrstva",
				feature : "Objekt"
			},
			wms : {
				emptyResult : 'N\u00e1jden\u00e9 na WMS vrstvy "${layerTitle}" \u017diadne v\u00fdsledky.'
			}
		}
	},
	geometryservice : {
		bundleName : "GeometryService",
		bundleDescription : "Bal\u00ed\u010dek poskytuje centr\u00e1lne geometrie slu\u017eby."
	},
	geojson : {
		bundleName : "GeoJSON",
		bundleDescription : "Tento bal\u00edk obsahuje prevodn\u00e9 slu\u017ebu, ktor\u00e1 dok\u00e1\u017ee spracova\u0165 GeoJSON a dobre zn\u00e1my text (WKT)."
	},
	infoviewer : {
		bundleName : "Informa\u010dn\u00fd displej",
		bundleDescription : "Na displeji sa zobraz\u00ed spr\u00e1va s u\u017e\u00edvate\u013emi podstatn\u00e9 inform\u00e1cie o mieste, na mape v okne.",
		ui : {
			title : "Info Viewer",
			tools : {
				closeInfoWindowMapdeskTool : "Zavrie\u0165",
				closeInfoWindowMapTool : "Zavrie\u0165",
				focusMapTool : "Stred v mape",
				attachToGeorefTool : "Pripoji\u0165 do polohy",
				mainActivationTool : "Inform\u00e1cie o umiestnen\u00ed"
			}
		}
	},
	languagetoggler : {
		bundleName : "Prep\u00edna\u010d jazykov",
		bundleDescription : "Prostredn\u00edctvom jazyka jazyk prep\u00edna\u010de rozhrania je mo\u017en\u00e9 zmeni\u0165.",
		ui : {
			title : "Jazyk"
		}
	},
	map : {
		bundleName : "Mapa",
		bundleDescription : "Bal\u00ed\u010dek Map riadi hlavn\u00fa mapu a v\u0161etky inform\u00e1cie mapa obsahu. Klient m\u00f4\u017ee abstraktn\u00e9 slu\u017eby pou\u017eit\u00e9 v hierarchickom modeli mapy tak, \u017ee m\u00f4\u017eu by\u0165 zobrazen\u00e9 r\u00f4znymi sp\u00f4sobmi pre u\u017e\u00edvate\u013ea.",
		ui : {
			nodeUpdateError : {
				info : "Aktualiz\u00e1cia slu\u017eby \"${title} 'zlyhalo! Chyba: ${errorMsg}",
				detail : "Aktualiz\u00e1cia slu\u017eby \"${title} 'zlyhalo! Chyba: ${url} ${errorMsg} - Podrobnosti: ${error}"
			},
			sliderLabels : {
				country : "Krajiny",
				region : "Kraj",
				town : "Mesto"
			}
		},
		drawTooltips : {
			addPoint : "Kliknut\u00edm na mapu",
			addMultipoint : "Kliknut\u00edm na tla\u010didlo pre spustenie",
			finishLabel : "\u00daprava"
		}
	},
	mobiletoc : {
		bundleName : "Ovl\u00e1danie mobiln\u00e9 \u00farovne",
		bundleDescription : "Tento bal\u00edk obsahuje ovl\u00e1da\u010d mobiln\u00e9 \u00farove\u0148 ready",
		tool : {
			title : "Mapa obsah",
			tooltip : "Zapnutie / vypnutie Mapa obsah"
		},
		ui : {
			basemaps : "Z\u00e1kladn\u00e9 mapy",
			operationalLayers : "T\u00e9my \u00farovne",
			switchUI : {
				noTitle : "bez n\u00e1zvu",
				leftLabel : "Na",
				rightLabel : "Pre\u010d"
			}
		}
	},
	mobileview : {
		bundleName : "Mobile View",
		bundleDescription : "Obsahuje \u0161peci\u00e1lne triedy, ktor\u00e9 opravi\u0165 niektor\u00e9 ot\u00e1zky t\u00fdkaj\u00face sa dojox.mobile"
	},
	notifier : {
		bundleName : "Ozn\u00e1menia",
		bundleDescription : "V\u0161etky stavov\u00e9, pokroku alebo chybov\u00e9 hl\u00e1senie sa zobraz\u00ed pou\u017e\u00edvate\u013eovi v spr\u00e1ve pop-up tak, aby bolo jasn\u00e9, \u010do sa deje v aplik\u00e1cii.",
		ui : {
			title : "Notifier",
			tooltips : {
				close : "Zavrie\u0165 t\u00fato spr\u00e1vu",
				glue : "Pripoji\u0165 spr\u00e1vu"
			}
		}
	},
	parametermanager : {
		bundleName : "Manipul\u00e1cia parametrov",
		bundleDescription : "Parameter mana\u017e\u00e9r je zodpovedn\u00fd prenies\u0165 parametre z URL, aby sa pod\u013ea prie\u010dinkov na \u0161tarte.",
		ui : {
			encoderBtn : "Link n\u00e1stroj",
			encoderBtnTooltip : "Link n\u00e1stroj",
			sendMail : "E-mail",
			refresh : "Osvie\u017ei\u0165",
			linkBoxTitle : "Link URL",
			size : "Ve\u013ekos\u0165 (v pixeloch)",
			codeBoxTitle : "K\u00f3d vlo\u017ei\u0165 do HTML",
			qrCode : "QRCode",
			mailBody : "${url}",
			mailSubject : "Pozrite sa ob\u010das t\u00fato kartu!",
			options : {
				small : "mal\u00fd (480 x 320)",
				medium : "medium (640 x 480)",
				large : "ve\u013ek\u00e9 (1280 x 1024)",
				custom : "u\u017e\u00edvate\u013esky definovan\u00e9"
			}
		}
	},
	printing : {
		bundleName : "Tla\u010d",
		bundleDescription : "Tento bal\u00edk obsahuje tla\u010dov\u00fd n\u00e1stroj k tla\u010di mapy.",
		tool : {
			title : "Tla\u010d",
			tooltip : "Tla\u010d",
			back : "Sp\u00e4\u0165"
		},
		resultWin : {
			title : "Tla\u010d"
		}
	},
	qrcode : {
		bundleName : "QRCode",
		bundleDescription : "Tento bal\u00edk poskytuje slu\u017ebu, ktor\u00e1 m\u00f4\u017ee by\u0165 pou\u017eit\u00e1 na vytvorenie QR k\u00f3dov.",
		errorMessage : "QR generovanie k\u00f3du nebol \u00faspe\u0161n\u00fd."
	},
	splashscreen : {
		bundleName : "Domovsk\u00e1 obrazovka",
		bundleDescription : "Domovsk\u00e1 obrazovka sa zobraz\u00ed ukazovate\u013e priebehu, ke\u010f je aplik\u00e1cia spusten\u00e1.",
		loadTitle : 'Spustenie aplik\u00e1cie "{appName}"',
		loadBundle : "{percent}% - {startedBundles}/{maxBundlesToStart} - nab\u00edjanie {name}"
	},
	system : {
		bundleName : "Syst\u00e9m",
		bundleDescription : "--CORE Funk\u010dnos\u0165-- System popisuje z\u00e1kladn\u00e9 z\u00e1kladn\u00e9 funkcie (skompilova\u0165 kostra) na map.apps."
	},
	templatelayout : {
		bundleName : "Zobrazenie rozlo\u017eenia",
		bundleDescription : "--CORE Funk\u010dnos\u0165-- \u0161abl\u00f3ny rozlo\u017eenia realizuje usporiadanie v\u0161etk\u00fdch prvkov pou\u017e\u00edvate\u013esk\u00e9ho rozhrania na z\u00e1klade preddefinovan\u00fdch \u0161abl\u00f3n."
	},
	templates : {
		bundleName : "Poh\u013ead",
		bundleDescription : "--CORE Funk\u010dnos\u0165-- Poh\u013eady s\u00fa pou\u017e\u00edvan\u00e9 rozlo\u017eenie \u0161abl\u00f3ny.",
		templates : {
			desktop : {
				title : "Desktop",
				desc : "Layout desktop orientovan\u00e9."
			},
			modern : {
				title : "Modern\u00e9",
				desc : "Modern\u00fd elegantn\u00fd layout."
			},
			minimal : {
				title : "Minim\u00e1lna",
				desc : "Minimalistick\u00fd layout"
			}
		},
		ui : {
			selectorLabelTitle : "Poh\u013ead"
		}
	},
	themes : {
		bundleName : "\u0160t\u00fdl",
		bundleDescription : "--CORE Funk\u010dnos\u0165-- zv\u00e4zok T\u00e9my spravuje v\u0161etky inform\u00e1cie, CSS, ako farby, \u0161t\u00fdly p\u00edsma, obr\u00e1zky na pozad\u00ed a pod Pou\u017e\u00edvatelia m\u00f4\u017eu prep\u00edna\u0165 medzi r\u00f4znymi poh\u013eadmi jednotliv\u00fdch prvkov pou\u017e\u00edvate\u013esk\u00e9ho rozhrania v\u00fdberom r\u00f4znych \u0161t\u00fdlov.",
		themes : {
			pure : {
				desc : '"\u010cist\u00fd" \u0161t\u00fdl map.apps.'
			},
			night : {
				desc : 'Map.apps "No" \u0161t\u00fdl.'
			}
		},
		ui : {
			selectorLabelTitle : "\u0160t\u00fdl"
		}
	},
	windowmanager : {
		bundleName : "Okno Spr\u00e1va",
		bundleDescription : "--CORE Funk\u010dnos\u0165-- Spr\u00e1vca okien je zodpovedn\u00fd za riadenie v\u0161etk\u00fdch dial\u00f3g.",
		ui : {
			defaultWindowTitle : "Okno",
			closeBtn : {
				title : "Zavrie\u0165"
			},
			minimizeBtn : {
				title : "Minimalizova\u0165"
			},
			maximizeBtn : {
				title : "Maximalizova\u0165"
			},
			restoreBtn : {
				title : "Obnovi\u0165"
			},
			opacityBtn : {
				title : "Zmeni\u0165 prieh\u013eadnos\u0165"
			},
			collapseBtn : {
				title : "Skry\u0165 obsah"
			},
			loading : {
				title : "\u010cakajte, pros\u00edm!",
				message : "Nakladanie ..."
			},
			okcancel : {
				title : "Ot\u00e1zka",
				okButton : "OK",
				cancelButton : "Zru\u0161i\u0165"
			}
		}
	},
	ec_legend : {
		bundleName : "Legend",
		bundleDescription : "This bundle provides the Esri legend.",
		tool : {
			title : "Legenda",
			tooltip : "Legenda"
		}
	},
	ec_parameter_interceptor : {
		bundleName : "ec-parameter-interceptor",
		bundleDescription : "ec-parameter-interceptor",
		errors : {
			missingQueryParameter : "Missing query parameters",
			missingTypeQueryParameter : "Missing type query parameter",
			missingDatasetOrUrlQueryParameter : "Missing dataset or url query parameter",
			noComponentContextProvided : "No componentContext provided",
			noBundleContextProvided : "No bundleContext provided",
			noQueryParametersProvided : "No query parameters provided",
			datasetTypeNotSupported : "The type '${type}' is not supported",
			invalidGeoJSON : "Invalid GeoJSON",
			unsupportedCRSType : "Only named CRS are supported",
			canNotParseNamedCRS : "Can not parse named CRS to WKID: ${name}",
			mixingOfDifferentCRS : "Mixing of different CRS is not supported",
			invalidGeoJSONType : "Unknown GeoJSON type attribute: ${type}",
			typeNotFoundInResource : "No appropriate format for ${type} found in resource",
			couldNotLoadDataset : "Could not load dataset: ${cause}",
			unableToAddDataset : "Unable to add dataset: ${cause}",
			unableToParseCapabilities : "Unable to parse WMS Capabilities response",
			canNotDetermineUrl : "Could not determine the download URL of the resource"
		}
	},
	ec_feature_info : {
		tool : {
			title : "Popis geoprvku",
			tooltip : "Popis geoprvku"
		}
	},
	ec_map_loading_screen : {
		bundleName : "ec-map-loading-screen",
		bundleDescription : "ec-map-loading-screen",
		messages : {
			initializingMap : "Inicializ\u00e1cia mapy. Pros\u00edm po\u010dkajte!"
		}
	},
	ec_wms_layer_selector : {
		bundleName : "ec-wms-layer-selector",
		bundleDescription : "ec-wms-layer-selector",
		ui : {
			windowTitle : "Vybra\u0165 vrstvy",
			hint : "Vybran\u00e1 WMS slu\u017eba obsahuje ${countToProvideSelection} vrstiev, vyberte v\u0161etky vrstvy, ktor\u00e9 maj\u00fa by\u0165 zobrazen\u00e9 v mape, pros\u00edm.",
			selectAll : "Vybra\u0165 v\u0161etko",
			closeButton : "OK"
		},
		tool : {
			title : "Vrstva",
			tooltip : "Vrstva"
		}
	},
	ec_error_messages : {
		bundleName : "ec-error-messages",
		bundleDescription : "ec-error-messages",
		general : {
			serviceNotAvailable : "Po\u017eadovan\u00e1 slu\u017ebe je nedostupn\u00e1",
			serviceNotSupported : "Po\u017eadovan\u00e1 slu\u017eba nie je podporovan\u00e1",
			invalidResource : "Na zadanej URL adrese bol n\u00e1jden\u00fd neplatn\u00fd \u00fadajov\u00fd zdroj",
			unsupportedResource : "Na zadanej URL adrese bol n\u00e1jden\u00fd nepodporovan\u00fd \u00fadajov\u00fd zdroj",
			errorSource : "Zisten\u00fd bol nasleduj\u00faci zdroj chyby",
			requestedUrl : "Po\u017eadovan\u00e1 adresa URL",
			unsupportedServiceType : "Po\u017eadovan\u00e1 slu\u017eba nie je podporovan\u00e1",
			technicalSupport : "Ak bude probl\u00e9m na\u010falej pretrv\u00e1va\u0165, kontaktujte pros\u00edm technick\u00fa podporu",
			technicalSupportCreateTicket : "Vytvori\u0165 l\u00edstok podpory",
			weWillContactProvider : "Skontaktujeme poskytovate\u013ea slu\u017eby a nahl\u00e1sime mu zisten\u00fd probl\u00e9m.",
			closeButton : "OK"
		},
		detailedErrors : {
			crs : "Slu\u017eba neposkytuje podporovan\u00fd referen\u010dn\u00fd s\u00faradnicov\u00fd syst\u00e9m.",
			encoding : "Slu\u017eba neposkytuje znakov\u00fa sadu v podporovanom k\u00f3dovan\u00ed.",
			metadata : "Meta\u00fadajov\u00fd dokument slu\u017eby je chybn\u00fd",
			fileFormat : "Zo zadanej URL adresy bol z\u00edskan\u00fd neplatn\u00fd \u00fadajov\u00fd form\u00e1t.",
			dataRequest : "Slu\u017eba vr\u00e1tila k po\u017eiadavke o \u00fadaje neplatn\u00fa odpove\u010f. D\u00f4vodom m\u00f4\u017ee by\u0165 napr\u00edklad: vn\u00fatorn\u00e1 chyba servera, zl\u00e1 konfigur\u00e1cia slu\u017eby, chovanie slu\u017eby  nie je nes\u00falade so \u0161tandardmi at\u010f."
		},
		httpIssues : {
			303 : "Odpove\u010f na po\u017eiadavku je dostupn\u00e1 na inej adrese URI",
			305 : "Po\u017eadovan\u00fd zdroj je dostupn\u00fd len cez proxy",
			400 : "Server nespracuje teraz ani v bud\u00facnosti zadan\u00fa po\u017eiadavku kv\u00f4li zistenej chybe na strane klienta",
			401 : "Po\u017eadovan\u00fd zdroj si vy\u017eaduje autoriz\u00e1ciu pr\u00edstupu",
			403 : "Po\u017eadovan\u00fd zdroj nie je verejne dostupn\u00fd",
			404 : "Po\u017eadovan\u00fd zdroj sa nepodarilo n\u00e1js\u0165",
			405 : "Po\u017eiadavka na dan\u00fd zdroj bola zadan\u00e1 prostredn\u00edctvom nepovolenej HTTP met\u00f3dy.",
			406 : "Zo zdroja bol vy\u017eiadan\u00fd neprijate\u013en\u00fd obsah.",
			407 : "Pre pr\u00edstup k zdroju je potrebn\u00e9 vopred vykona\u0165 autentifik\u00e1ciu v proxy slu\u017ebe",
			500 : "Zdroj je nedostupn\u00fd kv\u00f4li internej chybe servera",
			501 : "Zdroj je nedostupn\u00fd kv\u00f4li nerozoznej forme po\u017eiadavky",
			502 : "Zdroj je nedostupn\u00fd kv\u00f4li chybne konfigurovanej br\u00e1ne",
			503 : "Zdroj je moment\u00e1lne nedostupn\u00fd kv\u00f4li vykon\u00e1vanej syst\u00e9movej \u00fadr\u017ebe",
			504 : "Zdroj je nedostupn\u00fd kv\u00f4li chybne nakonfigurovanej alebo nereagucej br\u00e1ne",
			505 : "Zdroj nepodporuje dan\u00fa HTTP verziu",
			generic : "Nastala chyba servera pri rie\u0161en\u00ed po\u017eiadavky pre zadan\u00fd zdroj"
		}
	},
	ec_user_tutorial : {
		bundleName : "ec-user-tutorial",
		bundleDescription : "ec-user-tutorial",
		externalResourceDisclaimer : {
			introduction : "Tento mapov\u00fd prehliada\u010d vyu\u017e\u00edva extern\u00e9 webov\u00e9 slu\u017eby a zobrazuje ich \u00fadaje. Tieto extern\u00e9 webov\u00e9 slu\u017eby nie s\u00fa v spr\u00e1ve Eur\u00f3pskej komisie a preto nem\u00e1me \u017eiadny vplyv na ich dostupnos\u0165/stabilitu. M\u00f4\u017eu nasta\u0165 nasledovn\u00e9 probl\u00e9my:",
			rotatingCircle : "Rotuj\u00faci kr\u00fa\u017eok v priestore mapov\u00e9ho n\u00e1h\u013eadu znamen\u00e1, \u017ee prehliada\u010d \u010dak\u00e1 na odpove\u010f \u00fadajov\u00e9ho zdroja. Toto m\u00f4\u017ee by\u0165 sp\u00f4soben\u00e9 nedostupnos\u0165ou servera alebo sie\u0165ov\u00fdmi probl\u00e9mami.",
			layerZoomLevel : "Niektor\u00e9 zdroje vrstvy sa nemusia zobrazova\u0165 v niektor\u00fdch \u00farovniach pribl\u00ed\u017eenia mapy. Toto je sp\u00f4soben\u00e9 obmedzeniami, ktor\u00e9 ur\u010dil poskytovate\u013e externej slu\u017eby. Pre tak\u00e9 pr\u00edpady posta\u010d\u00ed zmeni\u0165 pribl\u00ed\u017eenie (zoom) mapy, aby boli zobrazen\u00e9 v\u0161etky \u00fadaje danej vrstvy.",
			layerPanning : "\u00dadaje z\u00edskan\u00e9 z extern\u00e9ho servera m\u00f4\u017eu ma\u0165 geopriestorov\u00e9 umiestnenie mimo aktu\u00e1lneho mapov\u00e9ho v\u00fdrezu. Mapov\u00fd prehliada\u010d sa pok\u00fasi automaticky identifikova\u0165 kompletn\u00fa oblas\u0165 mapov\u00e9ho pokrytia. Napriek tomu sa m\u00f4\u017ee sta\u0165, \u017ee meta\u00fadaje s\u00fa ne\u00fapln\u00e9 \u010di chybn\u00e9 a preto bude mo\u017en\u00e9 vyu\u017e\u00edva\u0165 \u00fadaje len v aktu\u00e1lnom rozsahu mapov\u00e9ho v\u00fdrezu.",
			detailInformation : "Ak \u017eiadate o podrobn\u00e9 inform\u00e1cie k zvolen\u00e9mu mapov\u00e9mu prvku, tieto s\u00fa z\u00edskavan\u00e9 \u0161pecifickou po\u017eiadavkou zaslanou na mapov\u00fd server. Ak je odpove\u010f na tak\u00fa po\u017eiadavku v zlej forme alebo pr\u00e1zdna (napr. kv\u00f4li chybe servera) , potom ch\u00fdba inform\u00e1cia, ktor\u00e1 by sa dala zobrazi\u0165 a zobraz\u00ed sa t\u00fdm p\u00e1dom pr\u00e1zdne okno.",
			mapScaling : "Z\u00e1kladn\u00e9 mapy pou\u017eit\u00e9 v tejto aplik\u00e1cii poskytol \u0161tatistick\u00fd \u00farad Eur\u00f3pskej \u00fanie (Eurostat). Tieto mapy s\u00fa v s\u00fa\u010dasnosti dostupn\u00e9 v mierkach a\u017e po 1:50 000. Podrobnej\u0161ie z\u00e1kladn\u00e9 mapy s\u00fa aktu\u00e1lne v pr\u00edprave (napr. pre vizualizovanie \u00fadajov v r\u00e1mci mesta). T\u00fdm p\u00e1dom niektor\u00e9 \u00fadajov\u00e9 zdroje nemusia by\u0165 zobrazen\u00e9 s najvyhovuj\u00facej\u0161ou podkladovou mapou.",
			technicalSupport : "Ak naraz\u00edte na nejak\u00fd probl\u00e9m, nev\u00e1hajte kontaktova\u0165 technick\u00fa podporu:",
			technicalSupportContactLink : "Kontaktova\u0165 technick\u00fa podporu",
			donotshow : "Nezobrazova\u0165 znovu t\u00fato \u00favodn\u00fa obrazovku.",
			mapLoading : "Mapa sa nahr\u00e1va..."
		},
		legalNotice : "Pou\u017eit\u00e9 n\u00e1zvoslovie a materi\u00e1l prezentovan\u00fd na tejto mape nepredstavuj\u00fa vyjadrenie ak\u00e9hoko\u013evek n\u00e1zoru zo strany Eur\u00f3pskej \u00fanie t\u00fdkaj\u00face sa pr\u00e1vneho postavenia ktorejko\u013evek krajiny, \u00fazemia, mesta alebo oblasti alebo ich org\u00e1nov, \u010di t\u00fdkaj\u00face sa vymedzenia ich hran\u00edc. Kosovo *: T\u00fdmto ozna\u010den\u00edm nie s\u00fa dotknut\u00e9 stanovisk\u00e1 t\u00fdkaj\u00face sa \u0161tat\u00fatu a ozna\u010denie je v s\u00falade s rezol\u00faciou Bezpe\u010dnostnej rady OSN 1244/1999 ako aj so stanoviskom Medzin\u00e1rodn\u00e9ho s\u00fadneho dvora k vyhl\u00e1seniu nez\u00e1vislosti Kosova. Palest\u00edna *: Toto ozna\u010denie sa nesmie vyklada\u0165 ako uznanie palest\u00ednskeho \u0161t\u00e1tu a net\u00fdka sa individu\u00e1lnych postojov \u010dlensk\u00fdch \u0161t\u00e1tov na t\u00fato problematiku.",
		legalNoticeHeader : "Pr\u00e1vne upozornenie",
		tutorial : {
			welcome : "Vitajte v mapovom prehliada\u010di Port\u00e1lu eur\u00f3pskych \u00fadajov.",
			familiarise : "Toto mal\u00e9 \u00favodn\u00e9 predstavenie m\u00e1 za cie\u013e zozn\u00e1mi\u0165 V\u00e1s so s\u00fa\u010das\u0165ami a funkcionalitou mapov\u00e9ho prehliada\u010da.",
			navigation : "Kliknite a pridr\u017ete \u013eav\u00e9 tla\u010didlo my\u0161i pre posun na mape.",
			zoom : "Tieto tla\u010didl\u00e1 menia \u00farove\u0148 pribl\u00ed\u017eenia na mape. Pr\u00edpadne m\u00f4\u017eete pou\u017ei\u0165 aj koliesko my\u0161i pre \u00fapravu pribl\u00ed\u017eenia.",
			features : "Dodato\u010dn\u00e1 funkcionalita je dostupn\u00e1 t\u00fdmito tla\u010didlami. Funguj\u00fa ako prep\u00edna\u010de a zap\u00ednaj\u00fa/vyp\u00ednaj\u00fa dan\u00fd n\u00e1stroj.",
			legend : "Legendu je mo\u017en\u00e9 vyu\u017ei\u0165 na overenie dostupnosti mapov\u00fdch vrstiev. N\u00e1zvy vrstiev s\u00fa prevzat\u00e9 z externej slu\u017eby, ktor\u00e1 dan\u00e9 vrstvy poskytuje.",
			transparency : "M\u00f4\u017eete upravova\u0165 aj transparentnos\u0165 vrstvy.",
			featureinfo : "Niektor\u00e9 \u00fadaje je mo\u017en\u00e9 sk\u00fama\u0165 aj detailnej\u0161ie. AK je t\u00e1to vlastnos\u0165 aktivovan\u00e1, m\u00f4\u017eete klikn\u00fa\u0165 do mapy a z\u00edska\u0165 \u010fal\u0161ie inform\u00e1cie.",
			okButton : "OK",
			closeButton : "Zavrie\u0165",
			next : "\u010eal\u0161\u00ed",
			back : "Predo\u0161l\u00fd",
			skip : "Vynecha\u0165",
			done : "Hotovo"
		}
	}
});