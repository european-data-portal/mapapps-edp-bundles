define({
	contentviewer : {
		bundleName : "Content Viewer",
		bundleDescription : "Visar olika typer av inneh\u00e5ll.",
		ui : {
			unknownContentError : "Inneh\u00e5llet \u00e4r ok\u00e4nd.",
			graphicinfotool : {
				title : "elementinformation",
				desc : "elementinformation"
			},
			content : {
				defaultTitle : "Element",
				grid : {
					detailView : "Visa detaljer",
					key : "Fastighetsf\u00f6rmedling",
					value : "V\u00e4rde"
				},
				customTemplate : {
					detailView : "Visa detaljer"
				},
				attachment : {
					noAttachmentSupport : "Detta lager st\u00f6der inte bifogade filer",
					detailView : "Visa detaljer"
				},
				AGSDetail : {
					title : "Visa detaljer",
					print : "Trycket",
					serviceMetadata : "Tj\u00e4nsten metadata",
					serviceURL : "URL",
					serviceCopyrights : "Upphovsr\u00e4tt",
					serviceTitle : "Titel",
					pager : {
						pageSizeLabel : "Feature ${currentPage} p\u00e5 ${endPage}"
					},
					key : "Fastighetsf\u00f6rmedling",
					value : "V\u00e4rde",
					detailView : "Visa detaljer"
				}
			}
		}
	},
	coordinatetransformer : {
		bundleName : "Koordinat transformator",
		bundleDescription : "--K\u00e4rnfunktioner-- Koordinat transformator omvandlar geometrier fr\u00e5n ett koordinatsystem till ett annat."
	},
	featureinfo : {
		bundleName : "FeatureInfo",
		bundleDescription : "FeatureInfo visar information om funktioner f\u00f6r aktiva skikt.",
		ui : {
			featureInfo : {
				noQueryLayersFound : "Ingen s\u00f6kbar skikt finns!",
				contentInfoWindowTitle : "Identifiera",
				noResultsFound : "Inga resultat hittades.",
				loadingInfoText : "St\u00e4lla fr\u00e5gor lagret ${layerName} (${layerIndex} p\u00e5 ${layersTotal}).",
				featureInfoTool : "Identifiera",
				layer : "Skikt",
				feature : "Objektet"
			},
			wms : {
				emptyResult : "Inga resultat hittades p\u00e5 WMS lagret \"${layerTitle} '."
			}
		}
	},
	geometryservice : {
		bundleName : "GeometryService",
		bundleDescription : "Bunten ger en central geometri tj\u00e4nst."
	},
	geojson : {
		bundleName : "GeoJSON",
		bundleDescription : "Detta paket ger en konverteringstj\u00e4nst, vilket kan bearbeta GeoJSON och v\u00e4lk\u00e4nda texten (WKT)."
	},
	infoviewer : {
		bundleName : "Information Display",
		bundleDescription : "Meddelandet displayen visar anv\u00e4ndaren v\u00e4sentlig information om en plats p\u00e5 kartan i ett f\u00f6nster.",
		ui : {
			title : "Info Viewer",
			tools : {
				closeInfoWindowMapdeskTool : "St\u00e4ng",
				closeInfoWindowMapTool : "St\u00e4ng",
				focusMapTool : "Center i karta",
				attachToGeorefTool : "Bifoga till l\u00e4ge",
				mainActivationTool : "Platsinformation"
			}
		}
	},
	languagetoggler : {
		bundleName : "Spr\u00e5k switcher",
		bundleDescription : "Genom gr\u00e4nssnittsspr\u00e5ket switcher kan \u00e4ndras.",
		ui : {
			title : "Spr\u00e5k"
		}
	},
	map : {
		bundleName : "Karta",
		bundleDescription : "Kart bunt hanterar huvudkartan och all information kartan inneh\u00e5ll. Kunden kan abstrakta de tj\u00e4nster som anv\u00e4nds i en hierarkisk Karta modell s\u00e5 att de kan visas p\u00e5 olika s\u00e4tt f\u00f6r anv\u00e4ndaren.",
		ui : {
			nodeUpdateError : {
				info : "Uppdatering av tj\u00e4nsten \"${title} 'misslyckades! Fel: ${errorMsg}",
				detail : "Uppdatering av tj\u00e4nsten \"${title} 'misslyckades! Fel: ${url} ${errorMsg} - Detaljer: ${error}"
			},
			sliderLabels : {
				country : "Land",
				region : "Region",
				town : "Stad"
			}
		},
		drawTooltips : {
			addPoint : "Klicka p\u00e5 kartan",
			addMultipoint : "Klicka f\u00f6r att starta",
			finishLabel : "Avslut"
		}
	},
	mobiletoc : {
		bundleName : "Kontroll mobila niv\u00e5",
		bundleDescription : "Detta paket tillhandah\u00e5ller en mobil niv\u00e5 regulatorn klar",
		tool : {
			title : "Kartinneh\u00e5ll",
			tooltip : "Sl\u00e5 p\u00e5 / av Kartinneh\u00e5ll"
		},
		ui : {
			basemaps : "Base kartor",
			operationalLayers : "\u00c4mnen niv\u00e5er",
			switchUI : {
				noTitle : "ingen titel",
				leftLabel : "P\u00e5",
				rightLabel : "Av"
			}
		}
	},
	mobileview : {
		bundleName : "Mobil View",
		bundleDescription : "Inneh\u00e5ller specialklasser som fixerar vissa fr\u00e5gor som r\u00f6r dojox.mobile"
	},
	notifier : {
		bundleName : "Meddelanden",
		bundleDescription : "Alla status, framsteg eller felmeddelanden visas f\u00f6r anv\u00e4ndaren i ett popup-meddelande s\u00e5 att det klart framg\u00e5r vad som h\u00e4nder i programmet.",
		ui : {
			title : "Meddelaren",
			tooltips : {
				close : "St\u00e4ng Meddelande",
				glue : "F\u00e4st meddelande"
			}
		}
	},
	parametermanager : {
		bundleName : "Parameter f\u00f6rest\u00e5ndaren",
		bundleDescription : "Den Parameter Manager ansvarar f\u00f6r att delegera parametrar fr\u00e5n URL till enligt komponenter p\u00e5 start.",
		ui : {
			encoderBtn : "L\u00e4nkverktyg",
			encoderBtnTooltip : "L\u00e4nkverktyg",
			sendMail : "E-post",
			refresh : "Uppdatera",
			linkBoxTitle : "L\u00e4nk URL",
			size : "Storlek (bildpunkter)",
			codeBoxTitle : "Kod f\u00f6r att b\u00e4dda in HTML",
			qrCode : "QRCode",
			mailBody : "${url}",
			mailSubject : "Titta p\u00e5 tider detta kort!",
			options : {
				small : "liten (480 x 320)",
				medium : "medium (640 x 480)",
				large : "stor (1280 x 1024)",
				custom : "anv\u00e4ndardefinierade"
			}
		}
	},
	printing : {
		bundleName : "Trycket",
		bundleDescription : "Detta paket ger en utskriftsverktyg f\u00f6r att skriva ut kartan.",
		tool : {
			title : "Trycket",
			tooltip : "Trycket",
			back : "Tillbaka"
		},
		resultWin : {
			title : "Trycket"
		}
	},
	qrcode : {
		bundleName : "QRCode",
		bundleDescription : "Detta paket tillhandah\u00e5ller en tj\u00e4nst som kan anv\u00e4ndas f\u00f6r att skapa QR-koder.",
		errorMessage : "QR kodgenerering var inte lyckat."
	},
	splashscreen : {
		bundleName : "Startsk\u00e4rmen",
		bundleDescription : "Startsk\u00e4rmen visar en f\u00f6rloppsindikator medan programmet startas.",
		loadTitle : 'Starta programmet "{appName}"',
		loadBundle : "{percent}% - {startedBundles}/{maxBundlesToStart} - Laddning {name}"
	},
	system : {
		bundleName : "System",
		bundleDescription : "--K\u00e4rnfunktioner-- Systemet beskriver den grundl\u00e4ggande k\u00e4rnfunktioner (compilable skelett) av map.apps."
	},
	templatelayout : {
		bundleName : "Visa Layout",
		bundleDescription : "--K\u00e4rnfunktioner-- mallayouten genomf\u00f6r arrangemanget av alla gr\u00e4nssnittselement baserade p\u00e5 f\u00f6rdefinierade mallar."
	},
	templates : {
		bundleName : "Utsikt",
		bundleDescription : "--K\u00e4rnfunktioner-- Visningar anv\u00e4nds av Mall Layout.",
		templates : {
			desktop : {
				title : "Desktop",
				desc : "En Skrivbordsmilj\u00f6 orienterad layout."
			},
			modern : {
				title : "Modern",
				desc : "En modern snygg layout."
			},
			minimal : {
				title : "Minimal",
				desc : "En minimalistisk layout"
			}
		},
		ui : {
			selectorLabelTitle : "Utsikt"
		}
	},
	themes : {
		bundleName : "Stil",
		bundleDescription : "--K\u00e4rnfunktioner-- Den Teman bunt hanterar all CSS information s\u00e5som f\u00e4rger, teckensnitt, bakgrundsbilder etc. Anv\u00e4ndare kan v\u00e4xla mellan olika utseende hos anv\u00e4ndargr\u00e4nssnittselement genom att v\u00e4lja olika stilar.",
		themes : {
			pure : {
				desc : 'De map.apps "rena" stil.'
			},
			night : {
				desc : 'De map.apps "kv\u00e4ll" stil.'
			}
		},
		ui : {
			selectorLabelTitle : "Stil"
		}
	},
	windowmanager : {
		bundleName : "F\u00f6rvaltning f\u00f6nster",
		bundleDescription : "--K\u00e4rnfunktioner-- F\u00f6nsterhanteraren ansvarar f\u00f6r att hantera alla dialogf\u00f6nstret.",
		ui : {
			defaultWindowTitle : "F\u00f6nster",
			closeBtn : {
				title : "St\u00e4ng"
			},
			minimizeBtn : {
				title : "Minimera"
			},
			maximizeBtn : {
				title : "Maximera"
			},
			restoreBtn : {
				title : "Restore"
			},
			opacityBtn : {
				title : "\u00c4ndra transparens"
			},
			collapseBtn : {
				title : "G\u00f6m inneh\u00e5ll"
			},
			loading : {
				title : "V\u00e4nta!",
				message : "Laddar ..."
			},
			okcancel : {
				title : "Fr\u00e5ga",
				okButton : "OK",
				cancelButton : "Avboka"
			}
		}
	},
	ec_legend : {
		bundleName : "Legend",
		bundleDescription : "This bundle provides the Esri legend.",
		tool : {
			title : "Legend",
			tooltip : "Legend"
		}
	},
	ec_parameter_interceptor : {
		bundleName : "ec-parameter-interceptor",
		bundleDescription : "ec-parameter-interceptor",
		errors : {
			missingQueryParameter : "Missing query parameters",
			missingTypeQueryParameter : "Missing type query parameter",
			missingDatasetOrUrlQueryParameter : "Missing dataset or url query parameter",
			noComponentContextProvided : "No componentContext provided",
			noBundleContextProvided : "No bundleContext provided",
			noQueryParametersProvided : "No query parameters provided",
			datasetTypeNotSupported : "The type '${type}' is not supported",
			invalidGeoJSON : "Invalid GeoJSON",
			unsupportedCRSType : "Only named CRS are supported",
			canNotParseNamedCRS : "Can not parse named CRS to WKID: ${name}",
			mixingOfDifferentCRS : "Mixing of different CRS is not supported",
			invalidGeoJSONType : "Unknown GeoJSON type attribute: ${type}",
			typeNotFoundInResource : "No appropriate format for ${type} found in resource",
			couldNotLoadDataset : "Could not load dataset: ${cause}",
			unableToAddDataset : "Unable to add dataset: ${cause}",
			unableToParseCapabilities : "Unable to parse WMS Capabilities response",
			canNotDetermineUrl : "Could not determine the download URL of the resource"
		}
	},
	ec_feature_info : {
		tool : {
			title : "Funktionsinfo",
			tooltip : "Funktionsinfo"
		}
	},
	ec_map_loading_screen : {
		bundleName : "ec-map-loading-screen",
		bundleDescription : "ec-map-loading-screen",
		messages : {
			initializingMap : "Initierar karta. V\u00e4nligen v\u00e4nta!"
		}
	},
	ec_wms_layer_selector : {
		bundleName : "ec-wms-layer-selector",
		bundleDescription : "ec-wms-layer-selector",
		ui : {
			windowTitle : "V\u00e4lj niv\u00e5",
			hint : "Den valda WMS tj\u00e4nsten inneh\u00e5ller ${countToProvideSelection} niv\u00e5n, v\u00e4lj alla niv\u00e5er som ska visas p\u00e5 kartan.",
			selectAll : "V\u00e4lj alla",
			closeButton : "OK"
		},
		tool : {
			title : "Skikt",
			tooltip : "Skikt"
		}
	},
	ec_error_messages : {
		bundleName : "ec-error-messages",
		bundleDescription : "ec-error-messages",
		general : {
			serviceNotAvailable : "Den beg\u00e4rda tj\u00e4nsten \u00e4r inte tillg\u00e4nglig",
			serviceNotSupported : "Den beg\u00e4rda tj\u00e4nsten st\u00f6ds inte",
			invalidResource : "Ett ogiltigt resurs hittades p\u00e5 den angivna URL",
			unsupportedResource : "En ouppburen resurs hittades p\u00e5 den angivna URL",
			errorSource : "F\u00f6ljande felk\u00e4lla fastst\u00e4lldes",
			requestedUrl : "Beg\u00e4rda webbadressen",
			unsupportedServiceType : "Den beg\u00e4rda tj\u00e4nsten st\u00f6ds inte",
			technicalSupport : "Om problemet kvarst\u00e5r i framtiden, kontakta den tekniska supporten",
			technicalSupportCreateTicket : "Skapa support\u00e4rende ",
			weWillContactProvider : "Vi kontaktar leverant\u00f6ren av tj\u00e4nsten och rapporterar problemet.",
			closeButton : "OK"
		},
		detailedErrors : {
			crs : "Tj\u00e4nsten erbjuder inget  underst\u00f6tt referenskoordinatsystem",
			encoding : "Tj\u00e4nsten erbjuder inga underst\u00f6dda teckenkodningar",
			metadata : "Tj\u00e4nstens metadatadokument \u00e4r felaktig",
			fileFormat : "Ett ogiltigt dataformat inkom till den givna webbadressen",
			dataRequest : "Vid  s\u00f6kning av data fr\u00e5n tj\u00e4nsten har ett ogiltigt svar emottagits. Orsakerna till detta kan vara, till exempel: internt server fel, fel konfiguration av tj\u00e4nsten, icke-standardiserade \u00f6vertr\u00e4delser hos tj\u00e4nsten, etc."
		},
		httpIssues : {
			303 : "Svaret p\u00e5 denna beg\u00e4ran kan hittas i en annan URI",
			305 : "Den beg\u00e4rda resursen \u00e4r endast tillg\u00e4nglig via en proxy",
			400 : "Servern kan eller inte vill hantera beg\u00e4ran p\u00e5 grund av ett upplevt klientfel",
			401 : "Den beg\u00e4rda resursen kr\u00e4ver tillst\u00e5nd",
			403 : "Den beg\u00e4rda resursen \u00e4r inte tillg\u00e4nglig f\u00f6r allm\u00e4nheten",
			404 : "Den beg\u00e4rda resursen kunde inte hittas",
			405 : "Resursen beg\u00e4rdes genom en icke till\u00e5ten HTTP-metod",
			406 : "Ett icke acceptabelt inneh\u00e5ll beg\u00e4rdes fr\u00e5n resursen",
			407 : "Resursen kr\u00e4ver en tidigare autentisering vid en proxytj\u00e4nst",
			500 : "Resursen \u00e4r inte tillg\u00e4nglig p\u00e5 grund av ett internt serverfel",
			501 : "Resursen \u00e4r inte tillg\u00e4nglig p\u00e5 grund av en ok\u00e4nd metodbeg\u00e4ran",
			502 : "Resursen \u00e4r inte tillg\u00e4nglig p\u00e5 grund av en felaktigt konfigurerad gateway",
			503 : "Resursen \u00e4r f\u00f6r n\u00e4rvarande inte tillg\u00e4nglig p\u00e5 grund av \u00f6verbelastning eller underh\u00e5ll",
			504 : "Resursen \u00e4r inte tillg\u00e4nglig p\u00e5 grund av en felaktigt konfigurerad eller oemottaglig gateway",
			505 : "Resursen st\u00f6der inte given HTTP-versionen",
			generic : "Ett serverfel uppstod vid beg\u00e4ran av s\u00e4rskild resurs"
		}
	},
	ec_user_tutorial : {
		bundleName : "ec-user-tutorial",
		bundleDescription : "ec-user-tutorial",
		externalResourceDisclaimer : {
			introduction : "Denna kartbetraktare medger \u00e5tkomst av externa tj\u00e4nster och visar data. Dessa externa tj\u00e4nster uppr\u00e4tth\u00e5lls inte av Europeiska kommissionen och d\u00e4rf\u00f6r har vi inget inflytande p\u00e5 deras tillg\u00e4nglighet / stabilitet. F\u00f6ljande problem kan uppst\u00e5:",
			rotatingCircle : "En roterande cirkel i kartvyn indikerar att betraktaren v\u00e4ntar p\u00e5 ett svar fr\u00e5n en datak\u00e4lla. Detta kan orsakas av en otillg\u00e4nglig server eller n\u00e4tverksproblem.",
			layerZoomLevel : "Vissa skiktk\u00e4llor kan inte visas vid vissa zoomniv\u00e5er. Detta beror p\u00e5 begr\u00e4nsningar satta av den externa tj\u00e4nsteleverant\u00f6ren. Du kan beh\u00f6va zooma in eller ut f\u00f6r att visa data i skiktet.",
			layerPanning : "Data som kan n\u00e5s fr\u00e5n en extern server kan vara placerad utanf\u00f6r den aktuella kartvyn. Denna kartbetraktare f\u00f6rs\u00f6ker att automatiskt identifiera det t\u00e4ckta omr\u00e5det. \u00c4nd\u00e5 i vissa fall kan det vara s\u00e5 att metadata icke \u00e4r fullst\u00e4ndig eller felaktig s\u00e5 att du har att panorera till den faktiska omfattningen.",
			detailInformation : "Om du beg\u00e4r information f\u00f6r en vald funktion kommer detta att hanteras av en s\u00e4rskild beg\u00e4ran till kartan servern. Om svaret p\u00e5 denna beg\u00e4ran \u00e4r felaktigt eller tomt (t ex p\u00e5 grund av ett serverfel) kan ingen information visas och ett tomt f\u00f6nster kommer att visas.",
			mapScaling : "De bakgrundskartor som anv\u00e4nds i denna till\u00e4mpning tillhandah\u00e5lls av statistikkontor i Europeiska unionen (Eurostat). Dessa kartor finns f\u00f6r n\u00e4rvarande tillg\u00e4ngliga i en uppl\u00f6sning p\u00e5 upp till 1: 50,000. Mer finkorniga bakgrundskartor f\u00f6rbereds f\u00f6r n\u00e4rvarande (t ex f\u00f6r att st\u00f6dja den visualisering av data inom en stad). S\u00e5ledes kan vissa datak\u00e4llor kanske \u00e4nnu inte visas med en optimalt anpassad bakgrundskarta.",
			technicalSupport : "Om du st\u00f6ter p\u00e5 n\u00e5gra andra problem tveka inte att ta kontakt med den tekniska supporten:",
			technicalSupportContactLink : "Kontakta den tekniska supporten",
			mapLoading : "Karta laddas",
			donotshow : "Visa inte det h\u00e4r v\u00e4lkomstf\u00f6nstret igen."
		},
		legalNotice : "Beteckningarna som anv\u00e4nds och presentationen av material p\u00e5 denna karta inneb\u00e4r inte uttryck f\u00f6r n\u00e5gon \u00e5sikt av Europeiska unionen om den r\u00e4ttsliga st\u00e4llningen f\u00f6r n\u00e5got land, territorium, stad eller omr\u00e5de eller dess myndigheter, eller om avgr\u00e4nsningen av dess gr\u00e4nser eller gr\u00e4nslinjer. Kosovo *: Denna beteckning p\u00e5verkar inte st\u00e5ndpunkter om Kosovos status och \u00e4r i linje med s\u00e4kerhetsr\u00e5dets resolution 1244/1999 och med Internationella domstolens utl\u00e5tande om Kosovos sj\u00e4lvst\u00e4ndighetsf\u00f6rklaring. Palestina *: Denna beteckning ska inte tolkas som ett erk\u00e4nnande av en palestinsk stat och p\u00e5verkar inte de enskilda st\u00e5ndpunkter medlemsstaterna i denna fr\u00e5ga.",
		legalNoticeHeader : "R\u00e4ttsligt meddelande",
		tutorial : {
			welcome : "V\u00e4lkommen till kartvisaren i den Europeiska dataportalen",
			familiarise : "Denna korta introduktion har som m\u00e5l att g\u00f6ra  dig bekant med delarna och funktionerna i kartvisaren.",
			navigation : "Klicka och h\u00e5ll nere v\u00e4nster musknapp f\u00f6r att panorera p\u00e5 kartan.",
			zoom : "Dessa knappar \u00e4ndrar zoomniv\u00e5n p\u00e5 kartan. Alternativt kan du anv\u00e4nda mushjulet f\u00f6r att justera zoom.",
			features : "Ytterligare funktioner \u00e4r tillg\u00e4ngliga via dessa knappar. De aktiverar eller inaktiverar den angivna funktionen.",
			legend : "Legenden kan anv\u00e4ndas f\u00f6r att unders\u00f6ka de tillg\u00e4ngliga kartniv\u00e5erna och aktivera eller inaktivera deras nuvarande visning p\u00e5 kartan. Namnen kan direkt h\u00e4rledas fr\u00e5n den externt tillg\u00e4ngliga tj\u00e4nsten.",
			transparency : "Du kan \u00e4ven justera insynen i en niv\u00e5",
			featureinfo : "Vissa data kan unders\u00f6kas \u00e4ven mer i detalj. N\u00e4r den h\u00e4r funktionen \u00e4r aktiverad kan du klicka p\u00e5 kartan f\u00f6r att s\u00f6ka ytterligare information.",
			done : "Klar",
			okButton : "OK",
			closeButton : "St\u00e4nga",
			skip : "Avbryt",
			next : "N\u00e4sta",
			back : "F\u00f6reg\u00e5ende"
		}
	}
});