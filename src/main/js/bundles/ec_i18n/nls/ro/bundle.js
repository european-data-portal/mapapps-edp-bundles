define({
	contentviewer : {
		bundleName : "Content Viewer",
		bundleDescription : "Displays different kinds of content.",
		ui : {
			unknownContentError : "The content is unknown.",
			graphicinfotool : {
				title : "Item Info",
				desc : "Item Info"
			},
			content : {
				defaultTitle : "Item",
				grid : {
					detailView : "Show details",
					key : "Property",
					value : "Value"
				},
				customTemplate : {
					detailView : "Show details"
				},
				attachment : {
					noAttachmentSupport : "This layer does not offer attachment support",
					detailView : "Open detail view"
				},
				AGSDetail : {
					title : "Detail View",
					print : "Print",
					serviceMetadata : "Service metadata",
					serviceURL : "URL",
					serviceCopyrights : "Copyrights",
					serviceTitle : "Title",
					pager : {
						pageSizeLabel : "Feature ${currentPage} of ${endPage}"
					},
					key : "Property",
					value : "Value",
					detailView : "Show details"
				}
			}
		}
	},
	coordinatetransformer : {
		bundleName : "Coordinate Transformer",
		bundleDescription : "--CORE FUNCTIONALITY-- The Coordinate Transformer transforms geometries from a source coordinate system to a target coordinate system."
	},
	featureinfo : {
		bundleName : "FeatureInfo",
		bundleDescription : "FeatureInfo displays information on features for active layers.",
		ui : {
			featureInfo : {
				noQueryLayersFound : "No queryable layers found!",
				contentInfoWindowTitle : "Identify",
				noResultsFound : "No results found.",
				loadingInfoText : "Querying layer ${layerName} (${layerIndex} of ${layersTotal}).",
				featureInfoTool : "Identify",
				layer : "Layer",
				feature : "Feature"
			},
			wms : {
				emptyResult : "No results found on the WMS layer '${layerTitle}'."
			}
		}
	},
	geometryservice : {
		bundleName : "GeometryService",
		bundleDescription : "The bundle provides a central geometry service."
	},
	geojson : {
		bundleName : "GeoJson",
		bundleDescription : "This bundle provides a conversion service for geojson and well-known text formats"
	},
	infoviewer : {
		bundleName : "Info Viewer",
		bundleDescription : "The Info Viewer shows content information for a location in a moveable and resizable window.",
		ui : {
			title : "Info Viewer",
			tools : {
				closeInfoWindowMapdeskTool : "Close",
				closeInfoWindowMapTool : "Close",
				focusMapTool : "Center in Map",
				attachToGeorefTool : "Attach to position",
				mainActivationTool : "Location Information"
			}
		}
	},
	languagetoggler : {
		bundleName : "Language Toggler",
		bundleDescription : "The language of the user interface can be switched by a language toggler.",
		ui : {
			title : "Language"
		}
	},
	map : {
		bundleName : "Map",
		bundleDescription : "The Map bundle manages the main map and all map content information. The client can abstract the services used in a hierarchical Map model so that they can be displayed in various ways to the user.",
		ui : {
			nodeUpdateError : {
				info : "Update of service '${title}' failed! Msg: ${errorMsg}",
				detail : "Update of service '${title}' failed! Msg: ${url} ${errorMsg} - Details: ${error}"
			},
			sliderLabels : {
				country : "Country",
				region : "Region",
				town : "Town"
			}
		},
		drawTooltips : {
			addPoint : "Click in the map",
			addMultipoint : "Click to start",
			finishLabel : "Finish"
		}
	},
	mobiletoc : {
		bundleName : "Mobile TOC",
		bundleDescription : "This bundle provides a mobile table of contents",
		tool : {
			title : "Map Content",
			tooltip : "Turn on/off Map Content"
		},
		ui : {
			basemaps : "Base maps",
			operationalLayers : "Operational layers",
			switchUI : {
				noTitle : "no title",
				leftLabel : "On",
				rightLabel : "Off"
			}
		}
	},
	mobileview : {
		bundleName : "Mobile View",
		bundleDescription : "Contains special classes that fix some issues concerning dojox.mobile"
	},
	notifier : {
		bundleName : "Notifier",
		bundleDescription : "All messages on status, progress, errors and warnings are shown in a popup message so that a user can understand what is going on in the application.",
		ui : {
			title : "Notifier",
			tooltips : {
				close : "Close this message",
				glue : "Pin this message"
			}
		}
	},
	parametermanager : {
		bundleName : "Parameter Manager",
		bundleDescription : "The Parameter Manager is responsible to delegate parameters from the URL to the according components on startup.",
		ui : {
			encoderBtn : "Link tool",
			encoderBtnTooltip : "Link tool",
			sendMail : "EMail",
			refresh : "Refresh",
			linkBoxTitle : "Link URL",
			size : "Size (Pixels)",
			codeBoxTitle : "Code to embed in HTML",
			qrCode : "QRCode",
			mailBody : "${url}",
			mailSubject : "Check out this map!",
			options : {
				small : "small (480 x 320)",
				medium : "medium (640 x 480)",
				large : "large (1280 x 1024)",
				custom : "custom"
			}
		}
	},
	printing : {
		bundleName : "Print",
		bundleDescription : "This bundle provides a print tool to print the map.",
		tool : {
			title : "Print",
			tooltip : "Print",
			back : "Back"
		},
		resultWin : {
			title : "Print"
		}
	},
	qrcode : {
		bundleName : "QRCode",
		bundleDescription : "This bundle provides a service that can be used to create QR codes.",
		errorMessage : "QR Code can not be generated."
	},
	splashscreen : {
		bundleName : "Splash Screen",
		bundleDescription : "The Splash Screen shows a progress bar and a logo (e.g. the map.apps logo) and/or a text while starting the application.",
		loadTitle : "Starting Application '{appName}'",
		loadBundle : "{percent}% - {startedBundles}/{maxBundlesToStart} - Load {name} "
	},
	system : {
		bundleName : "System",
		bundleDescription : "--CORE FUNCTIONALITY-- The System describes the basic core functionality (compilable skeleton) of map.apps."
	},
	templatelayout : {
		bundleName : "Template Layout",
		bundleDescription : "--CORE FUNCTIONALITY-- The Template Layout implements the arrangement of all UI elements based on predefined templates."
	},
	templates : {
		bundleName : "View",
		bundleDescription : "--CORE FUNCTIONALITY-- Views are used by the Template Layout.",
		templates : {
			desktop : {
				title : "Desktop",
				desc : "The desktop style layout"
			},
			modern : {
				title : "Modern",
				desc : "A modern layout"
			},
			minimal : {
				title : "Minimal",
				desc : "A minimal layout"
			}
		},
		ui : {
			selectorLabelTitle : "View"
		}
	},
	themes : {
		bundleName : "Style",
		bundleDescription : "--CORE FUNCTIONALITY-- The Themes bundle manages all CSS information like colors, font styles, background images etc. Users can switch between different looks of the user interface elements by selecting different styles.",
		themes : {
			pure : {
				desc : "The map.apps 'pure' style."
			},
			night : {
				desc : "The map.apps 'night' style."
			}
		},
		ui : {
			selectorLabelTitle : "Style"
		}
	},
	windowmanager : {
		bundleName : "Window Manager",
		bundleDescription : "--CORE FUNCTIONALITY-- The Window Manager is responsible to manage all floating windows.",
		ui : {
			defaultWindowTitle : "Window",
			closeBtn : {
				title : "Close"
			},
			minimizeBtn : {
				title : "Minimize"
			},
			maximizeBtn : {
				title : "Maximize"
			},
			restoreBtn : {
				title : "Restore"
			},
			opacityBtn : {
				title : "Change transparency"
			},
			collapseBtn : {
				title : "Hide content"
			},
			loading : {
				title : "Please wait!",
				message : "Loading..."
			},
			okcancel : {
				title : "Question",
				okButton : "OK",
				cancelButton : "Cancel"
			}
		}
	},
	ec_legend : {
		bundleName : "Legend\u0103",
		bundleDescription : "This bundle provides the Esri legend.",
		tool : {
			title : "Legend\u0103",
			tooltip : "Legend\u0103"
		}
	},
	ec_parameter_interceptor : {
		bundleName : "ec-parameter-interceptor",
		bundleDescription : "ec-parameter-interceptor",
		errors : {
			missingQueryParameter : "Missing query parameters",
			missingTypeQueryParameter : "Missing type query parameter",
			missingDatasetOrUrlQueryParameter : "Missing dataset or url query parameter",
			noComponentContextProvided : "No componentContext provided",
			noBundleContextProvided : "No bundleContext provided",
			noQueryParametersProvided : "No query parameters provided",
			datasetTypeNotSupported : "The type '${type}' is not supported",
			invalidGeoJSON : "Invalid GeoJSON",
			unsupportedCRSType : "Only named CRS are supported",
			canNotParseNamedCRS : "Can not parse named CRS to WKID: ${name}",
			mixingOfDifferentCRS : "Mixing of different CRS is not supported",
			invalidGeoJSONType : "Unknown GeoJSON type attribute: ${type}",
			typeNotFoundInResource : "No appropriate format for ${type} found in resource",
			couldNotLoadDataset : "Could not load dataset: ${cause}",
			unableToAddDataset : "Unable to add dataset: ${cause}",
			unableToParseCapabilities : "Unable to parse WMS Capabilities response",
			canNotDetermineUrl : "Could not determine the download URL of the resource"
		}
	},
	ec_feature_info : {
		tool : {
			title : "Informa\u021Bii detaliate",
			tooltip : "Informa\u021Bii detaliate"
		}
	},
	ec_map_loading_screen : {
		bundleName : "ec-map-loading-screen",
		bundleDescription : "ec-map-loading-screen",
		messages : {
			initializingMap : "Harta se \u00EEncarc\u0103..."
		}
	},
	ec_wms_layer_selector : {
		bundleName : "ec-wms-layer-selector",
		bundleDescription : "ec-wms-layer-selector",
		ui : {
			windowTitle : "Selectare straturi",
			hint : "Serviciul WMS selectat con\u021Bine ${countToProvideSelection} strat(uri), v\u0103 rug\u0103m s\u0103 selecta\u021Bi toate straturile care dori\u021Bi s\u0103 fie afi\u0219ate de hart\u0103.",
			selectAll : "Selectare tot",
			closeButton : "OK"
		},
		tool : {
			title : "Strat",
			tooltip : "Strat"
		}
	},
	ec_error_messages : {
		bundleName : "ec-error-messages",
		bundleDescription : "ec-error-messages",
		general : {
			serviceNotAvailable : "Serviciul solicitat nu este disponibil",
			serviceNotSupported : "Serviciul solicitat nu este acceptat",
			invalidResource : "Acest URL con\u021Bine o resurs\u0103 nevalid\u0103",
			unsupportedResource : "Acest URL con\u021Bine o resurs\u0103 neacceptat\u0103",
			errorSource : "S-a identificat urm\u0103toarea surs\u0103 a erorii",
			requestedUrl : "URL solicitat",
			unsupportedServiceType : "Furnizat tipul de serviciu nu este acceptat",
			technicalSupport : "Dac\u0103 problema persist\u0103, v\u0103 rug\u0103m s\u0103 contacta\u021Bi serviciul de asisten\u021B\u0103 tehnic\u0103",
			technicalSupportCreateTicket : "Create support ticket",
			weWillContactProvider : "Vom contacta furnizorul serviciului \u0219i \u00EEi vom raporta problema.",
			closeButton : "OK"
		},
		detailedErrors : {
			crs : "Serviciul nu ofer\u0103 un sistem acceptat de coordonate de referin\u021B\u0103",
			encoding : "Serviciul nu ofer\u0103 o codificare acceptat\u0103 a caracterelor",
			metadata : "Documentul de metadate al serviciului este eronat",
			fileFormat : "Acest URL a indicat un format de date nevalid",
			dataRequest : "La interogarea datelor serviciului, s-a primit un r\u0103spuns nevalid. Motivele pot fi, de exemplu: o eroare intern\u0103 a serverului, o configura\u021Bie gre\u0219it\u0103 a serviciului, un comportament neconform cu standardele al serviciului etc."
		},
		httpIssues : {
			303 : "R\u0103spunsul la solicitare poate fi g\u0103sit sub un alt URI",
			305 : "Resursa solicitat\u0103 este disponibil\u0103 doar printr-un proxy",
			400 : "Serverul nu poate sau nu va procesa solicitarea din cauza unei erori de client percepute",
			401 : "Resursa solicitat\u0103 a necesitat o autorizare",
			403 : "Resursa solicitat\u0103 nu este accesibil\u0103 publicului",
			404 : "Resursa solicitat\u0103 nu a putut fi g\u0103sit\u0103",
			405 : "Resursa a fost solicitat\u0103 printr-o metod\u0103 HTTP nepermis\u0103",
			406 : "S-a solicitat un con\u021Binut neacceptabil din partea resursei",
			407 : "Resursa necesit\u0103 o autentificare prealabil\u0103 printr-un serviciu proxy",
			500 : "Resursa nu este disponibil\u0103 din cauza unei erori interne a serverului",
			501 : "Resursa nu este disponibil\u0103 din cauza unei metode de solicitare nerecunoscute",
			502 : "Resursa nu este disponibil\u0103 din cauza unui gateway configurat gre\u0219it",
			503 : "Resursa nu este disponibil\u0103 din cauza suprasolicit\u0103rii sau mentenan\u021Bei",
			504 : "Resursa nu este disponibil\u0103 din cauza unui gateway configurat gre\u0219it sau care nu r\u0103spunde",
			505 : "Resursa nu accept\u0103 versiunea HTTP utilizat\u0103",
			generic : "A ap\u0103rut o eroare de server \u00EEn momentul solicit\u0103rii resursei"
		}
	},
	ec_user_tutorial : {
		bundleName : "ec-user-tutorial",
		bundleDescription : "ec-user-tutorial",
		externalResourceDisclaimer : {
			introduction : "Acest vizualizator de h\u0103r\u021Bi acceseaz\u0103 servicii externe \u0219i afi\u0219eaz\u0103 datele acestora. Comisia European\u0103 nu r\u0103spunde de mentenan\u021Ba acestor servicii, astfel c\u0103 nu poate influen\u021Ba disponibilitatea sau stabilitatea lor. Pot ap\u0103rea urm\u0103toarele probleme:",
			rotatingCircle : "Un cerc rotitor \u00EEn modul de vizualizare a h\u0103r\u021Bii indic\u0103 faptul c\u0103 vizualizatorul a\u0219teapt\u0103 un r\u0103spuns din partea unei surse de date. Cauzele pot fi un server indisponibil sau probleme de re\u021Bea.",
			layerZoomLevel : "Este posibil ca unele surse de straturi s\u0103 nu fie afi\u0219ate la anumite niveluri de panoramare. Poate fi rezultatul unor restric\u021Bii impuse de furnizorul serviciului extern. Ar putea fi nevoie s\u0103 mic\u0219ora\u021Bi sau s\u0103 m\u0103ri\u021Bi harta pentru a afi\u0219a datele stratului.",
			layerPanning : "Datele accesate de pe un server extern pot fi reprezentate \u00EEn afara vizualiz\u0103rii actuale a h\u0103r\u021Bii. Vizualizatorul de h\u0103r\u021Bi \u00EEncearc\u0103 s\u0103 identifice automat spa\u021Biul acoperit. Totu\u0219i, \u00EEn unele cazuri, metadatele sunt incomplete sau eronate, astfel c\u0103 trebuie s\u0103 panorama\u021Bi la acoperirea efectiv\u0103.",
			detailInformation : "Dac\u0103 solicita\u021Bi informa\u021Bii detaliate pentru o func\u021Bionalitate selectat\u0103, acestea vor fi tratate printr-o solicitare specific\u0103 adresat\u0103 serverului h\u0103r\u021Bii. Dac\u0103 r\u0103spunsul la aceast\u0103 solicitare este malformat sau gol (de ex. din cauza unei erori de server), informa\u021Biile nu vor putea fi afi\u0219ate \u0219i va ap\u0103rea o fereastr\u0103 goal\u0103.",
			mapScaling : "H\u0103r\u021Bile de baz\u0103 folosite \u00EEn aceast\u0103 aplica\u021Bie sunt furnizate de Oficiul pentru Statistic\u0103 al Uniunii Europene (Eurostat). Aceste h\u0103r\u021Bi sunt disponibile momentan la o rezolu\u021Bie de p\u00E2n\u0103 la 1:50.000. Mai multe h\u0103r\u021Bi de baz\u0103 cu rezolu\u021Bii mai fine sunt \u00EEn curs de preg\u0103tire (de ex. pentru a sprijini vizaulizarea datelor din cadrul unui ora\u0219). Prin urmare, este posibil ca unele surse de date s\u0103 nu fie afi\u0219ate cu o hart\u0103 de fond optim\u0103.",
			technicalSupport : "Dac\u0103 \u00EEnt\u00E2mpina\u021Bi orice alte probleme, nu ezita\u021Bi s\u0103 contacta\u021Bi serviciul de asisten\u021B\u0103 tehnic\u0103:",
			technicalSupportContactLink : "Contacta\u021Bi serviciul de asisten\u021B\u0103 tehnic\u0103",
			mapLoading : "Harta se \u00EEncarc\u0103...",
			donotshow : "Nu mai afi\u0219a acest ecran de \u00EEnt\u00E2mpinare."
		},
		legalNotice : "Denumirile utilizate \u0219i prezentarea materialelor pe aceast\u0103 hart\u0103 nu implic\u0103 exprimarea vreunei opinii din partea Uniunii Europene referitoare la statutul juridic al oric\u0103rei \u021B\u0103ri sau regiuni, al oric\u0103rui teritoriu sau ora\u0219 sau al autorit\u0103\u021Bilor sale ori referitoare la delimitarea frontierelor sau limitelor sale. Kosovo*: Aceast\u0103 desemnare nu aduce atingere pozi\u021Biilor privind statutul \u0219i este \u00EEn concordan\u021B\u0103 cu RCSONU 1244/1999 \u0219i cu Avizul CIJ privind declararea independen\u021Bei de c\u0103tre Kosovo. Palestina*: Aceast\u0103 desemnare nu trebuie considerat\u0103 o recunoa\u0219tere a Statului Palestinian \u0219i nu aduce atingere pozi\u021Biilor individuale ale statelor membre \u00EEn aceast\u0103 privin\u021B\u0103.",
		legalNoticeHeader : "Aviz juridic",
		tutorial : {
			welcome : "Bine a\u021Bi venit la vizualizatorul de h\u0103r\u021Bi al Portalului european de date!",
			familiarise : "Aceast\u0103 scurt\u0103 introducere este menit\u0103 s\u0103 v\u0103 familiarizeze cu elementele \u0219i func\u021Bionalitatea vizualizatorului de h\u0103r\u021Bi.",
			navigation : "Pentru a mi\u0219ca harta, da\u021Bi clic \u0219i \u021Bine\u021Bi ap\u0103sat butonul st\u00E2ng al mouse-ului.",
			zoom : "Aceste butoane v\u0103 permit s\u0103 schimba\u021Bi gradul de panoramare a h\u0103r\u021Bii. Alternativ, pute\u021Bi folosi roti\u021Ba mouse-ului pentru ajustarea panoram\u0103rii.",
			features : "Aceste butoane v\u0103 ofer\u0103 acces la func\u021Bionalit\u0103\u021Bi suplimentare. Ele servesc la activarea sau dezactivarea func\u021Bionalit\u0103\u021Bilor respective.",
			legend : "Legenda poate fi folosit\u0103 pentru a examina straturile disponibile ale h\u0103r\u021Bii \u0219i pentru a activa/dezactiva afi\u0219area lor pe hart\u0103. Denumirile sunt derivate direct din serviciile accesate extern.",
			transparency : "Pute\u021Bi ajusta \u0219i transparen\u021Ba unui strat.",
			featureinfo : "Unele date pot fi examinate mai detaliat. Atunci c\u00E2nd aceast\u0103 func\u021Bie este activat\u0103, pute\u021Bi da clic pe hart\u0103 pentru a vedea informa\u021Bii suplimentare.",
			done : "Terminat",
			okButton : "OK",
			closeButton : "\u00CEnchidere",
			skip : "Ignorare",
			next : "Pagina urm\u0103toare",
			back : "Pagina precedent\u0103"
		}
	}
});