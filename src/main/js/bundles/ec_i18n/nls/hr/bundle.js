define({
	contentviewer : {
		bundleName : "Content Viewer",
		bundleDescription : "Displays different kinds of content.",
		ui : {
			unknownContentError : "The content is unknown.",
			graphicinfotool : {
				title : "Item Info",
				desc : "Item Info"
			},
			content : {
				defaultTitle : "Item",
				grid : {
					detailView : "Show details",
					key : "Property",
					value : "Value"
				},
				customTemplate : {
					detailView : "Show details"
				},
				attachment : {
					noAttachmentSupport : "This layer does not offer attachment support",
					detailView : "Open detail view"
				},
				AGSDetail : {
					title : "Detail View",
					print : "Print",
					serviceMetadata : "Service metadata",
					serviceURL : "URL",
					serviceCopyrights : "Copyrights",
					serviceTitle : "Title",
					pager : {
						pageSizeLabel : "Feature ${currentPage} of ${endPage}"
					},
					key : "Property",
					value : "Value",
					detailView : "Show details"
				}
			}
		}
	},
	coordinatetransformer : {
		bundleName : "Coordinate Transformer",
		bundleDescription : "--CORE FUNCTIONALITY-- The Coordinate Transformer transforms geometries from a source coordinate system to a target coordinate system."
	},
	featureinfo : {
		bundleName : "FeatureInfo",
		bundleDescription : "FeatureInfo displays information on features for active layers.",
		ui : {
			featureInfo : {
				noQueryLayersFound : "No queryable layers found!",
				contentInfoWindowTitle : "Identify",
				noResultsFound : "No results found.",
				loadingInfoText : "Querying layer ${layerName} (${layerIndex} of ${layersTotal}).",
				featureInfoTool : "Identify",
				layer : "Layer",
				feature : "Feature"
			},
			wms : {
				emptyResult : "No results found on the WMS layer '${layerTitle}'."
			}
		}
	},
	geometryservice : {
		bundleName : "GeometryService",
		bundleDescription : "The bundle provides a central geometry service."
	},
	geojson : {
		bundleName : "GeoJson",
		bundleDescription : "This bundle provides a conversion service for geojson and well-known text formats"
	},
	infoviewer : {
		bundleName : "Info Viewer",
		bundleDescription : "The Info Viewer shows content information for a location in a moveable and resizable window.",
		ui : {
			title : "Info Viewer",
			tools : {
				closeInfoWindowMapdeskTool : "Close",
				closeInfoWindowMapTool : "Close",
				focusMapTool : "Center in Map",
				attachToGeorefTool : "Attach to position",
				mainActivationTool : "Location Information"
			}
		}
	},
	languagetoggler : {
		bundleName : "Language Toggler",
		bundleDescription : "The language of the user interface can be switched by a language toggler.",
		ui : {
			title : "Language"
		}
	},
	map : {
		bundleName : "Map",
		bundleDescription : "The Map bundle manages the main map and all map content information. The client can abstract the services used in a hierarchical Map model so that they can be displayed in various ways to the user.",
		ui : {
			nodeUpdateError : {
				info : "Update of service '${title}' failed! Msg: ${errorMsg}",
				detail : "Update of service '${title}' failed! Msg: ${url} ${errorMsg} - Details: ${error}"
			},
			sliderLabels : {
				country : "Country",
				region : "Region",
				town : "Town"
			}
		},
		drawTooltips : {
			addPoint : "Click in the map",
			addMultipoint : "Click to start",
			finishLabel : "Finish"
		}
	},
	mobiletoc : {
		bundleName : "Mobile TOC",
		bundleDescription : "This bundle provides a mobile table of contents",
		tool : {
			title : "Map Content",
			tooltip : "Turn on/off Map Content"
		},
		ui : {
			basemaps : "Base maps",
			operationalLayers : "Operational layers",
			switchUI : {
				noTitle : "no title",
				leftLabel : "On",
				rightLabel : "Off"
			}
		}
	},
	mobileview : {
		bundleName : "Mobile View",
		bundleDescription : "Contains special classes that fix some issues concerning dojox.mobile"
	},
	notifier : {
		bundleName : "Notifier",
		bundleDescription : "All messages on status, progress, errors and warnings are shown in a popup message so that a user can understand what is going on in the application.",
		ui : {
			title : "Notifier",
			tooltips : {
				close : "Close this message",
				glue : "Pin this message"
			}
		}
	},
	parametermanager : {
		bundleName : "Parameter Manager",
		bundleDescription : "The Parameter Manager is responsible to delegate parameters from the URL to the according components on startup.",
		ui : {
			encoderBtn : "Link tool",
			encoderBtnTooltip : "Link tool",
			sendMail : "EMail",
			refresh : "Refresh",
			linkBoxTitle : "Link URL",
			size : "Size (Pixels)",
			codeBoxTitle : "Code to embed in HTML",
			qrCode : "QRCode",
			mailBody : "${url}",
			mailSubject : "Check out this map!",
			options : {
				small : "small (480 x 320)",
				medium : "medium (640 x 480)",
				large : "large (1280 x 1024)",
				custom : "custom"
			}
		}
	},
	printing : {
		bundleName : "Print",
		bundleDescription : "This bundle provides a print tool to print the map.",
		tool : {
			title : "Print",
			tooltip : "Print",
			back : "Back"
		},
		resultWin : {
			title : "Print"
		}
	},
	qrcode : {
		bundleName : "QRCode",
		bundleDescription : "This bundle provides a service that can be used to create QR codes.",
		errorMessage : "QR Code can not be generated."
	},
	splashscreen : {
		bundleName : "Splash Screen",
		bundleDescription : "The Splash Screen shows a progress bar and a logo (e.g. the map.apps logo) and/or a text while starting the application.",
		loadTitle : "Starting Application '{appName}'",
		loadBundle : "{percent}% - {startedBundles}/{maxBundlesToStart} - Load {name} "
	},
	system : {
		bundleName : "System",
		bundleDescription : "--CORE FUNCTIONALITY-- The System describes the basic core functionality (compilable skeleton) of map.apps."
	},
	templatelayout : {
		bundleName : "Template Layout",
		bundleDescription : "--CORE FUNCTIONALITY-- The Template Layout implements the arrangement of all UI elements based on predefined templates."
	},
	templates : {
		bundleName : "View",
		bundleDescription : "--CORE FUNCTIONALITY-- Views are used by the Template Layout.",
		templates : {
			desktop : {
				title : "Desktop",
				desc : "The desktop style layout"
			},
			modern : {
				title : "Modern",
				desc : "A modern layout"
			},
			minimal : {
				title : "Minimal",
				desc : "A minimal layout"
			}
		},
		ui : {
			selectorLabelTitle : "View"
		}
	},
	themes : {
		bundleName : "Style",
		bundleDescription : "--CORE FUNCTIONALITY-- The Themes bundle manages all CSS information like colors, font styles, background images etc. Users can switch between different looks of the user interface elements by selecting different styles.",
		themes : {
			pure : {
				desc : "The map.apps 'pure' style."
			},
			night : {
				desc : "The map.apps 'night' style."
			}
		},
		ui : {
			selectorLabelTitle : "Style"
		}
	},
	windowmanager : {
		bundleName : "Window Manager",
		bundleDescription : "--CORE FUNCTIONALITY-- The Window Manager is responsible to manage all floating windows.",
		ui : {
			defaultWindowTitle : "Window",
			closeBtn : {
				title : "Close"
			},
			minimizeBtn : {
				title : "Minimize"
			},
			maximizeBtn : {
				title : "Maximize"
			},
			restoreBtn : {
				title : "Restore"
			},
			opacityBtn : {
				title : "Change transparency"
			},
			collapseBtn : {
				title : "Hide content"
			},
			loading : {
				title : "Please wait!",
				message : "Loading..."
			},
			okcancel : {
				title : "Question",
				okButton : "OK",
				cancelButton : "Cancel"
			}
		}
	},
	ec_legend : {
		bundleName : "Legenda",
		bundleDescription : "This bundle provides the Esri legend.",
		tool : {
			title : "Legenda",
			tooltip : "Legenda"
		}
	},
	ec_parameter_interceptor : {
		bundleName : "ec-parameter-interceptor",
		bundleDescription : "ec-parameter-interceptor",
		errors : {
			missingQueryParameter : "Missing query parameters",
			missingTypeQueryParameter : "Missing type query parameter",
			missingDatasetOrUrlQueryParameter : "Missing dataset or url query parameter",
			noComponentContextProvided : "No componentContext provided",
			noBundleContextProvided : "No bundleContext provided",
			noQueryParametersProvided : "No query parameters provided",
			datasetTypeNotSupported : "The type '${type}' is not supported",
			invalidGeoJSON : "Invalid GeoJSON",
			unsupportedCRSType : "Only named CRS are supported",
			canNotParseNamedCRS : "Can not parse named CRS to WKID: ${name}",
			mixingOfDifferentCRS : "Mixing of different CRS is not supported",
			invalidGeoJSONType : "Unknown GeoJSON type attribute: ${type}",
			typeNotFoundInResource : "No appropriate format for ${type} found in resource",
			couldNotLoadDataset : "Could not load dataset: ${cause}",
			unableToAddDataset : "Unable to add dataset: ${cause}",
			unableToParseCapabilities : "Unable to parse WMS Capabilities response",
			canNotDetermineUrl : "Could not determine the download URL of the resource"
		}
	},
	ec_feature_info : {
		tool : {
			title : "Informacije",
			tooltip : "Informacije"
		}
	},
	ec_map_loading_screen : {
		bundleName : "ec-map-loading-screen",
		bundleDescription : "ec-map-loading-screen",
		messages : {
			initializingMap : "Mapa se u\u010Ditava..."
		}
	},
	ec_wms_layer_selector : {
		bundleName : "ec-wms-layer-selector",
		bundleDescription : "ec-wms-layer-selector",
		ui : {
			windowTitle : "Odaberi slojeve",
			hint : "Odabrana WMS usluga sadrži ${countToProvideSelection} sloj, molimo odaberite sve slojeve koji bi trebali biti prikazani u mapi.",
			selectAll : "Odaberi sve",
			closeButton : "U redu"
		},
		tool : {
			title : "Sloj",
			tooltip : "Sloj"
		}
	},
	ec_error_messages : {
		bundleName : "ec-error-messages",
		bundleDescription : "ec-error-messages",
		general : {
			serviceNotAvailable : "Zatra\u017Eena usluga nije dostupna",
			serviceNotSupported : "Zatra\u017Eena usluga nije podr\u017Eana",
			invalidResource : "Neispravan resurs prona\u0111en je na tra\u017Eenom URL-u",
			unsupportedResource : "Nepodr\u017Eani resurs prona\u0111en je na tra\u017Eenom URL-u",
			errorSource : "Sljede\u0107i pogre\u0161ni izvor je odre\u0111en",
			requestedUrl : "Zatra\u017Eeni URL",
			unsupportedServiceType : "Unesena vrsta usluga nije podr\u017Eana",
			technicalSupport : "Ako se problem opet pojavi, kontaktirajte tehni\u010Dku podr\u0161ku",
			technicalSupportCreateTicket : "Create support ticket",
			weWillContactProvider : "Mi \u0107emo kontaktirati pru\u017Eatelja usluge i izvijestiti o problemu.",
			closeButton : "U redu"
		},
		detailedErrors : {
			crs : "Usluga ne pru\u017Ea podr\u017Eani koordinatni referentni sustav",
			encoding : "Usluga ne pru\u017Ea podr\u017Eano kodiranje znakova",
			metadata : "Dokument s metapodacima usluge je pogre\u0161an",
			fileFormat : "Neispravni oblik podataka primljen je u tra\u017Eenom URL-u",
			dataRequest : "Prilikom tra\u017Eenja podataka usluge primljen je neispravan odgovor. Razlozi toga mogu biti, primjerice: unutarnja gre\u0161ka servera, pogre\u0161na konfiguracija usluge, ne-standardno-slaganje pona\u0161anje usluge itd."
		},
		httpIssues : {
			303 : "Odgovor na zahtjev mo\u017Ee se prona\u0107i pod drugim URL-om",
			305 : "Tra\u017Eeni resurs dostupan je samo putem proxy poslu\u017Eitelja",
			400 : "Server ne mo\u017Ee ili ne\u0107e procesuirati zahtjev zbog percipirane gre\u0161ke klijenta",
			401 : "Tra\u017Eeni resurs zahtijeva provjeru autenti\u010Dnosti",
			403 : "Tra\u017Eeni resurs nije javno dostupan",
			404 : "Tra\u017Eeni zahtjev ne mo\u017Ee se prona\u0107i",
			405 : "Resurs je tra\u017Een putem nedozvoljene HTTP metode",
			406 : "Neprihvatljivi sadr\u017Eaj zatra\u017Een je od strane resursa",
			407 : "Resurs zahtijeva prethodnu provjeru autenti\u010Dnosti na proxy poslu\u017Eitelju",
			500 : "Resurs nije dostupan zbog unutarnje gre\u0161ke servera",
			501 : "Resurs nije dostupan zbog neprepoznatljive metode zahtjeva",
			502 : "Resurs nije dostupan zbog pogre\u0161no konfiguriranog ulaza",
			503 : "Resurs trenutno nije dostupan zbog preoptere\u0107enja ili odr\u017Eavanja",
			504 : "Resurs trenutno nije dostupan zbog pogre\u0161no konfiguriranog ulaza ili ulaza koji ne reagira",
			505 : "Resurs ne podr\u017Eava zadanu HTTP verziju",
			generic : "Pojavila se pogre\u0161ka servera prilikom tra\u017Eenja zadanog resursa"
		}
	},
	ec_user_tutorial : {
		bundleName : "ec-user-tutorial",
		bundleDescription : "ec-user-tutorial",
		externalResourceDisclaimer : {
			introduction : "Ovaj preglednik mape pristupa vanjskim uslugama i prikazuje njihove podatke. Te vanjske usluge ne odr\u017Eava Europska komisija i stoga nema utjecaj na njihovu dostupnost/stabilnost. Mogu se pojaviti sljede\u0107i problemi:",
			rotatingCircle : "Rotiraju\u0107i krug u pregledniku mape pokazuje da preglednik \u010Deka odgovor od izvora podataka. Tome mo\u017Ee biti uzrok nedostupni server ili problemi s mre\u017Eom.",
			layerZoomLevel : "Neki slojni izvori mo\u017Eda se ne\u0107e prikazivati na odre\u0111enim razinama pove\u0107avanja. Tome je tako zbog ograni\u010Denja koje je utvrdio vanjski pru\u017Eatelj usluga. Mo\u017Eda \u0107ete trebati pove\u0107ati ili smanjiti prikaz kako biste prikazali podatke sloja.",
			layerPanning : "Podaci kojima se pristupi iz vanjskog servera mogu biti locirani izvan trenutnog preglednika mape. Ovaj preglednik mape poku\u0161ava automatski prepoznati pokriveno podru\u010Dje. Ipak, u nekim slu\u010Dajevima metapodaci nisu potpuni ili su pogre\u0161ni pa morate kretati se do stvarne mjere.",
			detailInformation : "Ako tra\u017Eite detaljne informacije za odabranu mogu\u0107nost to \u0107e obaviti odre\u0111eni zahtjev serveru mape. Ako je odgovor na taj zahtjev krivi ili prazan (npr. zbog pogre\u0161ke servera) nikakve se informacije ne mogu prikazati i pojavit \u0107e se prazan prozor.",
			mapScaling : "Osnovne mape kori\u0161tene u ovoj aplikaciji pru\u017Ea statisti\u010Dki ured Europske unije (Eurostat). Te mape trenutno su dostupne u rezoluciji do 1:50,000. Detaljnije osnovne trenutno su u pripremi (npr. za potporu vizualizacije podataka unutar grada). Stoga se odre\u0111eni podaci jo\u0161 se mo\u017Eda ne\u0107e mo\u0107i prikazati s optimalnom pozadinskom mapom.",
			technicalSupport : "Ako se susretnete s nekim drugim problemima, ne oklijevajte kontaktirati tehni\u010Dku podr\u0161ku:",
			technicalSupportContactLink : "Kontaktiraj tehni\u010Dku podr\u0161ku",
			mapLoading : "Mapa se u\u010Ditava...",
			donotshow : "Vi\u0161e ne prikazuj ovaj ekran dobrodo\u0161lice."
		},
		legalNotice : "Primijenjene oznake i prezentacija materijala na ovoj mapi ne impliciraju izra\u017Eavanje bilo kojeg mi\u0161ljenja u ime Europske unije vezano uz pravni status bilo koje zemlje, teritorija, grada ili podru\u010Dja ili njihovih nadle\u017Enih tijela ili vezano uz razgrani\u010Denje svojih granica. Kosovo*: Ova oznaka ne dovodi u pitanje polo\u017Eajima o statusu i u skladu je s UNSCR 1244/1999 i ICJ Mi\u0161ljenjem o Kosovskoj deklaraciji neovisnosti. Palestina*: Ova se oznaka ne\u0107e tuma\u010Diti kao priznavanje Dr\u017Eave Palestine i ne dovodi u pitanje pojedina\u010Dna stajali\u0161ta dr\u017Eava \u010Dlanica o pitanju.",
		legalNoticeHeader : "Pravna Obavijest",
		tutorial : {
			welcome : "Dobrodo\u0161li na mapu Europskog portala podataka",
			familiarise : "Ovaj mali uvod ima za cilj upoznati vas s elementima i funkcioniranjem preglednika mape.",
			navigation : "Stisnite i dr\u017Eite lijevu tipku mi\u0161a kako biste se kretali na mapi.",
			zoom : "Te tipke mijenjaju razinu pove\u0107avanja na mapi. Isto tako, mo\u017Eete koristiti kota\u010Di\u0107 na mi\u0161u za prilago\u0111avanje pove\u0107avanja.",
			features : "Dodatna funkcionalnost dostupna je putem ovih tipki. One djeluju kao prekida\u010Di i omogu\u0107avaju ili onemogu\u0107avaju odre\u0111enu funkcionalnost.",
			legend : "Legenda se mo\u017Ee koristiti za istra\u017Eivanje dostupnih slojeva mape i omogu\u0107avanje ili onemogu\u0107avanje trenutnog prikaza na mapi. Imena su izravno izvu\u010Dena iz usluge kojoj se pristupilo izvana.",
			transparency : "Tako\u0111er mo\u017Eete prilagoditi transparentnost sloja.",
			featureinfo : "Neki podaci mogu se istra\u017Eiti i detaljnije. Kada je ova mogu\u0107nost aktivirana, mo\u017Eete kliknuti na mapu za potragu dodatnih informacija.",
			done : "Gotovo",
			okButton : "U redu",
			closeButton : "Zatvori",
			skip : "Presko\u010Di",
			next : "Sljede\u0107e",
			back : "Prethodno"
		}
	}
});