define({
	root : {
		contentviewer : {
			bundleName : "Content Viewer",
			bundleDescription : "Displays different kinds of content.",
			ui : {
				unknownContentError : "The content is unknown.",
				graphicinfotool : {
					title : "Item Info",
					desc : "Item Info"
				},
				content : {
					defaultTitle : "Item",
					grid : {
						detailView : "Show details",
						key : "Property",
						value : "Value"
					},
					customTemplate : {
						detailView : "Show details"
					},
					attachment : {
						noAttachmentSupport : "This layer does not offer attachment support",
						detailView : "Open detail view"
					},
					AGSDetail : {
						title : "Detail View",
						print : "Print",
						serviceMetadata : "Service metadata",
						serviceURL : "URL",
						serviceCopyrights : "Copyrights",
						serviceTitle : "Title",
						pager : {
							pageSizeLabel : "Feature ${currentPage} of ${endPage}"
						},
						key : "Property",
						value : "Value",
						detailView : "Show details"
					}
				}
			}
		},
		coordinatetransformer : {
			bundleName : "Coordinate Transformer",
			bundleDescription : "--CORE FUNCTIONALITY-- The Coordinate Transformer transforms geometries from a source coordinate system to a target coordinate system."
		},
		featureinfo : {
			bundleName : "FeatureInfo",
			bundleDescription : "FeatureInfo displays information on features for active layers.",
			ui : {
				featureInfo : {
					noQueryLayersFound : "No queryable layers found!",
					contentInfoWindowTitle : "Identify",
					noResultsFound : "No results found.",
					loadingInfoText : "Querying layer ${layerName} (${layerIndex} of ${layersTotal}).",
					featureInfoTool : "Identify",
					layer : "Layer",
					feature : "Feature"
				},
				wms : {
					emptyResult : "No results found on the WMS layer '${layerTitle}'."
				}
			}
		},
		geometryservice : {
			bundleName : "GeometryService",
			bundleDescription : "The bundle provides a central geometry service."
		},
		geojson : {
			bundleName : "GeoJson",
			bundleDescription : "This bundle provides a conversion service for geojson and well-known text formats"
		},
		infoviewer : {
			bundleName : "Info Viewer",
			bundleDescription : "The Info Viewer shows content information for a location in a moveable and resizable window.",
			ui : {
				title : "Info Viewer",
				tools : {
					closeInfoWindowMapdeskTool : "Close",
					closeInfoWindowMapTool : "Close",
					focusMapTool : "Center in Map",
					attachToGeorefTool : "Attach to position",
					mainActivationTool : "Location Information"
				}
			}
		},
		languagetoggler : {
			bundleName : "Language Toggler",
			bundleDescription : "The language of the user interface can be switched by a language toggler.",
			ui : {
				title : "Language"
			}
		},
		map : {
			bundleName : "Map",
			bundleDescription : "The Map bundle manages the main map and all map content information. The client can abstract the services used in a hierarchical Map model so that they can be displayed in various ways to the user.",
			ui : {
				nodeUpdateError : {
					info : "Update of service '${title}' failed! Msg: ${errorMsg}",
					detail : "Update of service '${title}' failed! Msg: ${url} ${errorMsg} - Details: ${error}"
				},
				sliderLabels : {
					country : "Country",
					region : "Region",
					town : "Town"
				}
			},
			drawTooltips : {
				addPoint : "Click in the map",
				addMultipoint : "Click to start",
				finishLabel : "Finish"
			}
		},
		mobiletoc : {
			bundleName : "Mobile TOC",
			bundleDescription : "This bundle provides a mobile table of contents",
			tool : {
				title : "Map Content",
				tooltip : "Turn on/off Map Content"
			},
			ui : {
				basemaps : "Base maps",
				operationalLayers : "Operational layers",
				switchUI : {
					noTitle : "no title",
					leftLabel : "On",
					rightLabel : "Off"
				}
			}
		},
		mobileview : {
			bundleName : "Mobile View",
			bundleDescription : "Contains special classes that fix some issues concerning dojox.mobile"
		},
		notifier : {
			bundleName : "Notifier",
			bundleDescription : "All messages on status, progress, errors and warnings are shown in a popup message so that a user can understand what is going on in the application.",
			ui : {
				title : "Notifier",
				tooltips : {
					close : "Close this message",
					glue : "Pin this message"
				}
			}
		},
		parametermanager : {
			bundleName : "Parameter Manager",
			bundleDescription : "The Parameter Manager is responsible to delegate parameters from the URL to the according components on startup.",
			ui : {
				encoderBtn : "Link tool",
				encoderBtnTooltip : "Link tool",
				sendMail : "EMail",
				refresh : "Refresh",
				linkBoxTitle : "Link URL",
				size : "Size (Pixels)",
				codeBoxTitle : "Code to embed in HTML",
				qrCode : "QRCode",
				mailBody : "${url}",
				mailSubject : "Check out this map!",
				options : {
					small : "small (480 x 320)",
					medium : "medium (640 x 480)",
					large : "large (1280 x 1024)",
					custom : "custom"
				}
			}
		},
		printing : {
			bundleName : "Print",
			bundleDescription : "This bundle provides a print tool to print the map.",
			tool : {
				title : "Print",
				tooltip : "Print",
				back : "Back"
			},
			resultWin : {
				title : "Print"
			}
		},
		qrcode : {
			bundleName : "QRCode",
			bundleDescription : "This bundle provides a service that can be used to create QR codes.",
			errorMessage : "QR Code can not be generated."
		},
		splashscreen : {
			bundleName : "Splash Screen",
			bundleDescription : "The Splash Screen shows a progress bar and a logo (e.g. the map.apps logo) and/or a text while starting the application.",
			loadTitle : "Starting Application '{appName}'",
			loadBundle : "{percent}% - {startedBundles}/{maxBundlesToStart} - Load {name} "
		},
		system : {
			bundleName : "System",
			bundleDescription : "--CORE FUNCTIONALITY-- The System describes the basic core functionality (compilable skeleton) of map.apps."
		},
		templatelayout : {
			bundleName : "Template Layout",
			bundleDescription : "--CORE FUNCTIONALITY-- The Template Layout implements the arrangement of all UI elements based on predefined templates."
		},
		templates : {
			bundleName : "View",
			bundleDescription : "--CORE FUNCTIONALITY-- Views are used by the Template Layout.",
			templates : {
				desktop : {
					title : "Desktop",
					desc : "The desktop style layout"
				},
				modern : {
					title : "Modern",
					desc : "A modern layout"
				},
				minimal : {
					title : "Minimal",
					desc : "A minimal layout"
				}
			},
			ui : {
				selectorLabelTitle : "View"
			}
		},
		themes : {
			bundleName : "Style",
			bundleDescription : "--CORE FUNCTIONALITY-- The Themes bundle manages all CSS information like colors, font styles, background images etc. Users can switch between different looks of the user interface elements by selecting different styles.",
			themes : {
				pure : {
					desc : "The map.apps 'pure' style."
				},
				night : {
					desc : "The map.apps 'night' style."
				}
			},
			ui : {
				selectorLabelTitle : "Style"
			}
		},
		windowmanager : {
			bundleName : "Window Manager",
			bundleDescription : "--CORE FUNCTIONALITY-- The Window Manager is responsible to manage all floating windows.",
			ui : {
				defaultWindowTitle : "Window",
				closeBtn : {
					title : "Close"
				},
				minimizeBtn : {
					title : "Minimize"
				},
				maximizeBtn : {
					title : "Maximize"
				},
				restoreBtn : {
					title : "Restore"
				},
				opacityBtn : {
					title : "Change transparency"
				},
				collapseBtn : {
					title : "Hide content"
				},
				loading : {
					title : "Please wait!",
					message : "Loading..."
				},
				okcancel : {
					title : "Question",
					okButton : "OK",
					cancelButton : "Cancel"
				}
			}
		},
		ec_legend : {
			bundleName : "Legend",
			bundleDescription : "This bundle provides the Esri legend.",
			tool : {
				title : "Legend",
				tooltip : "Legend"
			}
		},
		ec_parameter_interceptor : {
			bundleName : "ec-parameter-interceptor",
			bundleDescription : "ec-parameter-interceptor",
			errors : {
				missingQueryParameter : "Missing query parameters",
				missingTypeQueryParameter : "Missing type query parameter",
				missingDatasetOrUrlQueryParameter : "Missing dataset or url query parameter",
				noComponentContextProvided : "No componentContext provided",
				noBundleContextProvided : "No bundleContext provided",
				noQueryParametersProvided : "No query parameters provided",
				datasetTypeNotSupported : "The type '${type}' is not supported",
				invalidGeoJSON : "Invalid GeoJSON",
				unsupportedCRSType : "Only named CRS are supported",
				canNotParseNamedCRS : "Can not parse named CRS to WKID: ${name}",
				mixingOfDifferentCRS : "Mixing of different CRS is not supported",
				invalidGeoJSONType : "Unknown GeoJSON type attribute: ${type}",
				typeNotFoundInResource : "No appropriate format for ${type} found in resource",
				couldNotLoadDataset : "Could not load dataset: ${cause}",
				unableToAddDataset : "Unable to add dataset: ${cause}",
				unableToParseCapabilities : "Unable to parse WMS Capabilities response",
				canNotDetermineUrl : "Could not determine the download URL of the resource"
			}
		},
		ec_feature_info : {
			tool : {
				title : "Feature Info",
				tooltip : "Feature Info"
			}
		},
		ec_map_loading_screen : {
			bundleName : "ec-map-loading-screen",
			bundleDescription : "ec-map-loading-screen",
			messages : {
				initializingMap : "Initializing Map. Please stand by!"
			}
		},
		ec_wms_layer_selector : {
			bundleName : "ec-wms-layer-selector",
			bundleDescription : "ec-wms-layer-selector",
			ui : {
				windowTitle : "Select layer",
				hint : "The chosen WMS service contains ${countToProvideSelection} layer, please select all layers which should be displayed in the map.",
				selectAll : "Select all",
				closeButton : "OK"
			},
			tool : {
				title : "Layer",
				tooltip : "Layer"
			}
		},
		ec_error_messages : {
			bundleName : "ec-error-messages",
			bundleDescription : "ec-error-messages",
			general : {
				serviceNotAvailable : "The requested service is not available",
				serviceNotSupported : "The requested service is not supported",
				invalidResource : "An invalid resource was found at the given URL",
				unsupportedResource : "An unsupported resource was found at the given URL",
				errorSource : "The following error source was determined",
				requestedUrl : "Requested URL",
				unsupportedServiceType : "The provided service type is not supported",
				technicalSupport : "If the problem persists in the future, please contact the technical support",
				technicalSupportCreateTicket : "Create support ticket",
				weWillContactProvider : "We will contact the provider of the service and report the issue.",
				closeButton : "OK"
			},
			detailedErrors : {
				crs : "The service does not provide a supported coordinate reference system",
				encoding : "The service does not provide a supported character encoding",
				metadata : "The metadata document of the service is erroneous",
				fileFormat : "An invalid data format was received at the given URL",
				dataRequest : "While querying the data of the service an invalid response has been received. Reasons for this may be, for example: internal server error, wrong configuration of the service, non-standard-compliant behavior of the service, etc."
			},
			httpIssues : {
				303 : "The response to the request can be found under another URI",
				305 : "The requested resource is only available through a proxy",
				400 : "The server cannot or will not process the request due to a perceived client error",
				401 : "The requested resource required authorization",
				403 : "The requested resource is not available publicly",
				404 : "The requested resource could not be found",
				405 : "The resource was requested through a not allowed HTTP method",
				406 : "A not acceptable content was requested from the resource ",
				407 : "The resource requires a prior authentication at a proxy service",
				500 : "The resource is not available due to an internal server error",
				501 : "The resource is not available due to an unrecognized request method",
				502 : "The resource is not available due to a erroneously configured gateway",
				503 : "The resource is currently not available due to overload or maintenance",
				504 : "The resource is not available due to a erroneously configured or unresponsive gateway",
				505 : "The resource does not support the given HTTP version",
				generic : "A server error occurred while requesting the given resource"
			}
		},
		ec_user_tutorial : {
			bundleName : "ec-user-tutorial",
			bundleDescription : "ec-user-tutorial",
			externalResourceDisclaimer : {
				introduction : "This map viewer accesses external services and displays their data. These external services are not maintained by the European Commission and therefore we have no influence on their availability/stability. The following issues may occur:",
				rotatingCircle : "A rotating circle in the map view indicates that the viewer is waiting for a response from a data source. This may be caused by an unavailable server or network issues",
				layerZoomLevel : "Some layer sources may not display at certain zoom levels. This results from restrictions set by the external service provider. You may need to zoom in or out in order to display the data of the layer",
				layerPanning : "Data accessed from an external server may be located outside the current map view. This map viewer tries to automatically identify the covered area. Still, in some cases the metadata are not complete or erroneous so that you have to pan to the actual extent.",
				detailInformation : "If you request detailed information for a selected feature this will be handled by a specific request to the map server. If the response to this request is malformed or empty (e.g. due to a server error) no information can be displayed and an empty window will show up.",
				mapScaling : "The basemaps used in this application are provided by the statistical office of the European Union (Eurostat). These maps are currently available in a resolution of up to 1:50.000. More fine-grained basemaps are currently in preparation (e.g. to support the visualization of data within a city). Thus, certain data sources might not yet be displayed with an optimally suited background map.",
				technicalSupport : "If you encounter any other problems do not hesitate to get in touch with the technical support:",
				technicalSupportContactLink : "Contact technical support",
				mapLoading : "Map loading...",
				donotshow : "Do not display this welcome screen again."
			},
			legalNotice : "The designations employed and the presentation of material on this map do not imply the expression of any opinion whatsoever on the part of the European Union concerning the legal status of any country, territory, city or area or of its authorities, or concerning the delimitation of its frontiers or boundaries. Kosovo*: This designation is without prejudice to positions on status, and is in line with UNSCR 1244/1999 and the ICJ Opinion on the Kosovo declaration of independence. Palestine*: This designation shall not be construed as recognition of a State of Palestine and is without prejudice to the individual positions of the Member States on this issue.",
			legalNoticeHeader : "Legal information",
			tutorial : {
				welcome : "Welcome to the map viewer of the European Data Portal",
				familiarise : "This small introduction has the aim to familiarise you with the elements and functionality of the map viewer.",
				navigation : "Click and hold the left mouse button to pan on the map.",
				zoom : "These buttons change the zoom level on the map. Alternatively you can use the mouse wheel to adjust the zoom.",
				features : "Additional functionality is available through these buttons. They act as toggles and enable or disable the given functionality.",
				legend : "The legend can be used to examine the available map layers and enable or disable their current display on the map. The names are directly derived from the externally accessed service.",
				transparency : "You can also adjust the transparency of a layer.",
				featureinfo : "Some data can be examined even in more detail. When this feature is activated you can click on the map to query additional information.",
				done : "Done",
				okButton : "OK",
				closeButton : "Close",
				skip : "Skip",
				next : "Next",
				back : "Back"
			}
		}
	},
	bg : true,
	es : true,
	cs : true,
	da : true,
	de : true,
	el : true,
	et : true,
	fr : true,
	ga : true,
	hr : true,
	hu : true,
	it : true,
	lt : true,
	lv : true,
	mt : true,
	nl : true,
	pl : true,
	pt : true,
	ro : true,
	sk : true,
	sl : true,
	fi : true,
	sv : true
});
