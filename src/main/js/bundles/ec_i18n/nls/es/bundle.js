define({
	contentviewer : {
		bundleName : "Content Viewer",
		bundleDescription : "Muestra diferentes tipos de contenido.",
		ui : {
			unknownContentError : "El contenido es desconocido.",
			graphicinfotool : {
				title : "Informaci\u00f3n de elemento",
				desc : "Informaci\u00f3n de elemento"
			},
			content : {
				defaultTitle : "Elemento",
				grid : {
					detailView : "Mostrar detalles",
					key : "Propiedad",
					value : "Valor"
				},
				customTemplate : {
					detailView : "Mostrar detalles"
				},
				attachment : {
					noAttachmentSupport : "Esta capa no admite archivos adjuntos",
					detailView : "Mostrar detalles"
				},
				AGSDetail : {
					title : "Ver detalles",
					print : "Impresi\u00f3n",
					serviceMetadata : "Metadatos de servicio",
					serviceURL : "URL",
					serviceCopyrights : "Derechos de autor",
					serviceTitle : "T\u00edtulo",
					pager : {
						pageSizeLabel : "Caracter\u00edstica ${currentPage} de ${endPage}"
					},
					key : "Propiedad",
					value : "Valor",
					detailView : "Mostrar detalles"
				}
			}
		}
	},
	coordinatetransformer : {
		bundleName : "Coordinar transformador",
		bundleDescription : "--FUNCIONALIDAD b\u00e1sica-- La coordenada transformador transforma geometr\u00edas a partir de un sistema de coordenadas a otro."
	},
	featureinfo : {
		bundleName : "FeatureInfo",
		bundleDescription : "FeatureInfo muestra informaci\u00f3n sobre las caracter\u00edsticas de las capas activas.",
		ui : {
			featureInfo : {
				noQueryLayersFound : "Sin Capa b\u00fasquedas disponible!",
				contentInfoWindowTitle : "Identificar",
				noResultsFound : "No se encontraron resultados.",
				loadingInfoText : "Consulta capa ${layerName} (${layerIndex} de ${layersTotal}).",
				featureInfoTool : "Identificar",
				layer : "Capa",
				feature : "Objeto"
			},
			wms : {
				emptyResult : "No se encuentran en la capa '${layerTitle}' WMS resultados."
			}
		}
	},
	geometryservice : {
		bundleName : "GeometryService",
		bundleDescription : "El paquete proporciona un servicio de geometr\u00eda central."
	},
	geojson : {
		bundleName : "GeoJSON",
		bundleDescription : "Este paquete ofrece un servicio de conversi\u00f3n, que puede procesar GeoJSON y conocido texto (wkt)."
	},
	infoviewer : {
		bundleName : "Informaci\u00f3n de la pantalla",
		bundleDescription : "La pantalla de mensajes muestra la informaci\u00f3n sustantiva usuario acerca de un lugar en el mapa en una ventana.",
		ui : {
			title : "Info Visor",
			tools : {
				closeInfoWindowMapdeskTool : "Cerca",
				closeInfoWindowMapTool : "Cerca",
				focusMapTool : "Centro en Mapa",
				attachToGeorefTool : "Fije a la posici\u00f3n",
				mainActivationTool : "Informaci\u00f3n sobre la ubicaci\u00f3n"
			}
		}
	},
	languagetoggler : {
		bundleName : "Selector de idiomas",
		bundleDescription : "A trav\u00e9s del idioma de la interfaz de selecci\u00f3n de idiomas se puede cambiar.",
		ui : {
			title : "Idioma"
		}
	},
	map : {
		bundleName : "Mapa",
		bundleDescription : "El haz Mapa gestiona el mapa principal y toda la informaci\u00f3n Mapa contenido. El cliente puede abstraer los servicios utilizados en un modelo jer\u00e1rquico Mapa de modo que se pueden mostrar en diferentes formas al usuario.",
		ui : {
			nodeUpdateError : {
				info : "Actualizaci\u00f3n del servicio '${title}' fall\u00f3! Error: ${errorMsg}",
				detail : "Actualizaci\u00f3n del servicio '${title}' fall\u00f3! Error: ${url} ${errorMsg} - Detalle: ${error}"
			},
			sliderLabels : {
				country : "Pa\u00eds",
				region : "Regi\u00f3n",
				town : "Ciudad"
			}
		},
		drawTooltips : {
			addPoint : "Haga clic en el mapa",
			addMultipoint : "Haga clic para iniciar",
			finishLabel : "Acabado"
		}
	},
	mobiletoc : {
		bundleName : "Control de nivel m\u00f3vil",
		bundleDescription : "Este paquete proporciona un controlador de nivel m\u00f3vil listo",
		tool : {
			title : "Mapa contenido",
			tooltip : "Activar / desactivar mapa de contenidos"
		},
		ui : {
			basemaps : "Mapas base",
			operationalLayers : "Niveles Temas",
			switchUI : {
				noTitle : "ning\u00fan t\u00edtulo",
				leftLabel : "En",
				rightLabel : "Apagado"
			}
		}
	},
	mobileview : {
		bundleName : "Mobile View",
		bundleDescription : "Contiene clases especiales que fijan algunas cuestiones relativas dojox.mobile"
	},
	notifier : {
		bundleName : "Notificaciones",
		bundleDescription : "Todos los mensajes de estado, el progreso o error se muestran al usuario en un mensaje emergente para que est\u00e9 claro lo que est\u00e1 sucediendo en la aplicaci\u00f3n.",
		ui : {
			title : "Notificador",
			tooltips : {
				close : "Cerrar Mensaje",
				glue : "Adjuntar mensaje"
			}
		}
	},
	parametermanager : {
		bundleName : "La administraci\u00f3n de par\u00e1metros",
		bundleDescription : "El Administrador de par\u00e1metros es responsable de delegar par\u00e1metros de la URL para los componentes de acuerdo en el arranque.",
		ui : {
			encoderBtn : "Herramienta de Enlace",
			encoderBtnTooltip : "Herramienta de Enlace",
			sendMail : "E-mail",
			refresh : "Refrescar",
			linkBoxTitle : "Enlace URL",
			size : "Tama\u00f1o (p\u00edxeles)",
			codeBoxTitle : "El c\u00f3digo para incrustar en HTML",
			qrCode : "C\u00f3digo QR",
			mailBody : "${url}",
			mailSubject : "Mira, a veces esta tarjeta!",
			options : {
				small : "peque\u00f1o (480 x 320)",
				medium : "medio (640 x 480)",
				large : "grande (1280 x 1024)",
				custom : "definida por el usuario"
			}
		}
	},
	printing : {
		bundleName : "Impresi\u00f3n",
		bundleDescription : "Este paquete proporciona una herramienta de impresi\u00f3n para imprimir el mapa.",
		tool : {
			title : "Impresi\u00f3n",
			tooltip : "Impresi\u00f3n",
			back : "Espalda"
		},
		resultWin : {
			title : "Impresi\u00f3n"
		}
	},
	qrcode : {
		bundleName : "C\u00f3digo QR",
		bundleDescription : "Este paquete proporciona un servicio que puede ser usado para crear c\u00f3digos QR.",
		errorMessage : "Generaci\u00f3n de c\u00f3digo QR no tuvo \u00e9xito."
	},
	splashscreen : {
		bundleName : "Pantalla de inicio",
		bundleDescription : "La pantalla de inicio muestra una barra de progreso mientras se inicia la aplicaci\u00f3n.",
		loadTitle : "Iniciar aplicaci\u00f3n '{appName}'",
		loadBundle : "{percent}% - {startedBundles}/{maxBundlesToStart} - Carga {name}"
	},
	system : {
		bundleName : "Sistema",
		bundleDescription : "--FUNCIONALIDAD b\u00e1sica-- El sistema se describe la funcionalidad del n\u00facleo b\u00e1sico (esqueleto compilable) de map.apps."
	},
	templatelayout : {
		bundleName : "Ver Layout",
		bundleDescription : "--FUNCIONALIDAD b\u00e1sica-- la plantilla de dise\u00f1o implementa la disposici\u00f3n de todos los elementos de interfaz de usuario basado en plantillas predefinidas."
	},
	templates : {
		bundleName : "Vista",
		bundleDescription : "--FUNCIONALIDAD b\u00e1sica-- Vistas son utilizados por el dise\u00f1o de plantillas.",
		templates : {
			desktop : {
				title : "Escritorio",
				desc : "Un dise\u00f1o de escritorio orientada."
			},
			modern : {
				title : "Moderno",
				desc : "Un dise\u00f1o moderno y elegante."
			},
			minimal : {
				title : "M\u00ednimo",
				desc : "Un dise\u00f1o minimalista"
			}
		},
		ui : {
			selectorLabelTitle : "Vista"
		}
	},
	themes : {
		bundleName : "Estilo",
		bundleDescription : "--FUNCIONALIDAD b\u00e1sica-- El paquete Temas gestiona toda la informaci\u00f3n CSS como colores, estilos de fuente, im\u00e1genes de fondo, etc. Los usuarios pueden cambiar entre diferentes miradas de los elementos de la interfaz de usuario mediante la selecci\u00f3n de diferentes estilos.",
		themes : {
			pure : {
				desc : "Estilo Las map.apps 'puro'."
			},
			night : {
				desc : "Los map.apps 'noche' estilo."
			}
		},
		ui : {
			selectorLabelTitle : "Estilo"
		}
	},
	windowmanager : {
		bundleName : "Ventana Administraci\u00f3n",
		bundleDescription : "--FUNCIONALIDAD b\u00e1sica-- El gestor de ventanas es responsable de gestionar toda la ventana de di\u00e1logo.",
		ui : {
			defaultWindowTitle : "Ventana",
			closeBtn : {
				title : "Cerca"
			},
			minimizeBtn : {
				title : "Minimizar"
			},
			maximizeBtn : {
				title : "Maximizar"
			},
			restoreBtn : {
				title : "Restaurar"
			},
			opacityBtn : {
				title : "Cambiar la transparencia"
			},
			collapseBtn : {
				title : "Ocultar contenido"
			},
			loading : {
				title : "Por favor espera!",
				message : "Cargando ..."
			},
			okcancel : {
				title : "Pregunta",
				okButton : "Okay",
				cancelButton : "Cancelar"
			}
		}
	},
	ec_legend : {
		bundleName : "Leyenda",
		bundleDescription : "This bundle provides the Esri legend.",
		tool : {
			title : "Leyenda",
			tooltip : "Leyenda"
		}
	},
	ec_parameter_interceptor : {
		bundleName : "ec-parameter-interceptor",
		bundleDescription : "ec-parameter-interceptor",
		errors : {
			missingQueryParameter : "Missing query parameters",
			missingTypeQueryParameter : "Missing type query parameter",
			missingDatasetOrUrlQueryParameter : "Missing dataset or url query parameter",
			noComponentContextProvided : "No componentContext provided",
			noBundleContextProvided : "No bundleContext provided",
			noQueryParametersProvided : "No query parameters provided",
			datasetTypeNotSupported : "The type '${type}' is not supported",
			invalidGeoJSON : "Invalid GeoJSON",
			unsupportedCRSType : "Only named CRS are supported",
			canNotParseNamedCRS : "Can not parse named CRS to WKID: ${name}",
			mixingOfDifferentCRS : "Mixing of different CRS is not supported",
			invalidGeoJSONType : "Unknown GeoJSON type attribute: ${type}",
			typeNotFoundInResource : "No appropriate format for ${type} found in resource",
			couldNotLoadDataset : "Could not load dataset: ${cause}",
			unableToAddDataset : "Unable to add dataset: ${cause}",
			unableToParseCapabilities : "Unable to parse WMS Capabilities response",
			canNotDetermineUrl : "Could not determine the download URL of the resource"
		}
	},
	ec_feature_info : {
		tool : {
			title : "M\u00e1s informaci\u00f3n",
			tooltip : "M\u00e1s informaci\u00f3n"
		}
	},
	ec_map_loading_screen : {
		bundleName : "ec-map-loading-screen",
		bundleDescription : "ec-map-loading-screen",
		messages : {
			initializingMap : "El mapa est\u00e1 carg\u00e1ndose..."
		}
	},
	ec_wms_layer_selector : {
		bundleName : "ec-wms-layer-selector",
		bundleDescription : "ec-wms-layer-selector",
		ui : {
			windowTitle : "Seleccionar capas",
			hint : "El servicio WMS elegido contiene ${countToProvideSelection} capas; seleccione todas las que desea visualizar en el mapa.",
			selectAll : "Seleccionar todo",
			closeButton : "OK"
		},
		tool : {
			title : "Capas",
			tooltip : "Capas"
		}
	},
	ec_error_messages : {
		bundleName : "ec-error-messages",
		bundleDescription : "ec-error-messages",
		general : {
			serviceNotAvailable : "El servicio solicitado no est\u00e1 disponible",
			serviceNotSupported : "El servicio solicitado no est\u00e1 soportado",
			invalidResource : "Esta direcci\u00f3n web contiene un recurso no v\u00e1lido",
			unsupportedResource : "Esta direcci\u00f3n web contiene un recurso no compatible",
			errorSource : "Se ha identificado la siguiente causa de error",
			requestedUrl : "Direcci\u00f3n web solicitada",
			unsupportedServiceType : "El servicio solicitado no est\u00e1 soportado",
			technicalSupport : "Si el problema persiste, p\u00f3ngase en contacto con el servicio de asistencia t\u00e9cnica",
			technicalSupportCreateTicket : "Contactar con el servicio de asistencia t\u00e9cnica",
			weWillContactProvider : "Si el problema persiste, p\u00f3ngase en contacto con el servicio de asistencia t\u00e9cnica",
			closeButton : "OK"
		},
		detailedErrors : {
			crs : "El servicio no ofrece un sistema compatible de coordenadas de referencia",
			encoding : "El servicio no ofrece un sistema de codificaci\u00f3n de caracteres compatible",
			metadata : "El documento de metadatos del servicio es err\u00f3neo",
			fileFormat : "El formato de los datos proporcionados desde la direcci\u00f3n web no es v\u00e1lido",
			dataRequest : "Se ha recibido una respuesta no v\u00e1lida del servicio al solicitar los datos. Ello podr\u00eda deberse a un error interno del servidor, un error en la configuraci\u00f3n del servicio, un comportamiento del servicio no compatible con los est\u00e1ndares, etc."
		},
		httpIssues : {
			303 : "El resultado de la solicitud puede hallarse en otra direcci\u00f3n web",
			305 : "El recurso solicitado solo est\u00e1 disponible a trav\u00e9s de un proxy",
			400 : "El servidor no puede procesar o no autoriza el procesamiento de la solicitud por detectar un error del cliente",
			401 : "El recurso solicitado requiere autorizaci\u00f3n",
			403 : "El recurso solicitado no es de acceso p\u00fablico",
			404 : "No se ha encontrado el recurso solicitado",
			405 : "El recurso se ha solicitado a trav\u00e9s de un m\u00e9todo HTTP no autorizado",
			406 : "El recurso ha solicitado un contenido no aceptado",
			407 : "El recurso requiere previa autentificaci\u00f3n a trav\u00e9s de un servicio proxy",
			500 : "El recurso no est\u00e1 disponible debido a un error interno del servidor",
			501 : "El recurso no est\u00e1 disponible debido a que no se reconoce el m\u00e9todo de solicitud",
			502 : "El recurso no est\u00e1 disponible debido a un fallo en la configuraci\u00f3n de la puerta de enlace",
			503 : "El recurso no est\u00e1 disponible en estos momentos debido a una sobrecarga o a labores de mantenimiento",
			504 : "El recurso no est\u00e1 disponible debido a un fallo en la configuraci\u00f3n de la puerta de enlace o a que esta no responde",
			505 : "El recurso no es compatible con la versi\u00f3n de HTTP",
			generic : "Se ha producido un error de servidor durante la solicitud del recurso"
		}
	},
	ec_user_tutorial : {
		bundleName : "ec-user-tutorial",
		bundleDescription : "ec-user-tutorial",
		externalResourceDisclaimer : {
			introduction : "Este visualizador de mapas accede a servicios externos y muestra sus datos. La responsabilidad de mantener estos servicios externos no recae en la Comisi\u00f3n Europea y por tanto no depende de nosotros su disponibilidad/estabilidad. Pueden producirse los siguientes problemas:",
			rotatingCircle : "Un c\u00edrculo en movimiento giratorio sobre la vista del mapa indica que el visualizador est\u00e1 esperando la respuesta de la fuente de datos. Ello puede deberse a que el servidor no est\u00e1 disponible o a problemas en la red.",
			layerZoomLevel : "Es posible que algunas capas de informaci\u00f3n no se muestren en determinados niveles de zoom. Ello se debe a restricciones impuestas por el proveedor del servicio externo. Puede ser necesario acercar o alejar el mapa para visualizar los datos de la capa.",
			layerPanning : "Los datos a los que se accede desde un servidor externo pueden no encontrarse dentro de la vista actual del mapa. Este visualizador de mapas intenta identificar autom\u00e1ticamente el \u00e1rea contemplada. Tambi\u00e9n puede ocurrir que los metadatos no est\u00e9n completos o no sean correctos y deba navegar por la capa actual.",
			detailInformation : "Si solicita informaci\u00f3n detallada sobre una funci\u00f3n determinada, esta se tramitar\u00e1 mediante una solicitud espec\u00edfica al servidor de mapas. Si la respuesta a esta solicitud no est\u00e1 correctamente formada o carece de contenido (debido a un error del servidor) no se podr\u00e1 mostrar ninguna informaci\u00f3n y aparecer\u00e1 una ventana vac\u00eda.",
			technicalSupport : "Si tiene cualquier otro problema, por favor p\u00f3ngase en contacto con el servicio de asistencia t\u00e9cnica:",
			technicalSupportContactLink : "Contactar con el servicio de asistencia t\u00e9cnica",
			donotshow : "No volver a mostrar esta pantalla de bienvenida.",
			mapLoading : "El mapa est\u00e1 carg\u00e1ndose...",
			mapScaling : "Los mapas de base de esta aplicaci\u00f3n los facilita la Oficina Estad\u00edstica de la Uni\u00f3n Europea (Eurostat). Normalmente estos mapas ofrecen una resoluci\u00f3n de hasta 1:50.000. Se est\u00e1n elaborando mapas de base con mayor resoluci\u00f3n a fin de permitir, por ejemplo, la visualizaci\u00f3n de datos de una ciudad. Por tanto, es posible que algunas fuentes de datos no puedan mostrarse a\u00fan en un mapa de base \u00f3ptimo para ello."
		},
		legalNotice : "Las denominaciones utilizadas y la presentaci\u00f3n del contenido de este mapa no deben interpretarse en ning\u00fan caso como un reflejo de la opini\u00f3n de la Uni\u00f3n Europea con respecto a la condici\u00f3n jur\u00eddica de ning\u00fan pa\u00eds, territorio, ciudad o \u00e1rea ni de sus autoridades, ni en lo relativo a la demarcaci\u00f3n de sus fronteras o l\u00edmites. Kosovo*: esta denominaci\u00f3n no pretende entrar en conflicto con ninguna postura con respecto a la condici\u00f3n jur\u00eddica, y se ajusta a la resoluci\u00f3n 1244/1999 del Consejo de Seguridad de las Naciones Unidas y al dictamen de la CIJ con respecto a la declaraci\u00f3n de independencia de Kosovo. Territorios Palestinos*: esta denominaci\u00f3n no debe entenderse como un reconocimiento de un Estado de Palestina ni pretende entrar en conflicto con las posturas de los distintos Estados miembros sobre este asunto.",
		legalNoticeHeader : "Aviso legal",
		tutorial : {
			welcome : "Bienvenido/a al visualizador de mapas del Portal europeo de datos",
			familiarise : "El prop\u00f3sito de esta breve introducci\u00f3n es presentar los elementos y las funciones del visualizador de mapas.",
			navigation : "Mantenga pulsado el bot\u00f3n izquierdo del rat\u00f3n para moverse por el mapa.",
			zoom : "Estos botones permiten modificar el nivel de zoom del mapa. Tambi\u00e9n puede utilizar la rueda del rat\u00f3n para ajustar el zoom.",
			features : "Con estos botones puede acceder a funciones adicionales. Sirven para habilitar/deshabilitar las distintas funciones.",
			legend : "La leyenda permite examinar las capas del mapa e incluirlas o excluirlas de la vista activa. Los nombres son los proporcionados por el servicio externo.",
			transparency : "Tambi\u00e9n puede ajustar el nivel de transparencia de las capas.",
			featureinfo : "Algunos datos pueden examinarse incluso con mayor grado de detalle. Si esta funci\u00f3n est\u00e1 activada, puede pulsar sobre el mapa para obtener informaci\u00f3n adicional.",
			okButton : "OK",
			closeButton : "Cerrar",
			next : "Siguiente",
			back : "Anterior",
			skip : "Saltar",
			done : "Terminado"
		}
	}
});