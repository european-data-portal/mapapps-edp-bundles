define({
	contentviewer : {
		bundleName : "Content Viewer",
		bundleDescription : "Ein Viewer f\u00fcr unterschiedliche Arten von Inhalten.",
		ui : {
			unknownContentError : "Der Inhalt ist unbekannt.",
			graphicinfotool : {
				title : "Element Info",
				desc : "Element Info"
			},
			content : {
				defaultTitle : "Element",
				grid : {
					detailView : "Zeige Details",
					key : "Attribut",
					value : "Wert"
				},
				customTemplate : {
					detailView : "Zeige Details"
				},
				attachment : {
					noAttachmentSupport : "Diese Ebene hat keine Unterst\u00fctzung f\u00fcr Anh\u00e4nge",
					detailView : "Zeige Details"
				},
				AGSDetail : {
					title : "Detailansicht",
					print : "Drucken",
					serviceMetadata : "Service Metadaten",
					serviceURL : "URL",
					serviceCopyrights : "Copyrights",
					serviceTitle : "Titel",
					pager : {
						pageSizeLabel : "Geoobjekt ${currentPage} von ${endPage}"
					},
					key : "Attribut",
					value : "Wert",
					detailView : "Zeige Details"
				}
			}
		}
	},
	coordinatetransformer : {
		bundleName : "Koordinaten Transformator",
		bundleDescription : "--KERNFUNKTIONALIT\u00c4T-- Der Koordinaten Transformator transformiert Geometrien von einem Koordinatensystem in ein anderes."
	},
	featureinfo : {
		bundleName : "FeatureInfo",
		bundleDescription : "FeatureInfo zeigt FeatureInfos f\u00fcr alle aktivierten Layer an.",
		ui : {
			featureInfo : {
				noQueryLayersFound : "Keine durchsuchbaren Layer verf\u00fcgbar!",
				contentInfoWindowTitle : "Identifizieren",
				noResultsFound : "Keine Ergebnisse gefunden.",
				loadingInfoText : "Layer ${layerName} wird abgerufen (${layerIndex} von ${layersTotal}).",
				featureInfoTool : "Identifizieren",
				layer : "Ebene",
				feature : "Objekt"
			},
			wms : {
				emptyResult : "Der WMS-Layer '${layerTitle}' lieferte keine Ergebnisse."
			}
		}
	},
	geometryservice : {
		bundleName : "GeometryService",
		bundleDescription : "Das Modul bietet einen zentralen geometry service an."
	},
	geojson : {
		bundleName : "GeoJson",
		bundleDescription : "Dieses Bundle stellt einen Konvertierungsdienst bereit, der geojson and well-known text (wkt) verarbeiten kann."
	},
	infoviewer : {
		bundleName : "Informationsanzeige",
		bundleDescription : "Die Informationsanzeige zeigt dem Anwender inhaltliche Informationen zu einem Ort auf der Karte in einem Fenster.",
		ui : {
			title : "Info Viewer",
			tools : {
				closeInfoWindowMapdeskTool : "Schlie\u00dfen",
				closeInfoWindowMapTool : "Schlie\u00dfen",
				focusMapTool : "In Karte zentrieren",
				attachToGeorefTool : "An Position anheften",
				mainActivationTool : "Ortsinfo"
			}
		}
	},
	languagetoggler : {
		bundleName : "Sprachumschalter",
		bundleDescription : "Durch den Sprachumschalter kann die Sprache der Benutzeroberfl\u00e4che gewechselt werden.",
		ui : {
			title : "Sprache"
		}
	},
	map : {
		bundleName : "Karte",
		bundleDescription : "Das Karten Bundle stellt die Hauptkarte und alle Karteninhaltsinformationen zur Verf\u00fcgung.",
		ui : {
			nodeUpdateError : {
				info : "Aktualisierung des Dienstes '${title}' fehlgeschlagen! Fehler: ${errorMsg}",
				detail : "Aktualisierung des Dienstes '${title}' fehlgeschlagen! Fehler: ${url} ${errorMsg} - Detail: ${error}"
			},
			sliderLabels : {
				country : "Land",
				region : "Region",
				town : "Stadt"
			}
		},
		drawTooltips : {
			addPoint : "In die Karte klicken",
			addMultipoint : "In die Karte klicken um zu beginnen",
			finishLabel : "Beenden"
		}
	},
	mobiletoc : {
		bundleName : "Mobile Ebenensteuerung",
		bundleDescription : "Dieses Bundle stellt eine mobile Ebenensteuerung bereit",
		tool : {
			title : "Karteninhalt",
			tooltip : "Karteninhalt ein-/ausblenden"
		},
		ui : {
			basemaps : "Hintergrundkarten",
			operationalLayers : "Themenebenen",
			switchUI : {
				noTitle : "kein Titel",
				leftLabel : "An",
				rightLabel : "Aus"
			}
		}
	},
	mobileview : {
		bundleName : "Mobile View",
		bundleDescription : "Enthaelt Klassen, um einige Probleme mit dojox.mobile zu beheben."
	},
	notifier : {
		bundleName : "Benachrichtigungen",
		bundleDescription : "Alle Status-, Fortschritts- oder Fehlermeldungen werden dem Anwender in einer Popup Meldung angezeigt, so dass klar ist, was gerade in der Anwendung passiert.",
		ui : {
			title : "Notifier",
			tooltips : {
				close : "Nachricht schlie\u00dfen",
				glue : "Nachricht anheften"
			}
		}
	},
	parametermanager : {
		bundleName : "Parameter Verwaltung",
		bundleDescription : "Die Parameter Verwaltung ist beim Starten der Anwendung f\u00fcr die Weitergabe der Parameter aus der URL an die entsprechenden Komponenten verantwortlich.",
		ui : {
			encoderBtn : "Link Werkzeug",
			encoderBtnTooltip : "Link Werkzeug",
			sendMail : "E-Mail",
			refresh : "Aktualisieren",
			linkBoxTitle : "Link URL",
			size : "Gr\u00f6\u00dfe (Pixel)",
			codeBoxTitle : "Code zum Einbinden in HTML",
			qrCode : "QRCode",
			mailBody : "${url}",
			mailSubject : "Schau dir mal diese Karte an!",
			options : {
				small : "klein (480 x 320)",
				medium : "mittel (640 x 480)",
				large : "gro\u00df (1280 x 1024)",
				custom : "benutzerdefiniert"
			}
		}
	},
	printing : {
		bundleName : "Drucken",
		bundleDescription : "Stellt ein Druck-Werkzeug bereit um die Karte zu drucken",
		tool : {
			title : "Drucken",
			tooltip : "Drucken",
			back : "Zur\u00fcck"
		},
		resultWin : {
			title : "Drucken"
		}
	},
	qrcode : {
		bundleName : "QRCode",
		bundleDescription : "Dieses Bundle stellt einen Dienst zur Verf\u00fcgung um QR Codes zu erzeugen.",
		errorMessage : "QR Code Generierung war nicht erfolgreich."
	},
	splashscreen : {
		bundleName : "Startbildschirm",
		bundleDescription : "Der Startbildschirm zeigt einen Fortschrittsbalken w\u00e4hrend die Anwendung gestartet wird.",
		loadTitle : "Starte Anwendung '{appName}'",
		loadBundle : "{percent}% - {startedBundles}/{maxBundlesToStart} - Lade {name}"
	},
	system : {
		bundleName : "System",
		bundleDescription : "--KERNFUNKTIONALIT\u00c4T-- Das System beschreibt die Kernfunktionen von map.apps."
	},
	templatelayout : {
		bundleName : "Ansichts-Layout",
		bundleDescription : "--KERNFUNKTIONALIT\u00c4T-- Das Ansichts-Layout ordnet s\u00e4mtliche Elemente der Benutzeroberfl\u00e4che entsprechend vordefinierter Einstellungen an."
	},
	templates : {
		bundleName : "Ansicht",
		bundleDescription : "--KERNFUNKTIONALIT\u00c4T-- Die hier konfigurierten Ansichten werden vom Ansichts-Layout verwendet.",
		templates : {
			desktop : {
				title : "Desktop",
				desc : "Ein Desktop orientiertes Layout."
			},
			modern : {
				title : "Modern",
				desc : "Ein modernes stylisches Layout."
			},
			minimal : {
				title : "Minimal",
				desc : "Ein minimalistisches Layout"
			}
		},
		ui : {
			selectorLabelTitle : "Ansicht"
		}
	},
	themes : {
		bundleName : "Farbschemas",
		bundleDescription : "--KERNFUNKTIONALIT\u00c4T-- Dieses Bundle stellt alle CSS Informationen bereit, wie z.B: Farben.",
		themes : {
			pure : {
				desc : "Das map.apps 'pure' Farbschema."
			},
			night : {
				desc : "Das map.apps 'night' Farbschema."
			}
		},
		ui : {
			selectorLabelTitle : "Farbschema"
		}
	},
	windowmanager : {
		bundleName : "Fenster Verwaltung",
		bundleDescription : "--KERNFUNKTIONALIT\u00c4T-- Die Fenster Verwaltung ist f\u00fcr die Verwaltung aller Dialog-Fenster verantwortlich.",
		ui : {
			defaultWindowTitle : "Fenster",
			closeBtn : {
				title : "Schlie\u00dfen"
			},
			minimizeBtn : {
				title : "Verkleinern"
			},
			maximizeBtn : {
				title : "Maximieren"
			},
			restoreBtn : {
				title : "Wiederherstellen"
			},
			opacityBtn : {
				title : "Transparenz umschalten"
			},
			collapseBtn : {
				title : "Inhalt verstecken"
			},
			loading : {
				title : "Bitte warten!",
				message : "Lade..."
			},
			okcancel : {
				title : "Frage",
				okButton : "OK",
				cancelButton : "Abbrechen"
			}
		}
	},
	ec_legend : {
		bundleName : "Legende",
		bundleDescription : "Dieses Bundle stellt die Esri Legende zur Verf\u00fcgung.",
		tool : {
			title : "Legenda",
			tooltip : "Legenda"
		}
	},
	ec_parameter_interceptor : {
		bundleName : "ec-parameter-interceptor",
		bundleDescription : "ec-parameter-interceptor",
		errors : {
			missingQueryParameter : "Missing query parameters",
			missingTypeQueryParameter : "Missing type query parameter",
			missingDatasetOrUrlQueryParameter : "Missing dataset or url query parameter",
			noComponentContextProvided : "No componentContext provided",
			noBundleContextProvided : "No bundleContext provided",
			noQueryParametersProvided : "No query parameters provided",
			datasetTypeNotSupported : "The type '${type}' is not supported",
			invalidGeoJSON : "Invalid GeoJSON",
			unsupportedCRSType : "Only named CRS are supported",
			canNotParseNamedCRS : "Can not parse named CRS to WKID: ${name}",
			mixingOfDifferentCRS : "Mixing of different CRS is not supported",
			invalidGeoJSONType : "Unknown GeoJSON type attribute: ${type}",
			typeNotFoundInResource : "No appropriate format for ${type} found in resource",
			couldNotLoadDataset : "Could not load dataset: ${cause}",
			unableToAddDataset : "Unable to add dataset: ${cause}",
			unableToParseCapabilities : "Unable to parse WMS Capabilities response",
			canNotDetermineUrl : "Could not determine the download URL of the resource"
		}
	},
	ec_feature_info : {
		tool : {
			title : "Mais informa\u00E7\u00F5es",
			tooltip : "Mais informa\u00E7\u00F5es"
		}
	},
	ec_map_loading_screen : {
		bundleName : "ec-map-loading-screen",
		bundleDescription : "ec-map-loading-screen",
		messages : {
			initializingMap : "Initialisiere Karte. Bitte warten!"
		}
	},
	ec_wms_layer_selector : {
		bundleName : "ec-wms-layer-selector",
		bundleDescription : "ec-wms-layer-selector",
		ui : {
			windowTitle : "Selecionar camadas",
			hint : "O servi\u00E7o WMS escolhido cont\u00E9m ${countToProvideSelection} camadas, selecione todas as camadas a exibir no mapa.",
			selectAll : "Selecionar tudo",
			closeButton : "OK"
		},
		tool : {
			title : "Camada",
			tooltip : "Camada"
		}
	},
	ec_error_messages : {
		bundleName : "ec-error-messages",
		bundleDescription : "ec-error-messages",
		general : {
			serviceNotAvailable : "O servi\u00E7o pedido n\u00E3o est\u00E1 dispon\u00EDvel",
			serviceNotSupported : "O tipo de servi\u00E7o prestado n\u00E3o \u00E9 suportada",
			invalidResource : "Este URL cont\u00E9m um recurso n\u00E3o v\u00E1lido.",
			unsupportedResource : "Este URL cont\u00E9m um recurso n\u00E3o suportado.",
			errorSource : "Foi identificada a seguinte causa de erro",
			requestedUrl : "URL pedido",
			technicalSupport : "Se o problema persistir, contacte a assist\u00EAncia t\u00E9cnica.",
			unsupportedServiceType : "O tipo de servi\u00E7o prestado n\u00E3o \u00E9 suportada",
			technicalSupportCreateTicket : "Create support ticket",
			weWillContactProvider : "Contactaremos o fornecedor do servi\u00E7o e  inform\u00E1-lo-emos do sucedido.",
			closeButton : "OK"
		},
		detailedErrors : {
			crs : "O servi\u00E7o n\u00E3o oferece um sistema compat\u00EDvel de coordenadas de refer\u00EAncia.",
			encoding : "O servi\u00E7o n\u00E3o oferece um sistema compat\u00EDvel de codifica\u00E7\u00E3o de caracteres.",
			metadata : "O documento dos metadados do servi\u00E7o est\u00E1 incorreto.",
			fileFormat : "Foi recebido um formato de dados inv\u00E1lido do URL indicado.",
			dataRequest : "Foi recebida uma resposta inv\u00E1lida do servi\u00E7o ao pedir os dados. Isto pode dever-se a erro interno do servidor, erro na configura\u00E7\u00E3o do servi\u00E7o, comportamento do servi\u00E7o n\u00E3o compat\u00EDvel com as normas, etc."
		},
		httpIssues : {
			303 : "O resultado pode estar num URI diferente",
			305 : "O recurso pedido s\u00F3 est\u00E1 dispon\u00EDvel atrav\u00E9s de um servidor proxy",
			400 : "O servidor n\u00E3o pode executar o pedido devido a erro da parte do cliente",
			401 : "O recurso pedido requer autoriza\u00E7\u00E3o",
			403 : "O recurso pedido n\u00E3o est\u00E1 acess\u00EDvel ao p\u00FAblico",
			404 : "O recurso pedido n\u00E3o foi encontrado",
			405 : "O recurso foi pedido atrab\u00E9s de um m\u00E9todo HTTP n\u00E3o permitido",
			406 : "O recurso pediu um conte\u00FAdo n\u00E3o aceit\u00E1vel",
			407 : "O recurso requer autoriza\u00E7\u00E3o pr\u00E9via atrav\u00E9s de um servi\u00E7o proxy",
			500 : "O recurso n\u00E3o est\u00E1 dispon\u00EDvel devido a erro interno do servidor",
			501 : "O recurso n\u00E3o est\u00E1 dispon\u00EDvel devido a um m\u00E9todo de pedido n\u00E3o reconhecido",
			502 : "O recurso n\u00E3o est\u00E1 dispon\u00EDvel devido a m\u00E1 configura\u00E7\u00E3o da porta de acesso",
			503 : "O recurso n\u00E3o est\u00E1 dispon\u00EDvel neste momento devido a sobrecarga ou manuten\u00E7\u00E3o",
			504 : "O recurso n\u00E3o est\u00E1 dispon\u00EDvel devido a m\u00E1 configura\u00E7\u00E3o ou falta de resposta da porta de acesso",
			505 : "O recurso n\u00E3o suporta a vers\u00E3o HTTP utilizada",
			generic : "Ocorreu um erro de servidor durante o pedido do recurso"
		}
	},
	ec_user_tutorial : {
		bundleName : "ec-user-tutorial",
		bundleDescription : "ec-user-tutorial",
		externalResourceDisclaimer : {
			introduction : "Este visualizador de mapas acede a servi\u00E7os externos e exibe os seus dados.  Estes servi\u00E7os externos n\u00E3o s\u00E3o mantidos pela Comiss\u00E3o Europeia, motivo por que n\u00E3o tem qualquer influ\u00EAncia na sua disponibilidade/estabilidade. Podem ocorrer os seguintes problemas:",
			rotatingCircle : "Um c\u00EDrculo em rota\u00E7\u00E3o sobre o mapa indica que o visualizador aguarda a resposta da fonte dos dados. Isto pode dever-se a indisponibilidade do servidor ou a problemas na rede.",
			layerZoomLevel : "\u00C9 poss\u00EDvel que algumas camadas n\u00E3o estejam vis\u00EDveis em determinados n\u00EDveis de zoom. Isto deve-se a restri\u00E7\u00F5es impostas pelo fornecedor do servi\u00E7o externo. Pode ser necess\u00E1rio aproximar ou afastar o mapa para visualizar os dados da camada.",
			layerPanning : "Os dados acedidos a partir de um servidor externo podem estar fora da atual visualiza\u00E7\u00E3o do mapa. Este visualizador procura identificar automaticamente a \u00E1rea coberta. Tamb\u00E9m pode acontecer que os metadatos n\u00E3o estejam completos ou  corretos e deva navegar na camada atual.",
			detailInformation : "Um pedido de informa\u00E7\u00F5es pormenorizadas sobre uma funcionalidade ser\u00E1 processado mediante um pedido  espec\u00EDfico ao servidor do mapa. Se a resposta a este pedido estiver mal formulada ou sem conte\u00FAdo (devido a erro do servidor) n\u00E3o ser\u00E1 exibida qualquer informa\u00E7\u00E3o e surgir\u00E1 uma janela vazia.",
			technicalSupport : "Se encontrar qualquer outro problema, n\u00E3o hesite em contactar a assist\u00EAncia t\u00E9cnica:",
			technicalSupportContactLink : "Contactar a assist\u00EAncia t\u00E9cnica",
			donotshow : "N\u00E3o voltar a apresentar a p\u00E1gina de boas-vindas.",
			mapLoading : "Carregamento do mapa",
			mapScaling : "Os mapas de base utilizados por esta aplica\u00E7\u00E3o s\u00E3o disponibilizados pelo Servi\u00E7o de Estat\u00EDstica da Uni\u00E3o Europeia (Eurostat). Estes mapas t\u00EAm uma resolu\u00E7\u00E3o at\u00E9 1:50.000. Est\u00E3o a ser elaborados mapas de base com melhor resolu\u00E7\u00E3o (p. ex., para suportar a visualiza\u00E7\u00E3o de dados de uma cidade). Assim, \u00E9 poss\u00EDvel que algumas fontes de dados ainda n\u00E3o possam ser exibidas num mapa perfeitamente adequado para o efeito."
		},
		legalNotice : "A terminologia utilizada e a forma como o material \u00E9 apresentado neste mapa n\u00E3o subentendem a express\u00E3o de qualquer opini\u00E3o por parte da Uni\u00E3o Europeia quanto \u00E0 situa\u00E7\u00E3o jur\u00EDdica de qualquer pais, territ\u00F3rio, cidade ou \u00E1rea, nem sobre as suas autoridades, nem ainda sobre a delimita\u00E7\u00E3o das respetivas fronteiras ou limites. Kosovo*: esta designa\u00E7\u00E3o n\u00E3o prejudica as posi\u00E7\u00F5es relativas ao estatuto e est\u00E3o conformes com a Resolu\u00E7\u00E3o 1244/99 do Conselho de Seguran\u00E7a das Na\u00E7\u00F5es Unidas e o parecer do Tribunal Internacional de Justi\u00E7a sobre a declara\u00E7\u00E3o de independ\u00EAncia do Kosovo. Palestina*: esta designa\u00E7\u00E3o n\u00E3o deve ser interpretada como o reconhecimento de um Estado da Palestina e n\u00E3o  prejudica as posi\u00E7\u00F5es individuais dos Estados-Membros sobre esta quest\u00E3o.",
		legalNoticeHeader : "Cl\u00E1usula de isen\u00E7\u00E3o de responsabilidade",
		tutorial : {
			welcome : "Bem-vindo ao visualizador de mapas do Portal Europeu de Dados",
			familiarise : "Esta breve introdu\u00E7\u00E3o visa familiariz\u00E1-lo com os elementos e as funcionalidades do visualizador de mapas.",
			navigation : "Clique e mantenha premido o bot\u00E3o esquerdo do rato  para mover-se no mapa.",
			zoom : "Estes bot\u00F5es permitem alterar o n\u00EDvel de zoom do mapa. Tamb\u00E9m pode usar a roda do rato para ajustar o zoom.",
			features : "Estes bot\u00F5es d\u00E3o acesso a mais funcionalidades. Atuam como comandos e ativam ou desativam uma funcionalidade.",
			legend : "A legenda permite examinar as camadas do mapa e inclu\u00ED-las ou n\u00E3o na visualiza\u00E7\u00E3o ativa. Os nomes s\u00E3o obtidos diretamente dos servi\u00E7os externos.",
			transparency : "Tamb\u00E9m pode ajustar a transpar\u00EAncia de uma camada.",
			featureinfo : "Alguns dados podem ser examinados mais pormenorizadamente. Quando esta funcionalidade est\u00E1 ativada, pode clicar no mapa para obter mais informa\u00E7\u00F5es.",
			okButton : "OK",
			closeButton : "Fechar",
			next : "Seguinte",
			back : "Anterior",
			skip : "Omitir",
			done : "Conclu\u00EDdo"
		}
	}
});
