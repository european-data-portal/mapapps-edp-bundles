define({
	contentviewer : {
		bundleName : "Content Viewer",
		bundleDescription : "Displays different kinds of content.",
		ui : {
			unknownContentError : "The content is unknown.",
			graphicinfotool : {
				title : "Item Info",
				desc : "Item Info"
			},
			content : {
				defaultTitle : "Item",
				grid : {
					detailView : "Show details",
					key : "Property",
					value : "Value"
				},
				customTemplate : {
					detailView : "Show details"
				},
				attachment : {
					noAttachmentSupport : "This layer does not offer attachment support",
					detailView : "Open detail view"
				},
				AGSDetail : {
					title : "Detail View",
					print : "Print",
					serviceMetadata : "Service metadata",
					serviceURL : "URL",
					serviceCopyrights : "Copyrights",
					serviceTitle : "Title",
					pager : {
						pageSizeLabel : "Feature ${currentPage} of ${endPage}"
					},
					key : "Property",
					value : "Value",
					detailView : "Show details"
				}
			}
		}
	},
	coordinatetransformer : {
		bundleName : "Coordinate Transformer",
		bundleDescription : "--CORE FUNCTIONALITY-- The Coordinate Transformer transforms geometries from a source coordinate system to a target coordinate system."
	},
	featureinfo : {
		bundleName : "FeatureInfo",
		bundleDescription : "FeatureInfo displays information on features for active layers.",
		ui : {
			featureInfo : {
				noQueryLayersFound : "No queryable layers found!",
				contentInfoWindowTitle : "Identify",
				noResultsFound : "No results found.",
				loadingInfoText : "Querying layer ${layerName} (${layerIndex} of ${layersTotal}).",
				featureInfoTool : "Identify",
				layer : "Layer",
				feature : "Feature"
			},
			wms : {
				emptyResult : "No results found on the WMS layer '${layerTitle}'."
			}
		}
	},
	geometryservice : {
		bundleName : "GeometryService",
		bundleDescription : "The bundle provides a central geometry service."
	},
	geojson : {
		bundleName : "GeoJson",
		bundleDescription : "This bundle provides a conversion service for geojson and well-known text formats"
	},
	infoviewer : {
		bundleName : "Info Viewer",
		bundleDescription : "The Info Viewer shows content information for a location in a moveable and resizable window.",
		ui : {
			title : "Info Viewer",
			tools : {
				closeInfoWindowMapdeskTool : "Close",
				closeInfoWindowMapTool : "Close",
				focusMapTool : "Center in Map",
				attachToGeorefTool : "Attach to position",
				mainActivationTool : "Location Information"
			}
		}
	},
	languagetoggler : {
		bundleName : "Language Toggler",
		bundleDescription : "The language of the user interface can be switched by a language toggler.",
		ui : {
			title : "Language"
		}
	},
	map : {
		bundleName : "Map",
		bundleDescription : "The Map bundle manages the main map and all map content information. The client can abstract the services used in a hierarchical Map model so that they can be displayed in various ways to the user.",
		ui : {
			nodeUpdateError : {
				info : "Update of service '${title}' failed! Msg: ${errorMsg}",
				detail : "Update of service '${title}' failed! Msg: ${url} ${errorMsg} - Details: ${error}"
			},
			sliderLabels : {
				country : "Country",
				region : "Region",
				town : "Town"
			}
		},
		drawTooltips : {
			addPoint : "Click in the map",
			addMultipoint : "Click to start",
			finishLabel : "Finish"
		}
	},
	mobiletoc : {
		bundleName : "Mobile TOC",
		bundleDescription : "This bundle provides a mobile table of contents",
		tool : {
			title : "Map Content",
			tooltip : "Turn on/off Map Content"
		},
		ui : {
			basemaps : "Base maps",
			operationalLayers : "Operational layers",
			switchUI : {
				noTitle : "no title",
				leftLabel : "On",
				rightLabel : "Off"
			}
		}
	},
	mobileview : {
		bundleName : "Mobile View",
		bundleDescription : "Contains special classes that fix some issues concerning dojox.mobile"
	},
	notifier : {
		bundleName : "Notifier",
		bundleDescription : "All messages on status, progress, errors and warnings are shown in a popup message so that a user can understand what is going on in the application.",
		ui : {
			title : "Notifier",
			tooltips : {
				close : "Close this message",
				glue : "Pin this message"
			}
		}
	},
	parametermanager : {
		bundleName : "Parameter Manager",
		bundleDescription : "The Parameter Manager is responsible to delegate parameters from the URL to the according components on startup.",
		ui : {
			encoderBtn : "Link tool",
			encoderBtnTooltip : "Link tool",
			sendMail : "EMail",
			refresh : "Refresh",
			linkBoxTitle : "Link URL",
			size : "Size (Pixels)",
			codeBoxTitle : "Code to embed in HTML",
			qrCode : "QRCode",
			mailBody : "${url}",
			mailSubject : "Check out this map!",
			options : {
				small : "small (480 x 320)",
				medium : "medium (640 x 480)",
				large : "large (1280 x 1024)",
				custom : "custom"
			}
		}
	},
	printing : {
		bundleName : "Print",
		bundleDescription : "This bundle provides a print tool to print the map.",
		tool : {
			title : "Print",
			tooltip : "Print",
			back : "Back"
		},
		resultWin : {
			title : "Print"
		}
	},
	qrcode : {
		bundleName : "QRCode",
		bundleDescription : "This bundle provides a service that can be used to create QR codes.",
		errorMessage : "QR Code can not be generated."
	},
	splashscreen : {
		bundleName : "Splash Screen",
		bundleDescription : "The Splash Screen shows a progress bar and a logo (e.g. the map.apps logo) and/or a text while starting the application.",
		loadTitle : "Starting Application '{appName}'",
		loadBundle : "{percent}% - {startedBundles}/{maxBundlesToStart} - Load {name} "
	},
	system : {
		bundleName : "System",
		bundleDescription : "--CORE FUNCTIONALITY-- The System describes the basic core functionality (compilable skeleton) of map.apps."
	},
	templatelayout : {
		bundleName : "Template Layout",
		bundleDescription : "--CORE FUNCTIONALITY-- The Template Layout implements the arrangement of all UI elements based on predefined templates."
	},
	templates : {
		bundleName : "View",
		bundleDescription : "--CORE FUNCTIONALITY-- Views are used by the Template Layout.",
		templates : {
			desktop : {
				title : "Desktop",
				desc : "The desktop style layout"
			},
			modern : {
				title : "Modern",
				desc : "A modern layout"
			},
			minimal : {
				title : "Minimal",
				desc : "A minimal layout"
			}
		},
		ui : {
			selectorLabelTitle : "View"
		}
	},
	themes : {
		bundleName : "Style",
		bundleDescription : "--CORE FUNCTIONALITY-- The Themes bundle manages all CSS information like colors, font styles, background images etc. Users can switch between different looks of the user interface elements by selecting different styles.",
		themes : {
			pure : {
				desc : "The map.apps 'pure' style."
			},
			night : {
				desc : "The map.apps 'night' style."
			}
		},
		ui : {
			selectorLabelTitle : "Style"
		}
	},
	windowmanager : {
		bundleName : "Window Manager",
		bundleDescription : "--CORE FUNCTIONALITY-- The Window Manager is responsible to manage all floating windows.",
		ui : {
			defaultWindowTitle : "Window",
			closeBtn : {
				title : "Zatvori"
			},
			minimizeBtn : {
				title : "Minimize"
			},
			maximizeBtn : {
				title : "Maximize"
			},
			restoreBtn : {
				title : "Restore"
			},
			opacityBtn : {
				title : "Change transparency"
			},
			collapseBtn : {
				title : "Hide content"
			},
			loading : {
				title : "Please wait!",
				message : "Loading..."
			},
			okcancel : {
				title : "Question",
				okButton : "OK",
				cancelButton : "Cancel"
			}
		}
	},
	ec_legend : {
		bundleName : "Jelmagyar\u00E1zat",
		bundleDescription : "This bundle provides the Esri legend.",
		tool : {
			title : "Jelmagyar\u00E1zat",
			tooltip : "Jelmagyar\u00E1zat"
		}
	},
	ec_parameter_interceptor : {
		bundleName : "ec-parameter-interceptor",
		bundleDescription : "ec-parameter-interceptor",
		errors : {
			missingQueryParameter : "Missing query parameters",
			missingTypeQueryParameter : "Missing type query parameter",
			missingDatasetOrUrlQueryParameter : "Missing dataset or url query parameter",
			noComponentContextProvided : "No componentContext provided",
			noBundleContextProvided : "No bundleContext provided",
			noQueryParametersProvided : "No query parameters provided",
			datasetTypeNotSupported : "The type '${type}' is not supported",
			invalidGeoJSON : "Invalid GeoJSON",
			unsupportedCRSType : "Only named CRS are supported",
			canNotParseNamedCRS : "Can not parse named CRS to WKID: ${name}",
			mixingOfDifferentCRS : "Mixing of different CRS is not supported",
			invalidGeoJSONType : "Unknown GeoJSON type attribute: ${type}",
			typeNotFoundInResource : "No appropriate format for ${type} found in resource",
			couldNotLoadDataset : "Could not load dataset: ${cause}",
			unableToAddDataset : "Unable to add dataset: ${cause}",
			unableToParseCapabilities : "Unable to parse WMS Capabilities response",
			canNotDetermineUrl : "Could not determine the download URL of the resource"
		}
	},
	ec_feature_info : {
		tool : {
			title : "R\u00E9szletes adatok",
			tooltip : "R\u00E9szletes adatok"
		}
	},
	ec_map_loading_screen : {
		bundleName : "ec-map-loading-screen",
		bundleDescription : "ec-map-loading-screen",
		messages : {
			initializingMap : "A térkép betöltése folyamatban..."
		}
	},
	ec_wms_layer_selector : {
		bundleName : "ec-wms-layer-selector",
		bundleDescription : "ec-wms-layer-selector",
		ui : {
			windowTitle : "R\u00E9tegek kiv\u00E1laszt\u00E1sa",
			hint : "A kiv\u00E1lasztott WMS-szolg\u00E1ltat\u00E1s ${countToProvideSelection} r\u00E9teget tartalmaz. K\u00E9rj\u00FCk, v\u00E1lassza ki a t\u00E9rk\u00E9pen megjelen\u00EDtend\u0151 \u00F6sszes r\u00E9teget.",
			selectAll : "\u00D6sszes kiv\u00E1laszt\u00E1sa",
			closeButton : "OK"
		},
		tool : {
			title : "R\u00E9teg",
			tooltip : "R\u00E9teg"
		}
	},
	ec_error_messages : {
		bundleName : "ec-error-messages",
		bundleDescription : "ec-error-messages",
		general : {
			serviceNotAvailable : "A k\u00E9rt szolg\u00E1ltat\u00E1s nem \u00E9rhet\u0151 el",
			serviceNotSupported : "A k\u00E9rt szolg\u00E1ltat\u00E1s nem t\u00E1mogatott",
			invalidResource : "A megadott URL-en \u00E9rv\u00E9nytelen forr\u00E1s tal\u00E1lhat\u00F3",
			unsupportedResource : "A megadott URL-en nem t\u00E1mogatott forr\u00E1s tal\u00E1lhat\u00F3",
			errorSource : "A k\u00F6vetkez\u0151 hibaforr\u00E1st \u00E1llap\u00EDtotta meg a rendszer",
			requestedUrl : "K\u00E9rt URL",
			unsupportedServiceType : "A szolg\u00E1ltat\u00E1ssal nem t\u00E1mogatott",
			technicalSupport : "Amennyiben a probl\u00E9ma tov\u00E1bbra is fenn\u00E1ll, l\u00E9pjen kapcsolatba a m\u0171szaki t\u00E1mogat\u00E1si r\u00E9szleggel",
			technicalSupportCreateTicket : "Create support ticket",
			weWillContactProvider : "Kapcsolatba l\u00E9p\u00FCnk a szolg\u00E1ltat\u00E1s ny\u00FAjt\u00F3j\u00E1val, \u00E9s jelezz\u00FCk neki a probl\u00E9m\u00E1t.",
			closeButton : "OK"
		},
		detailedErrors : {
			crs : "A szolg\u00E1ltat\u00E1s nem ny\u00FAjt t\u00E1mogatott referenciakoordin\u00E1ta-rendszert",
			encoding : "A szolg\u00E1ltat\u00E1s nem ny\u00FAjt t\u00E1mogatott karakterk\u00F3dol\u00E1st",
			metadata : "A szolg\u00E1ltat\u00E1s metaadat-dokumentuma hib\u00E1s",
			fileFormat : "A megadott URL-en \u00E9rv\u00E9nytelen adatform\u00E1tum tal\u00E1lhat\u00F3",
			dataRequest : "A szolg\u00E1ltat\u00E1s adatainak lek\u00E9rdez\u00E9se sor\u00E1n \u00E9rv\u00E9nytelen v\u00E1lasz \u00E9rkezett. Ennek okai lehetnek t\u00F6bbek k\u00F6z\u00F6tt a k\u00F6vetkez\u0151k: bels\u0151 kiszolg\u00E1l\u00F3hiba, a szolg\u00E1ltat\u00E1s helytelen konfigur\u00E1ci\u00F3ja, a szolg\u00E1ltat\u00E1s nem a szabv\u00E1nyoknak megfelel\u0151 viselked\u00E9se stb."
		},
		httpIssues : {
			303 : "A k\u00E9rt v\u00E1lasz m\u00E1s URL-n tal\u00E1lhat\u00F3",
			305 : "A k\u00E9rt forr\u00E1s csak proxy-n kereszt\u00FCl \u00E9rhet\u0151 el",
			400 : "A kiszolg\u00E1l\u00F3 nem tudja feldolgozni a k\u00E9r\u00E9st egy \u00E9szlelt \u00FCgyf\u00E9lhiba miatt",
			401 : "A k\u00E9rt forr\u00E1shoz enged\u00E9lyez\u00E9s sz\u00FCks\u00E9ges",
			403 : "A k\u00E9rt forr\u00E1s nem \u00E9rhet\u0151 el nyilv\u00E1nosan",
			404 : "A k\u00E9rt forr\u00E1s nem tal\u00E1lhat\u00F3",
			405 : "A forr\u00E1st nem enged\u00E9lyezett HTTP-m\u00F3dszerrel k\u00E9rte",
			406 : "Nem elfogadhat\u00F3 tartalmat k\u00E9rt a forr\u00E1st\u00F3l",
			407 : "A forr\u00E1shoz val\u00F3 hozz\u00E1f\u00E9r\u00E9shez el\u0151zetes hiteles\u00EDt\u00E9s sz\u00FCks\u00E9ges egy proxyszolg\u00E1ltat\u00E1son kereszt\u00FCl",
			500 : "A forr\u00E1s bels\u0151 kiszolg\u00E1l\u00F3hiba miatt nem \u00E9rhet\u0151 el",
			501 : "A forr\u00E1s ismeretlen k\u00E9relmi met\u00F3dus miatt nem \u00E9rhet\u0151 el",
			502 : "A forr\u00E1s hib\u00E1san konfigur\u00E1lt \u00E1tj\u00E1r\u00F3 miatt nem \u00E9rhet\u0151 el",
			503 : "A forr\u00E1s jelenleg t\u00FAlterhelts\u00E9g vagy karbantart\u00E1s miatt nem \u00E9rhet\u0151 el",
			504 : "A forr\u00E1s hib\u00E1san konfigur\u00E1lt vagy nem v\u00E1laszol\u00F3 \u00E1tj\u00E1r\u00F3 miatt nem \u00E9rhet\u0151 el",
			505 : "A forr\u00E1s nem t\u00E1mogatja a megadott HTTP-verzi\u00F3t",
			generic : "Kiszolg\u00E1l\u00F3hiba l\u00E9pett fel a megadott forr\u00E1s lek\u00E9rdez\u00E9se sor\u00E1n"
		}
	},
	ec_user_tutorial : {
		bundleName : "ec-user-tutorial",
		bundleDescription : "ec-user-tutorial",
		externalResourceDisclaimer : {
			introduction : "A t\u00E9rk\u00E9p-megjelen\u00EDt\u0151 k\u00FCls\u0151 szolg\u00E1ltat\u00E1sokhoz f\u00E9r hozz\u00E1, \u00E9s megjelen\u00EDti az adataikat. Ezeket a k\u00FCls\u0151 szolg\u00E1ltat\u00E1sokat nem az Eur\u00F3pai Bizotts\u00E1g m\u0171k\u00F6dteti, ez\u00E9rt semmilyen hat\u00E1sa nincs az el\u00E9rhet\u0151s\u00E9g\u00FCkre vagy stabilit\u00E1sukra. A k\u00F6vetkez\u0151 probl\u00E9m\u00E1k l\u00E9phetnek fel:",
			rotatingCircle : "A t\u00E9rk\u00E9pen l\u00E1that\u00F3 forg\u00F3 k\u00F6r jelzi, hogy a t\u00E9rk\u00E9p-megjelen\u00EDt\u0151 az adatforr\u00E1s v\u00E1lasz\u00E1ra v\u00E1r. Ennek oka lehet, hogy a kiszolg\u00E1l\u00F3 nem \u00E9rhet\u0151 el, vagy h\u00E1l\u00F3zati probl\u00E9m\u00E1k l\u00E9ptek fel.",
			layerZoomLevel : "Bizonyos r\u00E9tegforr\u00E1sok nem jelennek meg bizonyos nagy\u00EDt\u00E1si szinteken. Ez a k\u00FCls\u0151 szolg\u00E1ltat\u00E1s ny\u00FAjt\u00F3ja \u00E1ltal be\u00E1ll\u00EDtott korl\u00E1toz\u00E1sok eredm\u00E9nye. A r\u00E9tegadatok megjelen\u00EDt\u00E9s\u00E9hez nagy\u00EDt\u00E1sra vagy kicsiny\u00EDt\u00E9sre lehet sz\u00FCks\u00E9g.",
			layerPanning : "Megt\u00F6rt\u00E9nhet, hogy a k\u00FCls\u0151 kiszolg\u00E1l\u00F3r\u00F3l el\u00E9rhet\u0151 adatok az aktu\u00E1lis t\u00E9rk\u00E9pn\u00E9zeten k\u00EDv\u00FCl vannak. A t\u00E9rk\u00E9p-megjelen\u00EDt\u0151 megpr\u00F3b\u00E1lja automatikusan azonos\u00EDtani a lefedett ter\u00FCletet. Bizonyos esetekben azonban a metaadatok nem pontosak vagy hib\u00E1sak, ez\u00E9rt sz\u00FCks\u00E9g lehet a t\u00E9nyleges m\u00E9rethez t\u00F6rt\u00E9n\u0151 igaz\u00EDt\u00E1sra.",
			detailInformation : "Amennyiben r\u00E9szletes inform\u00E1ci\u00F3kat k\u00E9rt egy kiv\u00E1lasztott funkci\u00F3hoz, ezt a t\u00E9rk\u00E9pkiszolg\u00E1l\u00F3nak k\u00FCld\u00F6tt k\u00FCl\u00F6n k\u00E9r\u00E9s keret\u00E9ben kezeli a rendszer. Amennyiben erre a k\u00E9r\u00E9sre helytelen form\u00E1tum\u00FA vagy \u00FCres (pl. kiszolg\u00E1l\u00F3hiba miatt) v\u00E1lasz \u00E9rkezik, nem jelen\u00EDthet\u0151 meg inform\u00E1ci\u00F3, \u00E9s \u00FCres ablak jelenik meg.",
			mapScaling : "Az alkalmaz\u00E1sban haszn\u00E1lt alapt\u00E9rk\u00E9peket az Eur\u00F3pai Uni\u00F3 statisztikai hivatala (Eurostat) biztos\u00EDtja. Ezen t\u00E9rk\u00E9pek jelenleg legfeljebb 1:50\u00A0000 l\u00E9pt\u00E9k\u0171ek lehetnek. Az enn\u00E9l r\u00E9szletesebb alapt\u00E9rk\u00E9pek jelenleg el\u0151k\u00E9sz\u00FClet alatt \u00E1llnak (pl. egy v\u00E1roson bel\u00FCli adatok vizualiz\u00E1ci\u00F3j\u00E1nak t\u00E1mogat\u00E1sa \u00E9rdek\u00E9ben). Ez\u00E9rt el\u0151fordulhat, hogy bizonyos adatforr\u00E1sok m\u00E9g nem jelennek meg egy optim\u00E1lis h\u00E1tt\u00E9rt\u00E9rk\u00E9pen.",
			technicalSupport : "Amennyiben probl\u00E9ma mer\u00FCl fel a haszn\u00E1lat sor\u00E1n, l\u00E9pjen kapcsolatba a m\u0171szaki t\u00E1mogat\u00E1si r\u00E9szleggel:",
			technicalSupportContactLink : "Kapcsolatfelv\u00E9tel a m\u0171szaki t\u00E1mogat\u00E1si r\u00E9szleggel",
			mapLoading : "A térkép betöltése folyamatban...",
			donotshow : "Ne jelen\u00EDtse meg t\u00F6bbet az \u00FCdv\u00F6zl\u0151k\u00E9perny\u0151t."
		},
		legalNotice : "A haszn\u00E1lt megnevez\u00E9sek \u00E9s a t\u00E9rk\u00E9pen megjelen\u00EDtett anyagok nem min\u0151s\u00FClnek az Eur\u00F3pai Uni\u00F3 v\u00E9lem\u00E9nynyilv\u00E1n\u00EDt\u00E1s\u00E1nak egyetlen orsz\u00E1g, ter\u00FClet, v\u00E1ros vagy t\u00E9rs\u00E9g vagy ezek hat\u00F3s\u00E1gai jog\u00E1ll\u00E1s\u00E1val vagy hat\u00E1rvonalaival kapcsolatban. Koszov\u00F3*: Ez a megnevez\u00E9s nem \u00E9rinti a jog\u00E1ll\u00E1ssal kapcsolatos \u00E1ll\u00E1spontokat, tov\u00E1bb\u00E1 \u00F6sszhangban van az 1244/1999 ENSZ BT-hat\u00E1rozattal \u00E9s a Nemzetk\u00F6zi B\u00EDr\u00F3s\u00E1gnak a koszov\u00F3i f\u00FCggetlens\u00E9gi nyilatkozatr\u00F3l sz\u00F3l\u00F3 v\u00E9lem\u00E9ny\u00E9vel. Palesztina*: Ez a megnevez\u00E9s nem \u00E9rtelmezhet\u0151 \u00FAgy, mint amely elismern\u00E9 a palesztin \u00E1llamot, valamint nem s\u00E9rti a tag\u00E1llamok egy\u00E9ni \u00E1ll\u00E1spontj\u00E1t ebben a k\u00E9rd\u00E9sben.",
		legalNoticeHeader : "Jogi nyilatkozat",
		tutorial : {
			welcome : "\u00DCdv\u00F6z\u00F6lj\u00FCk az eur\u00F3pai adatport\u00E1l t\u00E9rk\u00E9p-megjelen\u00EDt\u0151j\u00E9ben!",
			familiarise : "A k\u00F6vetkez\u0151 r\u00F6vid bevezet\u0151 c\u00E9lja, hogy megismertesse a t\u00E9rk\u00E9p-megjelen\u00EDt\u0151 elemeivel \u00E9s m\u0171k\u00F6d\u00E9s\u00E9vel.",
			navigation : "Nyomja le \u00E9s tartsa lenyomva a bal eg\u00E9rgombot a t\u00E9rk\u00E9pen t\u00F6rt\u00E9n\u0151 p\u00E1szt\u00E1z\u00E1shoz.",
			zoom : "Ezek a gombok a t\u00E9rk\u00E9p nagy\u00EDt\u00E1s\u00E1t m\u00F3dos\u00EDtj\u00E1k. A nagy\u00EDt\u00E1s m\u00F3dos\u00EDt\u00E1s\u00E1hoz az eg\u00E9rg\u00F6rg\u0151t is haszn\u00E1lhatja.",
			features : "Ezek a gombok tov\u00E1bbi funkci\u00F3kat is lehet\u0151v\u00E9 tesznek. A megadott funkci\u00F3 be- \u00E9s kikapcsol\u00F3ik\u00E9nt m\u0171k\u00F6dnek.",
			legend : "A jelmagyar\u00E1zat seg\u00EDts\u00E9g\u00E9vel megvizsg\u00E1lhatja az el\u00E9rhet\u0151 t\u00E9rk\u00E9pr\u00E9tegeket, valamint be- \u00E9s kikapcsolhatja megjelen\u00EDt\u00E9s\u00FCket a t\u00E9rk\u00E9pen. A nevek k\u00F6zvetlen\u00FCl a k\u00FCls\u0151 hozz\u00E1f\u00E9r\u00E9s\u0171 szolg\u00E1ltat\u00E1sb\u00F3l sz\u00E1rmaznak.",
			transparency : "Be\u00E1ll\u00EDthatja a r\u00E9tegek \u00E1tl\u00E1that\u00F3s\u00E1g\u00E1t is.",
			featureinfo : "Bizonyos adatokat r\u00E9szletesebben is megvizsg\u00E1lhat. Ezen funkci\u00F3 bekapcsol\u00E1s\u00E1val a t\u00E9rk\u00E9pre kattintva tov\u00E1bbi inform\u00E1ci\u00F3kat k\u00E9rdezhet le.",
			done : "K\u00E9sz",
			okButton : "OK",
			closeButton : "Bez\u00E1r\u00E1s",
			skip : "\u00C1tugr\u00E1s",
			next : "K\u00F6vetkez\u0151",
			back : "El\u0151z\u0151"
		}
	}
});