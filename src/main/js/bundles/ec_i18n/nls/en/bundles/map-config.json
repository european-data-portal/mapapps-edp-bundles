{
  "bundleName": "Map Configuration Wizard Bundle",
  "bundleDescription": "The Map bundle manages the main map and all map content information. The client can abstract the services used in a hierarchical Map model so that they can be displayed in various ways to the user.",
  "menu": {
    "mapstate": {
      "title": "Map Extent"
    },
    "mapmodel": {
      "title": "Map Content"
    },
    "mrrbuilder": {
      "title": "Services Management"
    },
    "mapoptions": {
      "title": "Map Options"
    }
  },
  "commonDelete": {
    "saveQuery": "Do you really want to delete '{number}' item(s)? Please note: The system can become inconsistent if you delete services referenced by other components! <b>Currently no consistent checks are performed!</b>",
    "error": "Deletion of items '{items}' failed!",
    "partialSuccess": "Some items could not be deleted!",
    "errorFinish": "No item could be deleted!",
    "success": "Items successfully deleted!"
  },
  "serviceDetails": {
    "windowTitle": "Service '{id}'",
    "newServiceId": "New",
    "url": "URL:",
    "urltooltip": "Please enter the exact service endpoint url here! e.g. http://services.arcgisonline.com/ArcGIS/rest/services/World_Topo_Map/MapServer",
    "title": "Title:",
    "titletooltip": "Please enter a title for the service",
    "description": "Description:",
    "descriptiontooltip": "Please enter a description for the service",
    "type": "Type:",
    "typetooltip": "Please choose the valid service type",
    "layer": "Layer:",
    "layertooltip": "Here you can enter the layer metadata of the service, structured like: <pre>[{\n  \"id\": \"0\",\n  \"title\": \"My Layer\"\n},...]</pre>",
    "options": "Options:",
    "optionstooltip": "<p>Here you can enter special options for service construction, structured like: <pre>{\n  \"optionName\": \"optionValue\",\n  ...\n}</pre></p><p>For example, opacity of a service can be configured as follows: <pre>{\n  \"opacity\": 0.5\n}</pre></p>",
    "fetchMetadata": "Fetch Metadata",
    "fetchMetadatatooltip": "Click this to fetch the configuration metadata from the url. Please note that all entered metadata will be overwritten with the data provided by the service!",
    "fetchMetadataDesc": "This will work for service types 'AGS_DYNAMIC', 'AGS_FEATURE' and 'AGS_TILED'. Nevertheless, other service types can be added manually with this form.",
    "fetchError": "Connection to Service '{url}' failed! {error}",
    "updateError": "Update of Service '{id}' failed! {error}",
    "updateSuccess": "Service '{id}' succesfull saved!"
  },
  "mrrBuilderWidget": {
    "createNewTitle": "Create new node",
    "removeServiceTool": "Remove service",
    "addServiceTool": "Add new service",
    "dataView": {
      "filter": {
        "menuDefaultLabel": "[All]",
        "textBoxPlaceHolder": ""
      },
      "noDataFound": "No services available...",
      "pager": {
        "pageLabelText": "Page ${currentPage} of ${endPage}",
        "backButtonTooltip": "Previous page",
        "forwardButtonTooltip": "Next page",
        "firstButtonTooltip": "First page",
        "lastButtonTooltip": "Last page",
        "pageSizeLabelText": "Items ${pageStartItemNumber}-${pageEndItemNumber} of ${itemCount}:"
      }
    },
    "categories": {
      "id": "Name",
      "title": "Title",
      "serviceUrl": "URL",
      "serviceType": "Type"
    }
  },
  "mapModelBuilderWidget": {
    "description": "In this window the map model of this app can be defined with operational and base layers. All available services are listed on the left. They can be dragged and dropped to the different layers in middle.",
    "hint": "New services can be added in the menu 'Services Management'.",
    "categoryDescription": "Description:",
    "categoryDescriptionTooltip": "Description of the service",
    "categoryTitle": "Title:",
    "categoryTitleTooltip": "Title of the service",
    "imageUrl": "URL for Map Flow cover image:",
    "imageUrlTooltip": "URL of Map Flow cover image (Size: 267x159 px, Formats: jpg, png, gif)",
    "defaultEnabled": "Category enabled on startup:",
    "defaultEnabledTooltip": "Check to set service to be enabled on startup of the app.",
    "detailsPaneTitle": "Change settings of service <b>'{category.title}'</b>",
    "detailsPaneService": "Based on service <b>'{service}'</b>",
    "transparencySlider": "Transparency:",
    "mapFlowPaneTitle": "<b>Map Flow Settings</b><br />If you are using the Map Flow, you can define properties (description and image) in the section below.",
    "detailsHint": "Select node to set details...",
    "servicesHint": "Drag services here...",
    "servicesTitle": "Services",
    "detailsTitle": "Details",
    "operationalLayer": "Operational Layer",
    "baseLayer": "Base Layer",
    "chooseLayers": "Choose which layers should be included in the service and if they should be visible initially.",
    "layerHead": "<b>Layer</b>",
    "layerVisibleHead": "<b>Visible initially</b>",
    "layerVisibleTooltip": "Check to set initial visible state of layer <b>'{title}'</b>.",
    "layerIncludeHead": "<b>Include</b>",
    "layerIncludeTooltip": "Check to include layer <b>'{title}'</b>.",
    "editable": "Editable",
    "editableTooltip": "Check to allow editing on this service"
  },
  "mapStateWidget": {
    "description": "The map extent defines the visible area of a map. Please choose a method to set the initial map extent.",
    "checkBoxApplyMainMapState": "Adjust map extent by panning the main map",
    "setExtentManually": "Manually type in the values for the map extent",
    "spatialReference": "Spatial Reference:",
    "errorMessage": "Please check the coordinates!"
  },
  "mapOptionsWidget": {
    "description": "With the following settings different map options can be defined.",
    "slider": {
      "label": "Show zoom slider",
      "tooltip": "Display a zoom slider on the map"
    },
    "sliderStyle": {
      "label": "Style of zoom slider",
      "tooltip": "Use small or default zoom slider",
      "type": {
        "small": "small",
        "large": "large"
      }
    },
    "nav": {
      "label": "Show navigation arrows",
      "tooltip": "Defines, if navigation arrows are displayed in corners of the map."
    },
    "logo": {
      "label": "Show \"powered-by\" logo",
      "tooltip": "Defines, if the \"powered-by\" logo is displayed on the map."
    },
    "doubleClickZoom": {
      "label": "Allow zooming on double click",
      "tooltip": "Defines, if zooming is possible by a double click."
    },
    "clickRecenter": {
      "label": "Allow recentering on [Shift] + click",
      "tooltip": "Defines, if recentering of the map is allowed by pressing [Shift] and mouse click."
    },
    "keyboardNavigation": {
      "label": "Allow keyboard navigation",
      "tooltip": "Defines, if keyboard navigation is allowed."
    },
    "scrollWheelZoom": {
      "label": "Allow zooming by mousewheel",
      "tooltip": "Defines, if zooming by mousewheel is allowed."
    },
    "sliderPosition": {
      "label": "Zoom slider position",
      "tooltip": "The position of the zoom slider (\"top-left\", \"top-right\", \"bottom-right\" or \"bottom-left\")",
      "position": {
        "topLeft": "top-left",
        "topRight": "top-right",
        "bottomLeft": "bottom-left",
        "bottomRight": "bottom-right"
      }
    },
    "displayGraphicsOnPan": {
      "label": "Show graphics while panning",
      "tooltip": "Defines, if graphics are displayed during panning. If set to false, the graphics are turned off during pan movement. Setting to false may improve performance in Internet Explorer."
    },
    "navigationMode": {
      "label": "Navigation mode",
      "tooltip": "Indicates whether the map uses CSS3 transformations when panning and zooming (if supported by the browser).",
      "csstransform": "css-transforms",
      "classic": "classic"
    },
    "fadeOnZoom": {
      "label": "Fade on zoom",
      "tooltip": "When true a fade effect is enabled for supported layers. Currently only tiled layers are supported and if navigation mode is set to 'css-transform'."
    },
    "showAttribution": {
      "label": "Show attribution",
      "tooltip": "Shows attribution information on the map (i.e. copyright information)."
    },
    "sliderOrientation": {
      "label": "Zoom slider orientation",
      "tooltip": "Orientation of the zoom slider. Valid values are: \"vertical\" or \"horizontal\".",
      "show": "show",
      "hide": "hide",
      "orientation": {
        "horizontal": "horizontal",
        "vertical": "vertical"
      }
    },
    "sliderLabels": {
      "label": "Show zoom slider labels",
      "tooltip": "Defines if labels are shown next to the zoom slider. (Works only with 'large' slider.)",
      "labels": {
        "country": "Country",
        "region": "Region",
        "town": "Town"
      }
    }
  }
}