define({
	contentviewer : {
		bundleName : "Amharc\u00e1n \u00c1bhar",
		bundleDescription : "Taispe\u00e1in cine\u00e1lacha \u00e9ags\u00fala \u00e1bhair.",
		ui : {
			unknownContentError : "Is \u00e9 an t-\u00e1bhar anaithnid.",
			graphicinfotool : {
				title : "Faisn\u00e9is eilimint",
				desc : "Faisn\u00e9is eilimint"
			},
			content : {
				defaultTitle : "Eilimint",
				grid : {
					detailView : "Taispe\u00e1in mionsonra\u00ed",
					key : "Maoin",
					value : "Luach"
				},
				customTemplate : {
					detailView : "Taispe\u00e1in mionsonra\u00ed"
				},
				attachment : {
					noAttachmentSupport : "N\u00ed dh\u00e9anann an ciseal taca\u00edocht ceangalt\u00e1n",
					detailView : "Taispe\u00e1in mionsonra\u00ed"
				},
				AGSDetail : {
					title : "F\u00e9ach ar na sonra\u00ed",
					print : "Priont\u00e1il",
					serviceMetadata : "Meiteashonra\u00ed Seirbh\u00eds",
					serviceURL : "URL",
					serviceCopyrights : "C\u00f3ipchearta",
					serviceTitle : "Teideal",
					pager : {
						pageSizeLabel : "Gn\u00e9 ${currentPage} de ${endPage}"
					},
					key : "Maoin",
					value : "Luach",
					detailView : "Taispe\u00e1in mionsonra\u00ed"
				}
			}
		}
	},
	coordinatetransformer : {
		bundleName : "Comhord\u00fa a claochlad\u00e1n",
		bundleDescription : "--Feidhmi\u00falacht CORE-- An chomhord\u00fa transforms claochlad\u00e1n c\u00e9imseata\u00ed \u00f3 cheann gc\u00f3ras chomhord\u00fa go ceann eile."
	},
	featureinfo : {
		bundleName : "FeatureInfo",
		bundleDescription : "FeatureInfo Taispe\u00e1nann eolas ar ghn\u00e9ithe d'sraitheanna gn\u00edomhach.",
		ui : {
			featureInfo : {
				noQueryLayersFound : "N\u00edl Ciseal inchuardaithe ar f\u00e1il!",
				contentInfoWindowTitle : "Aithin",
				noResultsFound : "N\u00edl na tortha\u00ed a fuarthas.",
				loadingInfoText : "Ceisti\u00fa $ ciseal {layerName} (${layerIndex} de ${layersTotal}).",
				featureInfoTool : "Aithin",
				layer : "Sraith",
				feature : "R\u00e9ad"
			},
			wms : {
				emptyResult : "N\u00edl na tortha\u00ed le f\u00e1il ar an ciseal WMS '${layerTitle}'."
			}
		}
	},
	geometryservice : {
		bundleName : "GeometryService",
		bundleDescription : "Sol\u00e1thra\u00edonn an bundle seirbh\u00eds c\u00e9imseata l\u00e1rnach."
	},
	geojson : {
		bundleName : "GeoJSON",
		bundleDescription : "Sol\u00e1thra\u00edonn an bundle seirbh\u00eds chomhsh\u00f3, is f\u00e9idir a phr\u00f3ise\u00e1il GeoJSON agus t\u00e9acs-maith ar a dtugtar (wkt)."
	},
	infoviewer : {
		bundleName : "Taispe\u00e1in Eolas",
		bundleDescription : "L\u00e9ir\u00edonn an taispe\u00e1ntas teachtaireacht t-eolas \u00fas\u00e1ideoir substainteach faoi \u00e1it ar an l\u00e9arsc\u00e1il i bhfuinneog.",
		ui : {
			title : "Eolas Viewer",
			tools : {
				closeInfoWindowMapdeskTool : "D\u00fan",
				closeInfoWindowMapTool : "D\u00fan",
				focusMapTool : "Ionad i L\u00e9arsc\u00e1il",
				attachToGeorefTool : "Ceangail le seasamh",
				mainActivationTool : "Su\u00edomh Eolas"
			}
		}
	},
	languagetoggler : {
		bundleName : "Switcher Teanga",
		bundleDescription : "Tr\u00edd an teanga an chomh\u00e9adain switcher teanga a athr\u00fa.",
		ui : {
			title : "Teanga"
		}
	},
	map : {
		bundleName : "L\u00e9arsc\u00e1il",
		bundleDescription : "Bainist\u00edonn an bundle L\u00e9arsc\u00e1il an l\u00e9arsc\u00e1il is m\u00f3 agus gach eolas ar chion l\u00e9arsc\u00e1il. Is f\u00e9idir teib\u00ed an cliant seirbh\u00eds\u00ed a \u00fas\u00e1idtear i samhail L\u00e9arsc\u00e1il ordlathach ionas gur f\u00e9idir iad a chur ar taispe\u00e1int ar bheala\u00ed \u00e9ags\u00fala chun an t-\u00fas\u00e1ideoir.",
		ui : {
			nodeUpdateError : {
				info : "Nuashonr\u00fa na seirbh\u00edse '${title}' theip! Earr\u00e1id: ${errorMsg}",
				detail : "Nuashonr\u00fa na seirbh\u00edse '${title}' theip! Earr\u00e1id: ${url} ${errorMsg} - Sonraigh: ${error}"
			},
			sliderLabels : {
				country : "T\u00edr",
				region : "R\u00e9igi\u00fan",
				town : "Cathair"
			}
		},
		drawTooltips : {
			addPoint : "Clice\u00e1il ar an l\u00e9arsc\u00e1il",
			addMultipoint : "Clice\u00e1il chun t\u00fas a chur",
			finishLabel : "Cr\u00edochnaigh"
		}
	},
	mobiletoc : {
		bundleName : "Rial\u00fa ar leibh\u00e9al soghluaiste",
		bundleDescription : "Sol\u00e1thra\u00edonn an bundle rialaitheoir leibh\u00e9al soghluaiste r\u00e9idh",
		tool : {
			title : "L\u00e9arsc\u00e1il \u00c1bhar",
			tooltip : "Cas ar / as \u00c1bhar L\u00e9arsc\u00e1il"
		},
		ui : {
			basemaps : "L\u00e9arsc\u00e1ileanna Base",
			operationalLayers : "Leibh\u00e9il \u00c1bhair",
			switchUI : {
				noTitle : "aon teideal",
				leftLabel : "Ar",
				rightLabel : "Off"
			}
		}
	},
	mobileview : {
		bundleName : "Soghluaiste F\u00e9ach",
		bundleDescription : "T\u00e1 ranganna speisialta a shocr\u00fa roinnt saincheisteanna a bhaineann le dojox.mobile"
	},
	notifier : {
		bundleName : "F\u00f3gra\u00ed",
		bundleDescription : "T\u00e1 gach teachtaireacht st\u00e1dais, an dul chun cinn n\u00f3 earr\u00e1id taispe\u00e1int don \u00fas\u00e1ideoir i teachtaireacht pop-suas go bhfuil s\u00e9 soil\u00e9ir cad at\u00e1 ag tarl\u00fa san iarratas.",
		ui : {
			title : "F\u00f3gr\u00f3ir",
			tooltips : {
				close : "D\u00fan Teachtaireacht",
				glue : "Teachtaireacht Ceangail"
			}
		}
	},
	parametermanager : {
		bundleName : "L\u00e1imhse\u00e1il Paraim\u00e9adar",
		bundleDescription : "T\u00e1 an Bainisteoir Paraim\u00e9adar freagrach paraim\u00e9adair a tharmligean \u00f3n URL do na comhph\u00e1irteanna r\u00e9ir ag am tosaithe.",
		ui : {
			encoderBtn : "Uirlis Nasc",
			encoderBtnTooltip : "Uirlis Nasc",
			sendMail : "R\u00edomhphost",
			refresh : "Athnuaigh",
			linkBoxTitle : "URL Link",
			size : "M\u00e9id (Picteil\u00edn)",
			codeBoxTitle : "C\u00f3d leab\u00fa i HTML",
			qrCode : "QRCode",
			mailBody : "${url}",
			mailSubject : "F\u00e9ach ar amanna an c\u00e1rta seo!",
			options : {
				small : "beag (480 x 320)",
				medium : "mhe\u00e1n (640 x 480)",
				large : "m\u00f3r (1280 x 1024)",
				custom : "\u00fas\u00e1ideoir-sainithe"
			}
		}
	},
	printing : {
		bundleName : "Priont\u00e1il",
		bundleDescription : "Sol\u00e1thra\u00edonn an bundle uirlis a phriont\u00e1il a phriont\u00e1il ar an l\u00e9arsc\u00e1il.",
		tool : {
			title : "Priont\u00e1il",
			tooltip : "Priont\u00e1il",
			back : "Ar ais"
		},
		resultWin : {
			title : "Priont\u00e1il"
		}
	},
	qrcode : {
		bundleName : "QRCode",
		bundleDescription : "Sol\u00e1thra\u00edonn an bundle seirbh\u00eds gur f\u00e9idir a \u00fas\u00e1id chun a chruth\u00fa c\u00f3id QR.",
		errorMessage : "N\u00ed raibh ghini\u00faint c\u00f3d QR rath\u00fail."
	},
	splashscreen : {
		bundleName : "Sc\u00e1ile\u00e1n sa bhaile",
		bundleDescription : "Taispe\u00e1nann an sc\u00e1ile\u00e1n sa bhaile barra dul chun cinn c\u00e9 go bhfuil an feidhmchl\u00e1r ag.",
		loadTitle : "Tosaigh iarratas '{appName}'",
		loadBundle : "{percent}% - {startedBundles}/{maxBundlesToStart} - Muirir {ainm}"
	},
	system : {
		bundleName : "C\u00f3ras",
		bundleDescription : "--Feidhmi\u00falacht CORE-- An C\u00f3ras cur s\u00edos ar an fheidhmi\u00falacht l\u00e1rnach bhun\u00fasach (creatlach compilable) de map.apps."
	},
	templatelayout : {
		bundleName : "F\u00e9ach ar Leagan Amach",
		bundleDescription : "--Feidhmi\u00falacht CORE-- an Teimpl\u00e9ad Leagan Amach bhfeidhm an socr\u00fa de gach heilimint\u00ed Chomh\u00e9adain bunaithe ar teimpl\u00e9id r\u00e9amhshainithe."
	},
	templates : {
		bundleName : "F\u00e9ach ar",
		bundleDescription : "--Feidhmi\u00falacht CORE-- Radhairc \u00fas\u00e1id ag an Leagan Amach Teimpl\u00e9ad.",
		templates : {
			desktop : {
				title : "Deasc",
				desc : "A leagan amach deisce-dh\u00edrithe."
			},
			modern : {
				title : "Nua-Aimseartha",
				desc : "A leagan amach nua-aimseartha stylish."
			},
			minimal : {
				title : "\u00cdosr\u00e1ta",
				desc : "A leagan amach \u00edostach"
			}
		},
		ui : {
			selectorLabelTitle : "F\u00e9ach ar"
		}
	},
	themes : {
		bundleName : "St\u00edl",
		bundleDescription : "--Feidhmi\u00falacht CORE-- Bainist\u00edonn an bundle T\u00e9ama\u00ed fhaisn\u00e9is go l\u00e9ir CSS mhaith dathanna, st\u00edleanna cl\u00f3, \u00edomh\u00e1nna c\u00falra etc Is f\u00e9idir le h\u00fas\u00e1ideoir\u00ed aistri\u00fa idir Breathna\u00edonn \u00e9ags\u00fala de na heilimint\u00ed comh\u00e9adan \u00fas\u00e1ideora ag roghn\u00fa st\u00edleanna \u00e9ags\u00fala.",
		themes : {
			pure : {
				desc : "Na map.apps st\u00edl '\u00edon'."
			},
			night : {
				desc : "Na map.apps 'o\u00edche' st\u00edl."
			}
		},
		ui : {
			selectorLabelTitle : "St\u00edl"
		}
	},
	windowmanager : {
		bundleName : "Fuinneog Bainist\u00edochta",
		bundleDescription : "--Feidhmi\u00falacht CORE-- Is \u00e9 an bainisteoir fuinneog at\u00e1 freagrach as bainisti\u00fa na an fhuinneog dial\u00f3g.",
		ui : {
			defaultWindowTitle : "Fuinneog",
			closeBtn : {
				title : "D\u00fan"
			},
			minimizeBtn : {
				title : "\u00cdoslaghdaigh"
			},
			maximizeBtn : {
				title : "Uasmh\u00e9adaigh"
			},
			restoreBtn : {
				title : "Athch\u00f3irigh"
			},
			opacityBtn : {
				title : "Athraigh tr\u00e9dhearcachta"
			},
			collapseBtn : {
				title : "\u00c1bhar hide"
			},
			loading : {
				title : "Fan go f\u00f3ill!",
				message : "Ag lucht\u00fa ..."
			},
			okcancel : {
				title : "Ceist",
				okButton : "Ceart go leor",
				cancelButton : "Cealaigh"
			}
		}
	},
	ec_legend : {
		bundleName : "Finsc\u00e9al",
		bundleDescription : "Sol\u00e1thra\u00edonn an bundle an finsc\u00e9al ESRI.",
		tool : {
			title : "Finsc\u00e9al",
			tooltip : "Finsc\u00e9al"
		}
	},
	ec_parameter_interceptor : {
		bundleName : "ec-parameter-interceptor",
		bundleDescription : "ec-parameter-interceptor",
		errors : {
			missingQueryParameter : "Missing query parameters",
			missingTypeQueryParameter : "Missing type query parameter",
			missingDatasetOrUrlQueryParameter : "Missing dataset or url query parameter",
			noComponentContextProvided : "No componentContext provided",
			noBundleContextProvided : "No bundleContext provided",
			noQueryParametersProvided : "No query parameters provided",
			datasetTypeNotSupported : "The type '${type}' is not supported",
			invalidGeoJSON : "Invalid GeoJSON",
			unsupportedCRSType : "Only named CRS are supported",
			canNotParseNamedCRS : "Can not parse named CRS to WKID: ${name}",
			mixingOfDifferentCRS : "Mixing of different CRS is not supported",
			invalidGeoJSONType : "Unknown GeoJSON type attribute: ${type}",
			typeNotFoundInResource : "No appropriate format for ${type} found in resource",
			couldNotLoadDataset : "Could not load dataset: ${cause}",
			unableToAddDataset : "Unable to add dataset: ${cause}",
			unableToParseCapabilities : "Unable to parse WMS Capabilities response",
			canNotDetermineUrl : "Could not determine the download URL of the resource"
		}
	},
	ec_feature_info : {
		tool : {
			title : "Eolas ar Ghn\u00E9ithe",
			tooltip : "Eolas ar Ghn\u00E9ithe"
		}
	},
	ec_map_loading_screen : {
		bundleName : "ec-map-loading-screen",
		bundleDescription : "ec-map-loading-screen",
		messages : {
			initializingMap : "L\u00E9arsc\u00E1il \u00E1 tionscnamh. Fan go f\u00F3ill!"
		}
	},
	ec_wms_layer_selector : {
		bundleName : "ec-wms-layer-selector",
		bundleDescription : "ec-wms-layer-selector",
		ui : {
			windowTitle : "Roghnaigh sraitheanna",
			hint : "T\u00E1 ${countToProvideSelection} sraith sa tseirbh\u00EDs WMS a roghna\u00EDodh, roghnaigh na sraitheanna uile ar ch\u00F3ir a bheith \u00E1 dtaispe\u00E1int sa l\u00E9arsc\u00E1il.",
			selectAll : "Roghnaigh uile",
			closeButton : "OK"
		},
		tool : {
			title : "Sraith",
			tooltip : "Sraith"
		}
	},
	ec_error_messages : {
		bundleName : "ec-error-messages",
		bundleDescription : "ec-error-messages",
		general : {
			serviceNotAvailable : "N\u00EDl an tseirbh\u00EDs a iarradh ar f\u00E1il",
			serviceNotSupported : "N\u00ED thaca\u00EDtear leis an tseirbh\u00EDs a iarradh",
			invalidResource : "Aims\u00EDodh acmhainn neamhbhail\u00ED ag an URL a tugadh",
			unsupportedResource : "Aims\u00EDodh acmhainn nach dtaca\u00EDtear l\u00E9i ag an URL a tugadh",
			errorSource : "Deimhn\u00EDodh an fhoinse earr\u00E1ide seo a leanas",
			requestedUrl : "URL iarrtha",
			unsupportedServiceType : "The provided service type is not supported",
			technicalSupport : "M\u00E1 leanann an fhadhb ar aghaidh amach anseo d\u00E9an teagmh\u00E1il leis an bhfoireann taca\u00EDochta teicni\u00FAla",
			technicalSupportCreateTicket : "Create support ticket",
			weWillContactProvider : "We will contact the provider of the service and report the issue.",
			closeButton : "OK"
		},
		detailedErrors : {
			crs : "N\u00ED chuireann an tseirbh\u00EDs c\u00F3ras tagartha comhordan\u00E1id\u00ED ar f\u00E1il",
			encoding : "N\u00ED chuireann an tseirbh\u00EDs ionch\u00F3d\u00FA carachtair a dtaca\u00EDtear leis ar f\u00E1il",
			metadata : "T\u00E1 doicim\u00E9ad meiteashonra\u00ED na seirbh\u00EDse earr\u00E1ideach.",
			fileFormat : "Fuarthas form\u00E1id sonra\u00ED neamhbhail\u00ED ag an URL a tugadh",
			dataRequest : "Agus sonra\u00ED na seirbh\u00EDse \u00E1 n-iarraidh fuarthas freagra neamhbhail\u00ED. D\u2019fh\u00E9adfadh iad seo a leanas a bheith mar ch\u00FAis leis, mar shampla: earr\u00E1id freastala\u00ED inmhe\u00E1nach, cumra\u00EDocht seirbh\u00EDse mh\u00EDcheart, iompar na seirbh\u00EDse nach bhfuil ag teacht leis an gcaighde\u00E1n srl."
		},
		httpIssues : {
			303 : "T\u00E1 an freagra ar an iarratas ar f\u00E1il faoi URI eile",
			305 : "N\u00EDl an acmhainn a iarradh ar f\u00E1il ach tr\u00ED sheachfhreastala\u00ED",
			400 : "N\u00ED f\u00E9idir leis an bhfreastala\u00ED an t-iarratas a phr\u00F3ise\u00E1il n\u00F3 n\u00ED dh\u00E9anfaidh s\u00E9 \u00E9 a phr\u00F3ise\u00E1il de dheasca earr\u00E1id chliaint airithe",
			401 : "Bh\u00ED g\u00E1 le h\u00FAdar\u00FA na hacmhainne a iarradh",
			403 : "N\u00EDl an acmhainn a iarradh ar f\u00E1il go poibl\u00ED",
			404 : "N\u00EDor aims\u00EDodh an acmhainn a iarradh",
			405 : "Iarradh an acmhainn tr\u00ED mhodh HTTP nach bhfuil ceadaithe",
			406 : "Iarradh inneachar \u00F3n acmhainn nach f\u00E9idir glacadh leis",
			407 : "T\u00E1 g\u00E1 an acmhainn a r\u00E9amh-\u00FAdar\u00FA ag seachsheirbh\u00EDs",
			500 : "N\u00EDl an acmhainn ar f\u00E1il de dheasca earr\u00E1id freastala\u00ED inmhe\u00E1naigh",
			501 : "N\u00EDl an acmhainn ar f\u00E1il de dheasca modh iarraidh nach n-aithn\u00EDtear",
			502 : "N\u00EDl an acmhainn ar f\u00E1il de dheasca geata at\u00E1 cumraithe go m\u00EDcheart",
			503 : "N\u00EDl an acmhainn ar f\u00E1il faoi l\u00E1thair de dheasca r\u00F3-ualaithe n\u00F3 oibre cothabh\u00E1la",
			504 : "N\u00EDl an acmhainn ar f\u00E1il de dheasca geata neamhfhreagrach n\u00F3 de dheasca \u00E9 a bheith cumraithe go m\u00EDcheart",
			505 : "N\u00ED thaca\u00EDonn an acmhainn leis an leagan HTTP tugtha",
			generic : "Tharla earr\u00E1id freastala\u00ED le linn an acmhainn thugtha a bheith \u00E1 hiarraidh."
		}
	},
	ec_user_tutorial : {
		bundleName : "ec-user-tutorial",
		bundleDescription : "ec-user-tutorial",
		externalResourceDisclaimer : {
			introduction : "D\u00E9anann an t-acmharc\u00F3ir seo rochtain ar sheirbh\u00EDs\u00ED seachtracha agus taispe\u00E1nann a gcuid sonra\u00ED. N\u00EDl na seirbh\u00EDs\u00ED seachtracha seo \u00E1 gcothabh\u00E1il ag an gCoimisi\u00FAn Eorpach. D\u00E1 bhr\u00ED sin n\u00EDl aon tionchar againn ar a n-infhaighteacht/a gcobhsa\u00EDocht. D\u2019fh\u00E9adfadh na fadhbanna seo a leanas teacht chun cinn:",
			rotatingCircle : "L\u00E9ir\u00EDonn ciorcal rothlach san amharc l\u00E9arsc\u00E1ile go bhfuil an t-amharc\u00F3ir ag feitheamh le freagra \u00F3 fhoinse sonra\u00ED. D\u2019fh\u00E9adfadh s\u00E9 gur freastala\u00ED nach bhfuil ar f\u00E1il n\u00F3 fadhbanna l\u00EDonra is c\u00FAis leis seo",
			layerZoomLevel : "D\u2019fh\u00E9adfadh s\u00E9 nach dtaispe\u00E1nfa\u00ED foins\u00ED sraithe \u00E1irithe ag leibh\u00E9il z\u00FAm\u00E1la \u00E1irithe. T\u00E1 sin amhlaidh mar thoradh ar shrianta a rinne an sol\u00E1thra\u00ED seirbh\u00EDse seachtrach. B\u2019fh\u00E9idir go mbeadh ort z\u00FAm\u00E1il isteach n\u00F3 amach d\u2019fhonn sonra\u00ED na sraithe a thaispe\u00E1int",
			layerPanning : "D\u2019fh\u00E9adfadh sonra\u00ED a nd\u00E9antar rochtain orthu \u00F3 fhreastala\u00ED seachtrach a bheith suite lasmuigh den amharc l\u00E9arsc\u00E1ile reatha. D\u00E9anann an t-amharc\u00F3ir l\u00E9arsc\u00E1ile seo iarracht go huathoibr\u00EDoch chun an limist\u00E9ar cl\u00FAdaithe a shainaithint. Mar sin f\u00E9in t\u00E1 c\u00E1sanna \u00E1irithe nach bhfuil na meiteashonra\u00ED ioml\u00E1n n\u00F3 b\u00EDonn earr\u00E1id\u00ED iontu ar shl\u00ED go mb\u00EDonn ort pean\u00E1il ar an raon iarbh\u00EDr.",
			detailInformation : "M\u00E1 iarrann t\u00FA faisn\u00E9is mhionsonraithe maidir le gn\u00E9 roghnaithe l\u00E1imhseofar \u00E9 seo tr\u00ED iarratas saini\u00FAil chuig freastala\u00ED na l\u00E9arsc\u00E1ile. M\u00E1 t\u00E1 an freagra ar an iarratas seo m\u00EDchumtha n\u00F3 folamh (m. sh. de dheasca earr\u00E1id freastala\u00ED) n\u00ED f\u00E9idir aon fhaisn\u00E9is a thaispe\u00E1int agus taispe\u00E1nfar fuinneog fholamh.",
			mapScaling : "Is \u00ED Oifig staidrimh an Aontais Eorpaigh (Eurostat) a chuireann na bonnl\u00E9arsc\u00E1ileanna a \u00FAs\u00E1idtear sa bhfeidhmchl\u00E1r seo ar f\u00E1il. T\u00E1 na l\u00E9arsc\u00E1ileanna seo ar f\u00E1il faoi l\u00E1thair le gl\u00E9ine suas chuig 1:50.000. T\u00E1 bonnl\u00E9arsc\u00E1ileanna n\u00EDos m\u00EDne \u00E1 n-ullmh\u00FA faoi l\u00E1thair, (m.sh. chun tac\u00FA le l\u00E9irshamhl\u00FA sonra\u00ED laistigh de chathair). D\u00E1 bhr\u00ED sin d\u2019fh\u00E9adfadh nach dtaispe\u00E1ntar foins\u00ED sonra\u00ED \u00E1irithe f\u00F3s leis an l\u00E9arsc\u00E1il c\u00FAlra is oiri\u00FAna\u00ED.",
			technicalSupport : "M\u00E1 thagann aon fhadhbanna eile romhat n\u00E1 b\u00EDodh leisce ort teagmh\u00E1il a dh\u00E9anamh leis an bhfoireann taca\u00EDochta teicni\u00FAla:",
			technicalSupportContactLink : "Teagmh\u00E1il leis an bhfoireann taca\u00EDochta teicni\u00FAla",
			mapLoading : "L\u00E9arsc\u00E1il \u00E1 l\u00F3d\u00E1il...",
			donotshow : "N\u00E1 taispe\u00E1in an sc\u00E1ile\u00E1n f\u00E1iltithe ar\u00EDs."
		},
		legalNotice : "N\u00ED thugann na hainmni\u00FAch\u00E1in n\u00E1 an l\u00E1ithreoireacht \u00E1bhair ar an l\u00E9arsc\u00E1il seo le tuiscint go gcuirtear aon tuairim ar bith in i\u00FAl \u00F3 thaobh an Aontais Eorpaigh de, maidir le st\u00E1das dl\u00ED t\u00EDre, cr\u00EDche, cathrach n\u00E1 limist\u00E9ir ar bith n\u00E1 maidir lena cuid \u00FAdar\u00E1s, n\u00E1 maidir le teorann\u00FA a himeallchr\u00EDocha n\u00E1 a teorainneacha.  An Chosaiv*: D\u00E9antar an t-ainmni\u00FA seo gan dochar do sheasaimh i dtaobh st\u00E1dais agus t\u00E1 s\u00E9 de r\u00E9ir UNSCR (R\u00FAn \u00F3 Chomhairle Sl\u00E1nd\u00E1la na N\u00E1isi\u00FAn Aontaithe) 1244/1999 agus Tuairim na C\u00FAirte Breithi\u00FAnais Idirn\u00E1isi\u00FAnta ar Fhor\u00F3gra Neamhsple\u00E1chais na Cosaive.  an Phalaist\u00EDn*: N\u00ED fhorl\u00E9ireofar an t-ainmni\u00FA seo mar aitheantas St\u00E1t na Palaist\u00EDne agus d\u00E9antar \u00E9 gan dochar do sheasamh na mBallst\u00E1t ar leith maidir leis an gceist seo.",
		legalNoticeHeader : "S\u00E9anadh dl\u00EDthi\u00FAil",
		tutorial : {
			welcome : "F\u00E1ilte chuig amharc\u00F3ir l\u00E9arsc\u00E1ile na Tairs\u00ED Sonra\u00ED Oscailte!",
			familiarise : "Is \u00E9 is cusp\u00F3ir don eolas tosaigh gairid seo n\u00E1 t\u00FA a chur ar an eolas faoi ghn\u00E9ithe agus fheidhmi\u00FAlacht an amharc\u00F3ir l\u00E9arsc\u00E1ile.",
			navigation : "Clice\u00E1il agus coinnigh an cnaipe luiche cl\u00E9 chun pean\u00E1il ar an l\u00E9arsc\u00E1il.",
			zoom : "Athra\u00EDonn na cnaip\u00ED seo an leibh\u00E9al z\u00FAm\u00E1la ar an l\u00E9arsc\u00E1il. Mar rogha air seo is f\u00E9idir leat roth na luiche a \u00FAs\u00E1id chun an z\u00FAm a choigeart\u00FA.",
			features : "T\u00E1 feidhmi\u00FAlacht bhreise ar f\u00E1il tr\u00EDd na cnaip\u00ED seo. Feidhm\u00EDonn siad mar scor\u00E1in agus cumasa\u00EDonn n\u00F3 d\u00EDchumasa\u00EDonn siad an fheidhmi\u00FAlacht thugtha.",
			legend : "Is f\u00E9idir an eochair eolais a \u00FAs\u00E1id chun na sraitheanna l\u00E9arsc\u00E1ile at\u00E1 ar f\u00E1il a scr\u00FAd\u00FA agus a dtaispe\u00E1int reatha ar an l\u00E9arsc\u00E1il a chumas\u00FA n\u00F3 a dh\u00EDchumas\u00FA. D\u00EDortha\u00EDtear na hainmneacha go d\u00EDreach \u00F3n tseirbh\u00EDs a dh\u00E9antar rochtain uirthi go seachtrach.",
			transparency : "Is f\u00E9idir leat tr\u00E9dhearcacht sraithe a choigeart\u00FA, leis.",
			featureinfo : "Is f\u00E9idir mionscr\u00FAd\u00FA a dh\u00E9anamh ar chuid de na sonra\u00ED fi\u00FA amh\u00E1in. Nuair a bh\u00EDonn an ghn\u00E9 seo gn\u00EDomhachtaithe is f\u00E9idir leat clice\u00E1il ar an l\u00E9arsc\u00E1il chun faisn\u00E9is bhreise a iarraidh.",
			done : "D\u00E9anta",
			okButton : "OK",
			closeButton : "D\u00FAn",
			skip : "F\u00E1g ar l\u00E1r",
			next : "Ar aghaidh",
			back : "Siar"
		}
	}
});