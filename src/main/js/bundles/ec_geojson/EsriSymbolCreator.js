define([
  "dojo/_base/declare",
  "esri/symbols/SimpleMarkerSymbol",
  "esri/symbols/SimpleLineSymbol",
  "esri/symbols/SimpleFillSymbol"
], function(declare, SimpleMarkerSymbol, SimpleLineSymbol, SimpleFillSymbol) {
  return declare([], {
    _symbols: {
      point: {
        color: [255, 255, 255, 64],
        size: 5,
        angle: -30,
        xoffset: 0,
        yoffset: 0,
        type: "esriSMS",
        style: "esriSMSCircle",
        outline: {
          color: [0, 0, 0, 128],
          width: 2,
          type: "esriSLS",
          style: "esriSLSSolid"
        }
      },
      line: {
        color: [255, 0, 0, 255],
        width: 2,
        type: "esriSLS",
        style: "esriSLSSolid"
      },
      polygon: {
        color: [0, 0, 255, 128],
        width: 2,
        type: "esriSFS",
        style: "esriSFSSolid",
        outline: {
          color: [0, 0, 255, 255],
          width: 2,
          type: "esriSLS",
          style: "esriSLSSolid"
        }
      }
    },
    constructor: function(options) {
      this._symbols.point = options.point || this._symbols.point;
      this._symbols.line = options.point || this._symbols.line;
      this._symbols.polygon = options.point || this._symbols.polygon;
    },
    _getSymbol: function(feature) {
      if (!feature.geometry) return;
      switch (feature.geometry.type) {
        case "point":
          return new SimpleMarkerSymbol(this._symbols.point);
        case "polyline":
          return new SimpleLineSymbol(this._symbols.line);
        case "polygon":
          return new SimpleFillSymbol(this._symbols.polygon);
      }
    },
    addEsriSymbolToFeature: function(feature) {
      var symbol = this._getSymbol(feature);
      if (symbol) {
        feature.symbol = symbol.toJson();
      }
    }
  });
});
