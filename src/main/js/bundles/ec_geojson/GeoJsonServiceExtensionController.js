define([
  "dojo/_base/declare",
  "dojo/_base/array",
  "dojo/_base/Deferred",
  "ct/array",
  "ct/_when",
  "ec_base/mapping/MappingResourceUtils",
  "ec_base/mapping/MapModelUtils",
  "ct/mapping/mapcontent/MappingResourceFactory"
], function(
  declare,
  d_array,
  Deferred,
  ct_array,
  ct_when,
  MappingResourceUtils,
  MapModelUtils,
  MappingResourceFactory
) {
  return declare([], {
    _servicesFromCatalogue: {},
    constructor: function() {
      this._mrFactory = new MappingResourceFactory();
      this._transformer = new Deferred();
    },
    activate: function() {
      this.insertionNode = this.mapModel.getOperationalLayer();
    },
    setGeoJsonTransformer: function(geoJsonTransformer) {
      this._transformer.resolve(geoJsonTransformer);
    },
    load: function(resource) {
      try {
        ct_when(
          this._getFeatures(resource),
          function(features) {
            var url = resource.getURL();
            var title =
              resource.getTitle() || url.substring(url.lastIndexOf("/") + 1);
            var res = {
              service: {
                TITLE: "GeoJSON",
                URL: url,
                SERVICE_TYPE: "GeoJSON",
                DATA: features,
                LAYERS: []
              },
              layerIds: []
            };
            var types = this._getTypes(features);
            d_array.forEach(
              types,
              function(type) {
                var layerId =
                  this._removeSlashesAndColonsFromString(url) + "__" + type;
                res.service.LAYERS.push({ ID: layerId, TITLE: title });
                res.layerIds.push(layerId);
              },
              this
            );
            this._handleLoadLayers(res);
          },
          function(error) {
            console.error(error);
          },
          this
        );
      } catch (error) {
        console.error(error);
      }
    },
    _removeSlashesAndColonsFromString: function(s) {
      return s.replace(/[:/]/g, "");
    },
    _getTypes: function(features) {
      var types = [];
      d_array.forEach(
        features,
        function(feature) {
          if (d_array.indexOf(types, feature.geometry.type) < 0) {
            types.push(feature.geometry.type);
          }
        },
        this
      );
      return types;
    },
    _getFeatures: function(resource) {
      return this._transformer.then(function(transformer) {
        var features = transformer.geojsonToGeometry(
          resource.getPayload(),
          resource.getExtent()
        );
        if (!features.length || features.length === 0) {
          throw new Error(
            "There are no features in the loaded dataset for url: " +
              resource.getURL()
          );
        }
        return features;
      });
    },
    _getType: function(type) {
      return type;
    },
    _handleLoadLayers: function(evt) {
      var serviceDescription = evt.service;
      var url = serviceDescription.URL;
      var type = this._getType(serviceDescription.SERVICE_TYPE);
      var title = serviceDescription.TITLE;
      var layers = evt.layerIds;
      var service = MappingResourceUtils.getServiceResource(
        this.mrr,
        this._mrFactory,
        url,
        type,
        title
      );
      var addedNode = null;
      service.data = serviceDescription.DATA;

      d_array.forEach(
        layers,
        function(l) {
          var layer = ct_array.arraySearchFirst(serviceDescription.LAYERS, {
            ID: l
          });
          if (layer) {
            var genID = url + layer.ID;
            var serviceMapModelNode = this.mapModel.getNodeById(genID);
            if (!serviceMapModelNode) {
              serviceMapModelNode = MappingResourceUtils.addServiceMapModelNode(
                service,
                layer.TITLE || layer.ID || title,
                this.insertionNode,
                genID,
                MapModelUtils.getNextRenderPriority(this.mapModel)
              );
              serviceMapModelNode.added = true;
              serviceMapModelNode.fromCatalogue = this._servicesFromCatalogue[
                url
              ];
              MappingResourceUtils.addLayer(
                this.mrr,
                this._mrFactory,
                service,
                { id: layer.ID, title: layer.TITLE },
                type,
                serviceMapModelNode
              );
              addedNode = serviceMapModelNode;
            }
          }
        },
        this
      );

      if (addedNode) {
        this.mapModel.fireModelStructureChanged({
          source: this,
          dynamicAdd: true
        });
      }
    }
  });
});
