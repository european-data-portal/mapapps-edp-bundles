define([
  "dojo/_base/array",
  "dojo/_base/declare",
  "./geojson",
  "esri/geometry/jsonUtils"
], function(d_array, declare, GeoJSON, EsriGeometry) {
  return declare([], {
    setSymbolCreator: function(symbolCreator) {
      this.symbolCreator = symbolCreator;
    },
    geojsonToGeometry: function(featureCollection, extent) {
      var wkid = extent.spatialReference.wkid;
      var collection = GeoJSON.toGeometry(featureCollection, wkid);
      var symbolCreator = this.symbolCreator;
      if (!symbolCreator) {
        console.warn(
          "SymbolAddingGeoJsonTransformer: No symbol creator defined."
        );
        return EsriGeometry.fromJson(collection);
      }
      d_array.forEach(collection, function(feature) {
        feature.geometry = EsriGeometry.fromJson(feature.geometry);
        symbolCreator.addEsriSymbolToFeature(feature);
      });
      return collection;
    }
  });
});
