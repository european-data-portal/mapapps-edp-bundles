/*
 * Patched version of CT ContentViewer
 */
define(["contentviewer/ContentViewer", "dojo/_base/connect"], function(
  ContentViewer,
  d_connect
) {
  // patch this methode to ignore onClose of a treenode for the feature of interest for geojson layers
  ContentViewer.prototype._setupConnections = function(contentWidget, window) {
    // support onClose events in content widgets
    var obsoleteWatch = d_connect.connect(
      contentWidget,
      "onClose",
      this,
      function(evt, node) {
        if (node.tree) return;
        window.close();
        if (evt.message && this.logger) {
          this.logger.info(evt.message);
        }
      }
    );

    // update title changes in window
    var titleWatch = contentWidget.watch("title", function(
      name,
      oldTitle,
      newTitle
    ) {
      window.set("title", newTitle);
    });

    var watchCleanup = d_connect.connect(
      window,
      "onClose",
      this,
      function() {
        d_connect.disconnect(watchCleanup);
        titleWatch.unwatch();
        d_connect.disconnect(obsoleteWatch);
      }
    );
  };
  return ContentViewer;
});
