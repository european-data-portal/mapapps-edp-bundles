define(["dojo/dom-construct", "dojo/query", "dojo/_base/window"], function(
  domConstruct,
  query,
  win
) {
  var showErrorMessagesWindow = function(ui) {
    var existingErrorWidgets = query(".error-messages");
    if (!existingErrorWidgets || existingErrorWidgets.length === 0) {
      //we disable this, at its very buggy
      //            ui.overlayUtils.addGlobalOverlay();
    }

    var div = domConstruct.create(
      "div",
      { class: "error-messages" },
      win.body()
    );
    domConstruct.place(ui.domNode, div);
  };

  var removeErrorMessagesWindow = function() {
    query(".error-messages").forEach(domConstruct.destroy);
  };

  return {
    showErrorMessagesWindow: showErrorMessagesWindow,
    removeErrorMessagesWindow: removeErrorMessagesWindow
  };
});
