define([
  "dojo/_base/declare",
  "dijit/_Widget",
  "dijit/_TemplatedMixin",
  "dijit/_WidgetsInTemplateMixin",
  "./Utils",
  "dojo/text!./templates/service-not-available.html",
  "dojo/text!./templates/service-not-supported.html",
  "dijit/layout/BorderContainer",
  "dijit/layout/ContentPane",
  "dijit/form/Button"
], function(
  declare,
  _Widget,
  _TemplatedMixin,
  _WidgetsInTemplateMixin,
  Utils,
  notAvailableTemplate,
  notSupportedTemplate,
  BorderContainer,
  ContentPane,
  Button
) {
  return declare([_Widget, _TemplatedMixin, _WidgetsInTemplateMixin], {
    //baseClass: "ecErrorMessagesWrapper",
    templateString: notSupportedTemplate,
    constructor: function(args) {
      switch (args.type) {
        case "notSupported":
          this.templateString = notSupportedTemplate;
          break;
        case "notAvailable":
          this.templateString = notAvailableTemplate;
          break;
      }
      args.url = args.url || "";
      args.http = args.http || "";
      args.detailed = args.detailed || "";
      this.showProvider = args.showContactProvider
        ? args.ticketMessages.general.weWillContactProvider
        : "";
      this.overlayUtils = args.overlayUtils;
      this._ticketMessages = args.ticketMessages;
      this.error = args;
    },
    buildRendering: function() {
      this.inherited(arguments);
    },
    _openSupportPage: function() {
      window.open("/feedback/form?" + this._createQueryString());
    },
    _createQueryString: function() {
      var query = "type=0&summary=" + this._createSummary();
      query += "&description=" + this._createDescription();
      query +=
        "&dataset=" +
        (this.error.ckanResource ? this.error.ckanResource.getId() : "n/a");
      query += "&component=" + "geo-visualisation";
      query +=
        "&errortype=" +
        this.error.reason +
        " (" +
        this.error.httpCode +
        "); Type=" +
        (this.error.ckanResource ? this.error.ckanResource.getType() : "n/a");
      return query;
    },
    _createSummary: function() {
      var result;
      switch (this.error.type) {
        case "notSupported":
          result = this._ticketMessages.general.serviceNotSupported;
        case "notAvailable":
          result = this._ticketMessages.general.serviceNotAvailable;
        default:
          result = "Generic Error";
      }

      result +=
        "; Dataset: " +
        (this.error.ckanResource ? this.error.ckanResource.getId() : "n/a");
      return result;
    },
    _createDescription: function() {
      var result = "";
      switch (this.error.type) {
        case "notSupported":
          result += this._ticketMessages.general.serviceNotSupported;
          result += ". " + this._ticketMessages.general.invalidResource;
          result += ". " + this._ticketMessages.general.errorSource;
          break;
        case "notAvailable":
          result += this._ticketMessages.general.serviceNotAvailable;
          result += ". " + this._ticketMessages.general.invalidResource;
          result += ". " + this._ticketMessages.general.errorSource;
          break;
        default:
          result += "No additional information available";
          break;
      }

      result += "; HTTP Status: " + this.error.httpCode;
      result += "; URL: " + this.error.url;

      return result;
    },
    _close: function() {
      Utils.removeErrorMessagesWindow();
      //we disable this, at its very buggy
      //                            this.overlayUtils.removeGlobalOverlay();
    }
  });
});
