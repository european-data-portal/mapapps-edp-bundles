define({
	europeanCommission: "Europeiska kommissionen",
	lang: "sv",
	headerTitle: "Europeiska Data Portalen",
	betaPageLink: "sv/what-we-do",
	legalNotice: "R\u00e4ttsligt meddelande",
	legalNoticeLink: "sv/legal-notice",
	cookies: "Cookies",
	cookiesLink: "sv/cookies",
	contact: "Kontakta",
	search: "S\u00f6k",
	legalDisclaimer: "Ansvarsfriskrivning",
	routing: {
		title: "Routing"
	},
	streetview: {
		title: "Street View"
	},
	mapdesk: {
		title: "Kart Desk"
	},
	overviewmap: {
		title: "\u00d6versikt Karta"
	},
	mapflow: {
		title: "Karta Fl\u00f6de"
	},
	followme: {
		title: "F\u00f6lj mig"
	},
	magnifier: {
		title: "F\u00f6rstoring"
	},
	gallery: {
		title: "Galleri"
	},
	resultcenter: {
		title: "Kunskapsf\u00f6rmedlingen"
	},
	measurement: {
		title: "M\u00e4tningsverktyg"
	},
	bookmarks: {
		title: "Spatiala bokm\u00e4rken"
	},
	parameterURL: {
		title: "L\u00e4nk Tool"
	},
	tableofcontents: {
		title: "TOC"
	},
	legend: {
		title: "Legend"
	},
	layerSelector: {
		title: "V\u00E4lj niv\u00E5"
	},
	coordfinder: {
		title: "Samordna Finder"
	},
	agsPringing: {
		title: "AGS Utskrift"
	},
	addThis: {
		title: "Social Bookmarking Tool"
	},
	agolmapFinder: {
		title: "ArcGIS Online Web Map Finder"
	},
	drawSymbolChooser: {
		title: "V\u00e4lj en symbol f\u00f6r att placera p\u00e5 kartan ..."
	},
	redliningStyleProperties: {
		title: "Rita Egenskaper"
	}
});