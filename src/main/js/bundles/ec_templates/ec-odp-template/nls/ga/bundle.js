define({
	europeanCommission: "An Coimisi\u00FAn Eorpach",
	lang: "ga",
	headerTitle: "TAIRSEACH SONRA\u00CD OSCAILTE",
	betaPageLink: "ga/what-we-do",
	legalNotice: "F\u00F3gra dl\u00ED",
	legalNoticeLink: "ga/legal-notice",
	cookies: "Fian\u00E1in",
	cookiesLink: "ga/cookies",
	contact: "Teagmh\u00E1il",
	search: "Cuardach",
	legalDisclaimer: "S\u00E9anadh dl\u00EDthi\u00FAil",
	routing: {
		title: "R\u00F3d\u00FA"
	},
	streetview: {
		title: "Street View"
	},
	mapdesk: {
		title: "L\u00E9arsc\u00E1il Deasc"
	},
	overviewmap: {
		title: "Forbhreathn\u00FA Mapa"
	},
	mapflow: {
		title: "L\u00E9arsc\u00E1il Sreabhadh"
	},
	followme: {
		title: "Lean Me"
	},
	magnifier: {
		title: "Formh\u00E9adaitheoir"
	},
	gallery: {
		title: "Gaileara\u00ED"
	},
	resultcenter: {
		title: "Toradh Center"
	},
	measurement: {
		title: "Uirlis\u00ED tomhais"
	},
	bookmarks: {
		title: "Leabharmharcanna sp\u00E1s\u00FAil"
	},
	parameterURL: {
		title: "Uirlis Nasc"
	},
	tableofcontents: {
		title: "TOC"
	},
	legend: {
		title: "Finsc\u00E9al"
	},
	layerSelector: {
		title: "Roghnaigh sraitheanna"
	},
	coordfinder: {
		title: "Comhord\u00fa a Aimsitheoir"
	},
	agsPringing: {
		title: "Priont\u00e1il AGS"
	},
	addThis: {
		title: "Bookmarking Uirlis S\u00f3isialta"
	},
	agolmapFinder: {
		title: "ArcGIS Online Gr\u00e9as\u00e1in L\u00e9arsc\u00e1il Aimsitheoir"
	},
	drawSymbolChooser: {
		title: "Roghnaigh siombail a chur ar an l\u00e9arsc\u00e1il ..."
	},
	redliningStyleProperties: {
		title: "Air\u00edonna Tarraing"
	}
});