define({
	europeanCommission: "Commission europ\u00e9enne",
	lang: "fr",
	headerTitle: "Portail europ\u00e9en de donn\u00e9es",
	betaPageLink: "fr/what-we-do",
	legalNotice: "Avis juridique",
	legalNoticeLink: "fr/legal-notice",
	cookies: "Cookies",
	cookiesLink: "fr/cookies",
	contact: "Contact",
	search: "Recherche",
	legalDisclaimer: "Avis juridique",
	routing: {
		title: "Routage"
	},
	streetview: {
		title: "Street View"
	},
	mapdesk: {
		title: "Carte bureau"
	},
	overviewmap: {
		title: "Aper\u00e7u Carte"
	},
	mapflow: {
		title: "Carte de d\u00e9bit"
	},
	followme: {
		title: "Suivez-Moi"
	},
	magnifier: {
		title: "Loupe"
	},
	gallery: {
		title: "Galerie"
	},
	resultcenter: {
		title: "Centre de R\u00e9sultat"
	},
	measurement: {
		title: "Outils de mesure"
	},
	bookmarks: {
		title: "Signets spatiales"
	},
	parameterURL: {
		title: "Lien Outil"
	},
	tableofcontents: {
		title: "TOC"
	},
	legend: {
		title: "L\u00e9gende"
	},
	layerSelector: {
		title: "Choisir le niveau"
	},
	coordfinder: {
		title: "Coordonner Finder"
	},
	agsPringing: {
		title: "Impression AGS"
	},
	addThis: {
		title: "Bookmarking outil social"
	},
	agolmapFinder: {
		title: "ArcGIS Online Carte Web Finder"
	},
	drawSymbolChooser: {
		title: "Choisissez un symbole \u00e0 placer sur la carte ..."
	},
	redliningStyleProperties: {
		title: "Dessinez Propri\u00e9t\u00e9s"
	}
});