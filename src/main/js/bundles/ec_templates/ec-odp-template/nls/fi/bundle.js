define({
	europeanCommission: "Euroopan komissio",
	lang: "fi",
	headerTitle: "Euroopan Dataportaali",
	betaPageLink: "fi/what-we-do",
	legalNotice: "Oikeudellinen huomautus",
	legalNoticeLink: "fi/legal-notice",
	cookies: "Ev\u00e4steet",
	cookiesLink: "fi/cookies",
	contact: "Ota yhteyttä",
	search: "Haku",
	legalDisclaimer: "Vastuuvapauslauseke",
	routing: {
		title: "Routing"
	},
	streetview: {
		title: "Street View"
	},
	mapdesk: {
		title: "Kartta Desk"
	},
	overviewmap: {
		title: "Yleist\u00e4 Kartta"
	},
	mapflow: {
		title: "Kartta Flow"
	},
	followme: {
		title: "Follow Me"
	},
	magnifier: {
		title: "Suurennuslasi"
	},
	gallery: {
		title: "Galleria"
	},
	resultcenter: {
		title: "Tulos keskus"
	},
	measurement: {
		title: "Mittaus Ty\u00f6kalut"
	},
	bookmarks: {
		title: "Spatial Kirjanmerkit"
	},
	parameterURL: {
		title: "Linkki Tool"
	},
	tableofcontents: {
		title: "TOC"
	},
	legend: {
		title: "Selite"
	},
	layerSelector: {
		title: "Valitse kerrokset"
	},
	coordfinder: {
		title: "Koordinaatti Finder"
	},
	agsPringing: {
		title: "AGS Tulostus"
	},
	addThis: {
		title: "Sosiaaliset kirjanmerkit Tool"
	},
	agolmapFinder: {
		title: "ArcGIS Online Web Map Finder"
	},
	drawSymbolChooser: {
		title: "Valitse symboli sijoittaa kartalle ..."
	},
	redliningStyleProperties: {
		title: "Piirr\u00e4 Ominaisuudet"
	}
});