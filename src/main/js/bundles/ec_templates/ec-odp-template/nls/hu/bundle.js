define({
	europeanCommission: "Eur\u00f3pai Bizotts\u00e1g",
	lang: "hu",
	headerTitle: "Eur\u00F3pai Adatport\u00E1l",
	betaPageLink: "hu/what-we-do",
	legalNotice: "Jogi nyilatkozat",
	legalNoticeLink: "hu/legal-notice",
	cookies: "Cookie-k",
	cookiesLink: "hu/cookies",
	contact: "Kapcsolat",
	search: "Keres\u00e9s",
	legalDisclaimer: "Jogi nyilatkozat",
	routing: {
		title: "Routing"
	},
	streetview: {
		title: "Street View"
	},
	mapdesk: {
		title: "T\u00e9rk\u00e9p Desk"
	},
	overviewmap: {
		title: "\u00c1ttekint\u0151 t\u00e9rk\u00e9p"
	},
	mapflow: {
		title: "T\u00e9rk\u00e9p Flow"
	},
	followme: {
		title: "Follow Me"
	},
	magnifier: {
		title: "Nagy\u00edt\u00f3\u00fcveg"
	},
	gallery: {
		title: "Karzat"
	},
	resultcenter: {
		title: "Eredm\u00e9ny Center"
	},
	measurement: {
		title: "M\u00e9r\u0151eszk\u00f6z\u00f6k"
	},
	bookmarks: {
		title: "T\u00e9rbeli k\u00f6nyvjelz\u0151k"
	},
	parameterURL: {
		title: "Link eszk\u00f6z"
	},
	tableofcontents: {
		title: "TOC"
	},
	legend: {
		title: "Jelmagyar\u00E1zat"
	},
	layerSelector: {
		title: "R\u00E9tegek kiv\u00E1laszt\u00E1sa"
	},
	coordfinder: {
		title: "Koordin\u00e1ta-Finder"
	},
	agsPringing: {
		title: "AGS Nyomtat\u00e1s"
	},
	addThis: {
		title: "T\u00e1rsadalmi K\u00f6nyvjelz\u0151 eszk\u00f6z"
	},
	agolmapFinder: {
		title: "ArcGIS Online Web-t\u00e9rk\u00e9p Finder"
	},
	drawSymbolChooser: {
		title: "V\u00e1lasszon egy szimb\u00f3lumot, hogy helyezze a t\u00e9rk\u00e9pen ..."
	},
	redliningStyleProperties: {
		title: "D\u00f6ntetlen Properties"
	}
});