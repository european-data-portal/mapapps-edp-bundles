define({
	europeanCommission: "Evropska komisija",
	lang: "sl",
	headerTitle: "EVROPSKI PODATKOVNI PORTAL",
	betaPageLink: "sl/what-we-do",
	legalNotice: "Pravno obvestilo",
	legalNoticeLink: "sl/legal-notice",
	cookies: "Pi\u0161kotki",
	cookiesLink: "en/cookies",
	contact: "Kontakt",
	search: "Iskanje",
	legalDisclaimer: "Izjava o omejitvi odgovornosti",
	routing: {
		title: "Usmerjanje"
	},
	streetview: {
		title: "Street View"
	},
	mapdesk: {
		title: "Map Desk"
	},
	overviewmap: {
		title: "Pregled na zemljevidu"
	},
	mapflow: {
		title: "Map Flow"
	},
	followme: {
		title: "Sledite mi"
	},
	magnifier: {
		title: "Lupa"
	},
	gallery: {
		title: "Galerija"
	},
	resultcenter: {
		title: "Center rezultat"
	},
	measurement: {
		title: "Merilno orodje"
	},
	bookmarks: {
		title: "Prostorski Zaznamki"
	},
	parameterURL: {
		title: "Link Tool"
	},
	tableofcontents: {
		title: "TOC"
	},
	legend: {
		title: "Legend"
	},
	layerSelector: {
		title: "Izberi sloje"
	},
	coordfinder: {
		title: "Usklajuje Finder"
	},
	agsPringing: {
		title: "AGS tiskanje"
	},
	addThis: {
		title: "Social Bookmarking orodje"
	},
	agolmapFinder: {
		title: "ArcGIS Online Web Map Finder"
	},
	drawSymbolChooser: {
		title: "Izberite simbol, da se na zemljevidu ..."
	},
	redliningStyleProperties: {
		title: "Draw Lastnosti"
	}
});