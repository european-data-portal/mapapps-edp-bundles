define({
	europeanCommission: "Europa-Kommissionen",
	lang: "da",
	headerTitle: "DEN EUROP\u00C6ISKE DATAPORTAL",
	betaPageLink: "da/what-we-do",
	legalNotice: "Juridisk meddelelse",
	legalNoticeLink: "da/legal-notice",
	cookies: "Cookies",
	cookiesLink: "da/cookies",
	contact: "Kontakt",
	search: "S\u00F8gning",
	legalDisclaimer: "Ansvarsfraskrivelse",
	routing: {
		title: "Routing"
	},
	streetview: {
		title: "Street View"
	},
	mapdesk: {
		title: "Kort Desk"
	},
	overviewmap: {
		title: "Oversigtskort"
	},
	mapflow: {
		title: "Kort Flow"
	},
	followme: {
		title: "Follow Me"
	},
	magnifier: {
		title: "Magnifier"
	},
	gallery: {
		title: "Galleri"
	},
	resultcenter: {
		title: "Resultat center"
	},
	measurement: {
		title: "M\u00E5lev\u00E6rkt\u00F8jer"
	},
	bookmarks: {
		title: "Rumlig bogm\u00E6rker"
	},
	parameterURL: {
		title: "Link Tool"
	},
	tableofcontents: {
		title: "TOC"
	},
	legend: {
		title: "Legend"
	},
	layerSelector: {
		title: "V\u00E6lg lag"
	},
	coordfinder: {
		title: "Koordinere Finder"
	},
	agsPringing: {
		title: "AGS Printing"
	},
	addThis: {
		title: "Social Bookmarking Tool"
	},
	agolmapFinder: {
		title: "ArcGIS Online Web Map Finder"
	},
	drawSymbolChooser: {
		title: "V\u00e6lg et symbol til at placere p\u00e5 kortet ..."
	},
	redliningStyleProperties: {
		title: "Tegn Egenskaber"
	}
});