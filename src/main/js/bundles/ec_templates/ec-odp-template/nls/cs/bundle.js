define({
	europeanCommission: "Evropsk\u00e1 komise",
	lang: "cs",
	headerTitle: "Evropsk\u00FD Datov\u00FD Port\u00E1l",
	betaPageLink: "cs/what-we-do",
	legalNotice: "Pr\u00e1vn\u00ed upozorn\u011bn\u00ed",
	legalNoticeLink: "cs/legal-notice",
	cookies: "Cookies",
	cookiesLink: "cs/cookies",
	contact: "Kontakt",
	search: "Vyhled\u00e1v\u00e1n\u00ed",
	legalDisclaimer: "Vylou\u010Den\u00ED odpov\u011Bdnosti",
	routing: {
		title: "Sm\u011brov\u00e1n\u00ed"
	},
	streetview: {
		title: "Street View"
	},
	mapdesk: {
		title: "Mapa Desk"
	},
	overviewmap: {
		title: "Mapka"
	},
	mapflow: {
		title: "Mapa Flow"
	},
	followme: {
		title: "Follow Me"
	},
	magnifier: {
		title: "Zv\u011bt\u0161ovac\u00ed sklo"
	},
	gallery: {
		title: "Galerie"
	},
	resultcenter: {
		title: "V\u00fdsledek Center"
	},
	measurement: {
		title: "M\u011b\u0159en\u00ed d\u00e9lky"
	},
	bookmarks: {
		title: "Prostorov\u00e9 Z\u00e1lo\u017eky"
	},
	parameterURL: {
		title: "Link Tool"
	},
	tableofcontents: {
		title: "TOC"
	},
	legend: {
		title: "Legenda"
	},
	layerSelector: {
		title: "Vyberte vrstvy"
	},
	coordfinder: {
		title: "Sou\u0159adnic Finder"
	},
	agsPringing: {
		title: "AGS tisku"
	},
	addThis: {
		title: "Social Bookmarking Tool"
	},
	agolmapFinder: {
		title: "ArcGIS Online Web Map Finder"
	},
	drawSymbolChooser: {
		title: "Vyberte symbol uv\u00e9st na map\u011b ..."
	},
	redliningStyleProperties: {
		title: "Rem\u00edza Vlastnosti"
	}
});