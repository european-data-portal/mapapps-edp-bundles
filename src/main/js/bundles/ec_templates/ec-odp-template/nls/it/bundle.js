define({
	europeanCommission: "Commissione europea",
	lang: "it",
	legalNotice: "Avviso legale",
	cookies: "Cookies",
	contact: "Contatti",
	search: "Cerca",
	headerTitle: "Portale Europeo dei Dati",
	betaPageLink: "it/what-we-do",
	legalNoticeLink: "it/legal-notice",
	cookiesLink: "it/cookies",
	legalDisclaimer: "Avviso legale",
	routing: {
		title: "Routing"
	},
	streetview: {
		title: "Street View"
	},
	mapdesk: {
		title: "Mappa Desk"
	},
	overviewmap: {
		title: "Descrizione Mappa"
	},
	mapflow: {
		title: "Mappa di flusso"
	},
	followme: {
		title: "Seguimi"
	},
	magnifier: {
		title: "Lente d'ingrandimento"
	},
	gallery: {
		title: "Galleria"
	},
	resultcenter: {
		title: "Risultato Centro"
	},
	measurement: {
		title: "Strumenti di misurazione"
	},
	bookmarks: {
		title: "Segnalibri spaziali"
	},
	parameterURL: {
		title: "Collegamento Strumento"
	},
	tableofcontents: {
		title: "TOC"
	},
	legend: {
		title: "Leggenda"
	},
	layerSelector: {
		title: "Seleziona livelli"
	},
	coordfinder: {
		title: "Coordinare Finder"
	},
	agsPringing: {
		title: "Stampa AGS"
	},
	addThis: {
		title: "Social Bookmarking Strumento"
	},
	agolmapFinder: {
		title: "ArcGIS Online Web Map Finder"
	},
	drawSymbolChooser: {
		title: "Scegliere un simbolo per posizionare sulla mappa ..."
	},
	redliningStyleProperties: {
		title: "Disegna Properties"
	}
});