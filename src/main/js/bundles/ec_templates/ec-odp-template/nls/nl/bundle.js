define({
	europeanCommission: "Europese Commissie",
	lang: "nl",
	headerTitle: "Europees Data Portaal",
	betaPageLink: "nl/what-we-do",
	legalNotice: "Juridische mededeling",
	legalNoticeLink: "nl/legal-notice",
	cookies: "Cookies",
	cookiesLink: "nl/cookies",
	contact: "Contact",
	search: "Zoeken",
	legalDisclaimer: "Niet-aansprakelijkheidsverklaring",
	routing: {
		title: "Routing"
	},
	streetview: {
		title: "Street View"
	},
	mapdesk: {
		title: "Kaart Desk"
	},
	overviewmap: {
		title: "Overzicht Kaart"
	},
	mapflow: {
		title: "Kaart Flow"
	},
	followme: {
		title: "Volg Mij"
	},
	magnifier: {
		title: "Vergrootglas"
	},
	gallery: {
		title: "Galerij"
	},
	resultcenter: {
		title: "Resultaat Center"
	},
	measurement: {
		title: "Meetapparatuur"
	},
	bookmarks: {
		title: "Ruimtelijke Bladwijzers"
	},
	parameterURL: {
		title: "Link Tool"
	},
	tableofcontents: {
		title: "TOC"
	},
	legend: {
		title: "Legende"
	},
	layerSelector: {
		title: "Kies layer"
	},
	coordfinder: {
		title: "Co\u00f6rdineren Finder"
	},
	agsPringing: {
		title: "AGS Printing"
	},
	addThis: {
		title: "Social Bookmarking Tool"
	},
	agolmapFinder: {
		title: "ArcGIS Online Web Map Finder"
	},
	drawSymbolChooser: {
		title: "Kies een symbool op de kaart te plaatsen ..."
	},
	redliningStyleProperties: {
		title: "Trekken Properties"
	}
});