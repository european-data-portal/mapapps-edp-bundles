define({
	europeanCommission: "Europos Komisija",
	lang: "lt",
	headerTitle: "EUROPOS DUOMENŲ PORTALAS",
	betaPageLink: "lt/what-we-do",
	legalNotice: "Teisinis pranešimas",
	legalNoticeLink: "lt/legal-notice",
	cookies: "Slapukai",
	cookiesLink: "lt/cookies",
	contact: "Kontaktai",
	search: "Paie\u0161ka",
	legalDisclaimer: "Teisinis atsakomybės ribojimas",
	routing: {
		title: "Mar\u0161rutai"
	},
	streetview: {
		title: '"Street View"'
	},
	mapdesk: {
		title: "\u017dem\u0117lapis stalas"
	},
	overviewmap: {
		title: "Ap\u017evalga \u017dem\u0117lapis"
	},
	mapflow: {
		title: "\u017dem\u0117lapis srauto"
	},
	followme: {
		title: "Follow Me"
	},
	magnifier: {
		title: "Didinamasis stiklas"
	},
	gallery: {
		title: "Galerija"
	},
	resultcenter: {
		title: "Rezultatas centras"
	},
	measurement: {
		title: "Matavimo \u012frankiai"
	},
	bookmarks: {
		title: "Erdviniai \u017dym\u0117s"
	},
	parameterURL: {
		title: "Nuoroda \u012frankiai"
	},
	tableofcontents: {
		title: "BOA"
	},
	legend: {
		title: "Legenda"
	},
	layerSelector: {
		title: "Pasirinkti sluoksnius"
	},
	coordfinder: {
		title: "Koordina\u010di\u0173 ie\u0161kiklis"
	},
	agsPringing: {
		title: "AGS spauda"
	},
	addThis: {
		title: "Social Bookmarking \u012frankis"
	},
	agolmapFinder: {
		title: "ArcGIS Online interneto \u017dem\u0117lapis ie\u0161kiklis"
	},
	drawSymbolChooser: {
		title: "Pasirinkite simbol\u012f \u012fd\u0117ti \u017eem\u0117lapyje ..."
	},
	redliningStyleProperties: {
		title: "Lygiosios Ypatyb\u0117s"
	}
});