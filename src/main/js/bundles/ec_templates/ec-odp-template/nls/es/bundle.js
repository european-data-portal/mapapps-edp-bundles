define({
	europeanCommission: "Comisi\u00f3n Europea",
	lang: "es",
	legalNotice: "Aviso legal",
	cookies: "Cookies",
	contact: "Contacto",
	search: "B\u00fasqueda",
	headerTitle: "Portal Europeo de Datos",
	betaPageLink: "es/what-we-do",
	legalNoticeLink: "es/legal-notice",
	cookiesLink: "es/cookies",
	legalDisclaimer: "Exenci\u00f3n de responsabilidad",
	routing: {
		title: "Enrutamiento"
	},
	streetview: {
		title: "Vista de la calle"
	},
	mapdesk: {
		title: "Mapa Desk"
	},
	overviewmap: {
		title: "Informaci\u00f3n general Mapa"
	},
	mapflow: {
		title: "Mapa de Flujo"
	},
	followme: {
		title: "S\u00edgueme"
	},
	magnifier: {
		title: "Lupa"
	},
	gallery: {
		title: "Galer\u00eda"
	},
	resultcenter: {
		title: "Centro de Resultados"
	},
	measurement: {
		title: "Herramientas de medici\u00f3n"
	},
	bookmarks: {
		title: "Marcadores espaciales"
	},
	parameterURL: {
		title: "Enlace Herramienta"
	},
	tableofcontents: {
		title: "TOC"
	},
	legend: {
		title: "Leyenda"
	},
	layerSelector: {
		title: "Seleccionar capas"
	},
	coordfinder: {
		title: "Coordinar Buscador"
	},
	agsPringing: {
		title: "Impresi\u00f3n AGS"
	},
	addThis: {
		title: "Herramienta de marcadores sociales"
	},
	agolmapFinder: {
		title: "ArcGIS Online Mapa Web Buscador"
	},
	drawSymbolChooser: {
		title: "Elige un s\u00edmbolo para colocar en el mapa ..."
	},
	redliningStyleProperties: {
		title: "Dibuja Propiedades"
	}
});