define({
	europeanCommission : "Europ\u00e4ische Kommission",
	lang : "de",
	headerTitle : "Europ\u00e4isches Datenportal",
	betaPageLink : "de/what-we-do",
	legalNotice : "Rechtlicher Hinweis",
	legalNoticeLink : "de/legal-notice",
	cookies : "Cookies",
	cookiesLink : "de/cookies",
	contact : "Kontakt",
	search : "Suche",
	legalDisclaimer : "Rechtlicher Hinweis",
	routing : {
		title : "Routing"
	},
	streetview : {
		title : "Street View"
	},
	mapdesk : {
		title : "Map Desk"
	},
	overviewmap : {
		title : "\u00dcbersichtskarte"
	},
	mapflow : {
		title : "Map Flow"
	},
	followme : {
		title : "Follow Me"
	},
	magnifier : {
		title : "Lupe"
	},
	gallery : {
		title : "Galerie"
	},
	resultcenter : {
		title : "Ergebniscenter"
	},
	measurement : {
		title : "Messwerkzeuge"
	},
	bookmarks : {
		title : "R\u00e4umliche Lesezeichen"
	},
	parameterURL : {
		title : "Link Werkzeug"
	},
	tableofcontents : {
		title : "TOC"
	},
	legend : {
		title : "Legende"
	},
	layerSelector : {
		title : "Layer ausw\u00E4hlen"
	},
	coordfinder : {
		title : "Koordinatenfinder"
	},
	agsPringing : {},
	addThis : {
		title : "Social Bookmarking Werkzeug"
	},
	agolmapFinder : {
		title : "ArcGIS Online Web Map Suche"
	},
	drawSymbolChooser : {
		title : "Ein Symbol zum Platzieren auf der Karte w\u00e4hlen..."
	},
	redliningStyleProperties : {
		title : "Zeichen-Eigenschaften"
	}
});