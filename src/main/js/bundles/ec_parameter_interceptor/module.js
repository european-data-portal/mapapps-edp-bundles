define([
  "./util",
  "./Deferred",
  "./ckan/ParameterInterceptor",
  "./ckan/Resource",
  "./ckan/ResourceFetcher",
  "./ckan/WMSResourceAnalyzer",
  "./ckan/GeoJSONResourceAnalyzer",
  "./ckan/ExtentChanger",
  "./layer/DynamicLayerFactory",
  "./layer/DynamicWMSLayerFactory",
  "./layer/DynamicGeoJSONLayerFactory",
  "./layer/CKANResourceHandler",
  "./layer/WMSLayer",
  "./layer/WMSServiceRegistration"
], {});
