define([
  "dojo/_base/declare",
  "ct/request",
  "../layer/WMSLayer",
  "../util"
], function(declare, request, WMSLayer, util) {
  function createGetCapabilitiesURL(url) {
    var param = util.decodeQuery(url);
    var lc, serviceFound, requestFound;
    for (var key in param) {
      if (param.hasOwnProperty(key)) {
        lc = key.toLowerCase();
        if (lc === "service") {
          param[key] = "WMS";
          serviceFound = true;
        } else if (lc === "request") {
          param[key] = "GetCapabilities";
          requestFound = true;
        }
      }
    }
    if (!serviceFound) {
      param.service = "WMS";
    }
    if (!requestFound) {
      param.request = "GetCapabilities";
    }
    return util.encodeQuery(url, param);
  }

  return declare([], {
    type: "WMS",
    activate: function() {
      this.i18n = this._i18n.get();
    },
    getExtent: function(resource) {
      var url = createGetCapabilitiesURL(resource.getURL());
      return this.createService(url).then(function(service) {
        // save the service/extent for later usage
        resource.setPayload(service);
        resource.setPayloadTitle(service.title);
        resource.setExtent(service.extent);
        return resource.getExtent();
      });
    },
    createService: function(url) {
      var _this = this;
      var service = new WMSLayer(url);
      var req = request({
        url: url,
        handleAs: "xml"
      });
      return req.then(function(xml) {
        try {
          service._parseCapabilities(xml);
        } catch (err) {
          // these may be really ugly looking errors...
          //console.error(err);
          throw new Error(_this.i18n.errors.unableToParseCapabilities);
        }
        return service;
      });
    }
  });
});
