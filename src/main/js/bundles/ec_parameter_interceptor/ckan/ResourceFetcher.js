define([
  "dojo/_base/declare",
  "ct/request",
  "../Deferred",
  "./Resource"
], function(declare, request, Deferred, Resource) {
  return declare([], {
    _timeout: null,
    _url: null,
    /**
     * This store provides an interface for the location finder service.
     * @constructs
     * @param options options
     */
    constructor: function(options) {
      options = options || {};
      this._url = options.url;
      this._timeout = options.timeout || 5000;
    },

    getTimeout: function() {
      return this._timeout;
    },

    getUrl: function() {
      return this._url;
    },

    fetch: function(dataset) {
      return this._getCKANResource(dataset).then(function(json) {
        return new Resource(dataset.id, dataset.type, json);
      });
    },
    _getCKANResource: function(dataset) {
      if (dataset.id) {
        return this._requestCKANResource(dataset);
      } else {
        return this._createCKANResource(dataset);
      }
    },
    _requestCKANResource: function(dataset) {
      var url = this._url.replace(":id", encodeURIComponent(dataset.id));
      return request({
        url: url,
        handleAs: "json",
        timeout: this._timeout
      }).then(function(body) {
        if (!body || !body['@graph'] || body['@graph'].length === 0) {
          throw new Error("empty CKAN response");
        }

        // currently not present in the resources of the new API
        // if (!body.success) {
        //   throw new Error(body.error.message);
        // }

        // new API response, identify the "distribution" @type (JSON-LD)
        var dist;
        body['@graph'].forEach(function(g) {
          if (g['@type'] === 'http://www.w3.org/ns/dcat#Distribution' && g['http://www.w3.org/ns/dcat#accessURL'] && g['http://www.w3.org/ns/dcat#accessURL']['@id']) {
                dist = g;
              }
        });

        // transform to internal data model
        return {
          id: dataset.id,
          url: dist['http://www.w3.org/ns/dcat#accessURL']['@id'],
          format: dist['http://purl.org/dc/terms/format'],
          description: dist['http://purl.org/dc/terms/title'],
        };
      });
    },
    /**
     * Creates a dummy CKAN JSON resource for the supplied parameters.
     * @param {Object} dataset the query parameters
     * @returns {Object} the CKAN resource
     */
    _createCKANResource: function(dataset) {
      return Deferred.resolve({
        title: dataset.title,
        description: dataset.description,
        url: dataset.url,
        format: dataset.format
      });
    }
  });
});
