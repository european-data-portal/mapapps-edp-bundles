define(["dojo/_base/declare"], function(declare) {
  return declare([], {
    _restrictions: {
      4326: {
        xmax: 90.0,
        xmin: -90.0,
        ymax: 180.0,
        ymin: -180.0
      },
      3035: {
        xmax: 6829874.45,
        xmin: 1507846.05,
        ymax: 4662111.45,
        ymin: 1896628.62
      },
      3857: {
        xmax: 20048966.1,
        xmin: -20048966.1,
        ymax: 20026376.39,
        ymin: -20026376.39
      }
    },
    _bboxDefaultDegree: 0.005,
    _json: null,
    type: null,
    id: null,
    extent: null,
    _loadURL: null,
    _extentStretchFactor: 0.1,
    _payload: null,
    _payloadTitle: null,
    constructor: function(id, type, json) {
      this.id = id;
      this.type = type;
      this._json = json;
    },
    getURL: function() {
      return this._json.url;
    },
    getId: function() {
      return this.id;
    },
    getType: function() {
      return this.type;
    },
    getExtent: function() {
      return this.extent;
    },
    getTitle: function(lang) {
      if (lang) {
        if (
          this._json.translation &&
          this._json.translation[lang] &&
          this._json.translation[lang].name
        ) {
          return this._json.translation[lang].name;
        }
      }
      return this._json.title || this._json.name;
    },
    getDescription: function() {
      return this._json.description;
    },
    getPayload: function() {
      return this._payload;
    },
    setPayload: function(payload) {
      this._payload = payload;
    },
    getPayloadTitle: function() {
      return this._payloadTitle;
    },
    setPayloadTitle: function(pt) {
      this._payloadTitle = pt;
    },
    _correctExtent: function(extent) {
      // fix for broken extents
      if (extent.xmax === extent.xmin) {
        extent.xmax = extent.xmax + this._bboxDefaultDegree;
        extent.xmin = extent.xmin - this._bboxDefaultDegree;
      }
      if (extent.ymax === extent.ymin) {
        extent.ymax = extent.ymax + this._bboxDefaultDegree;
        extent.ymin = extent.ymin - this._bboxDefaultDegree;
      }
      return extent;
    },
    _widenExtent: function(extent) {
      var width = extent.xmax - extent.xmin;
      var height = extent.ymax - extent.ymin;
      var factor = this._extentStretchFactor;
      extent.ymin -= factor * height;
      extent.ymax += factor * height;
      extent.xmin -= factor * width;
      extent.xmax += factor * width;
      return extent;
    },
    _reduceToMaxValues: function(ex) {
      if (
        ex.spatialReference &&
        ex.spatialReference.wkid &&
        this._restrictions[ex.spatialReference.wkid]
      ) {
        var restriction = this._restrictions[ex.spatialReference.wkid];
        if (ex.xmax > restriction.xmax) {
          ex.xmax = restriction.xmax;
        }
        if (ex.ymax > restriction.ymax) {
          ex.ymax = restriction.ymax;
        }
        if (ex.xmin < restriction.xmin) {
          ex.xmin = restriction.xmin;
        }
        if (ex.ymin < restriction.ymin) {
          ex.ymin = restriction.ymin;
        }
      }
      return ex;
    },
    setExtent: function(ex) {
      ex = this._correctExtent(ex);
      ex = this._widenExtent(ex);
      ex = this._reduceToMaxValues(ex);
      this.extent = ex;
    }
  });
});
