define(['dojo/_base/config', 'dojo/_base/lang'], function (config, lang) {
    var name, names = ["assert", "count", "debug", "dir", "error", "_exception",
        "group", "groupCollapsed", "groupEnd", "info", "log", "profile",
        "profileEnd", "table", "time", "timeEnd", "trace", "warn"];
    var global = this, console = global.console || {}, noop = function () {};
    var con = {isDebug: config.isDebug};
    while (name = names.pop())
        con[name] = console[name] ? lang.hitch(console, console[name]) : noop;
    return con;
});