define([
  "dojo/_base/declare",
  "dijit/_Widget",
  "dijit/_TemplatedMixin",
  "dijit/_WidgetsInTemplateMixin",
  "./Utils",
  "dojo/query",
  "dojo/text!./templates/disclaimer.html",
  "dijit/layout/BorderContainer",
  "dijit/layout/ContentPane",
  "dijit/form/Button"
], function(
  declare,
  _Widget,
  _TemplatedMixin,
  _WidgetsInTemplateMixin,
  Utils,
  query,
  templateStringContent,
  BorderContainer,
  ContentPane,
  Button
) {
  return declare([_Widget, _TemplatedMixin, _WidgetsInTemplateMixin], {
    baseClass: "ecDisclaimerWrapper",
    templateString: templateStringContent,
    constructor: function(args) {
      this.i18n = args.i18n;
      this.overlayUtils = args.overlayUtils;
    },
    buildRendering: function() {
      this.inherited(arguments);
    },
    _closeDisclaimer: function() {
      if (!Utils.isTutorialDisabled()) {
        console.info("open tutorial");
      }
      var checkbox = query("#showIntroAgain");
      if (checkbox && checkbox.length === 1) {
        if (checkbox[0].checked) {
          setTimeout(Utils.disableTutorial, 1000);
        } else {
          setTimeout(Utils.enableTutorial, 1000);
        }
      }
      Utils.hideInfoDisclaimer();
      //we disable this, at its very buggy
      //this.overlayUtils.removeGlobalOverlay();
    }
  });
});
