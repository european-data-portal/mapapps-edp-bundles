define([
  "dojo/_base/declare",
  "dojo/_base/lang",
  "./Utils",
  "./intro",
  "dojo/query"
], function(declare, d_lang, Utils, introjs, query) {
  return declare([], {
    overlayUtils: null,
    _eventService: null,
    _activated: false,
    activate: function() {
      this.enableDisclaimerButtons();
      this.activateDisclaimerStartButton();
      this._eventService.sendEvent("ec/odp/template/AVAILABLE");

      //enable the tutorial button in the bottom menu bar
      var tutorialButton = query(".tutorial-button");
      if (tutorialButton.length > 0) {
        tutorialButton.connect(
          "onclick",
          d_lang.hitch(this, function() {
            Utils.hideInfoDisclaimer();
            this.overlayUtils.addGlobalOverlay();
            this.startTutorial();
          })
        );
      }

      //UGLY: lets request the initial extent! the disclaimer
      //requires the info in order to activate its buttons
      //we call this as we want to make sure that the button
      //is there, so we can use it (see activateDisclaimerStartButton)
      this._eventService.sendEvent("ec/odp/extent/REQUESTED");

      this._activated = true;
      this._disclaimer.onTutorialAvailable();

      if (this._disclaimer.removeAfterStartup()) {
      }
    },
    isActivated: function() {
      return this._activated;
    },
    activateDisclaimerStartButton: function() {
      //activate the (i) button in the bottom bar
      var disclaimerButton = query(".disclaimer-button");
      if (disclaimerButton.length > 0) {
        disclaimerButton.connect(
          "onclick",
          d_lang.hitch(this, function() {
            Utils.showInfoDisclaimer(true);
            //we disable this, at its very buggy
            //                                    this.overlayUtils.addGlobalOverlay();
          })
        );
      }

      //activate the ok button in the disclaimer widget
      var okButton = query("div.disclaimer-footer .ok-button");
      if (okButton.length > 0) {
        okButton.connect(
          "onclick",
          d_lang.hitch(this, function() {
            if (!Utils.isTutorialDisabled()) {
              this.startTutorial();
              this.overlayUtils.addGlobalOverlay();
            }
          })
        );
      }
    },
    enableDisclaimerButtons: function() {
      //we disable this, at its very buggy
      //                            this.overlayUtils.addGlobalOverlay();
      Utils.displayDisclaimerButtons();
    },
    startTutorial: function() {
      var tutorial = this._i18n.get().tutorial;
      var intro = introjs();
      intro.setOptions({
        showStepNumbers: false,
        exitOnOverlayClick: false,
        nextLabel: tutorial.next + " &rarr;",
        prevLabel: "&larr; " + tutorial.back,
        skipLabel: tutorial.skip,
        doneLabel: tutorial.done,
        steps: [
          {
            intro: tutorial.familiarise
          },
          {
            intro: tutorial.navigation
          },
          {
            element: document.querySelector(".esriSimpleSlider"), // auf die zoom buttons
            intro: tutorial.zoom,
            position: "left"
          },
          {
            element: document.querySelector(".dockingBarBottomRight"), // auf die buttons unten
            intro: tutorial.features,
            position: "top"
          },
          {
            element: document.querySelector(".ctTool_legendToggleTool"), // auf legend button
            intro: tutorial.legend,
            position: "top"
          },
          {
            element: document.querySelector(".ctTool_ec_feature_infoTool"),
            intro: tutorial.featureinfo,
            //intro: '<input type="checkbox" id="showIntroAgain"/><label for="checkBox">' + tutorial.donotshow + '</label>'
            position: "top"
          }
        ]
      });
      intro.start();
      //                                intro.oncomplete(d_lang.hitch(this, function () {
      //                                    var checkbox = query("#showIntroAgain");
      //                                    if (checkbox && checkbox.length === 1 && checkbox[0].checked) {
      //                                        Utils.disableTutorial();
      //                                    }
      //                                }));
      intro.oncomplete(
        d_lang.hitch(this, function() {
          Utils.disableTutorial();
          this.overlayUtils.removeGlobalOverlay();
        })
      );
      intro.onexit(
        d_lang.hitch(this, function() {
          Utils.disableTutorial();
          this.overlayUtils.removeGlobalOverlay();
        })
      );
    }
  });
});
