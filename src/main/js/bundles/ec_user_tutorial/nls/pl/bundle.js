define({
  "ec_user_tutorial": {
    "bundleName": "ec-użytkownik-poradnik",
    "bundleDescription": "ec-użytkownik-poradnik",
    "externalResourceDisclaimer": {
      "introduction": "Ta mapa widz uzyskuje dostęp do usług zewnętrznych i wyświetla ich dane. Te usługi zewnętrzne nie są prowadzone przez Komisję Europejską, a więc nie mamy żadnego wpływu na ich dostępność / stabilności. Mogą wystąpić następujące problemy:",
      "rotatingCircle": "Wirujące koło w widoku mapy wskazuje, że widz czeka na odpowiedź ze źródła danych. Może to być spowodowane przez niedostępnych zagadnień serwerowych i sieciowych",
      "layerZoomLevel": "Niektóre źródła warstwy mogą nie być wyświetlane na niektórych poziomach zoomu. Wynika to z ograniczeń ustalonych przez usługodawcę zewnętrznego. Może trzeba będzie powiększyć lub pomniejszyć, aby wyświetlić dane z warstwy",
      "layerPanning": "Dane dostępne z serwera zewnętrznego mogą znajdować się poza bieżącym widoku mapy. Ta mapa widz próbuje automatycznie zidentyfikować zadaszone. Mimo to, w niektórych przypadkach metadane nie są kompletne lub błędne, tak, że trzeba przesunąć do rzeczywistego stopnia.",
      "detailInformation": "Jeśli poprosisz szczegółowe informacje o wybranej funkcji będzie to być obsługiwane przez specjalne żądanie do serwera mapy. Jeśli odpowiedź na ten wniosek jest uszkodzony lub pusty (np z powodu błędu serwera) nie mogą być wyświetlane informacje i puste okno pojawi się.",
      "technicalSupport": "Jeśli napotkasz jakieś inne problemy, nie wahaj się skontaktować się z pomocą techniczną:"
    },
    "tutorial": {
      "welcome": "Zapraszamy do widza mapy European Data Portal",
      "familiarise": "To małe wprowadzenie ma na celu zapoznanie Cię z elementami i funkcjonalności przeglądarki mapy.",
      "navigation": "Kliknij i przytrzymaj lewy przycisk myszy, aby przesuwać na mapie.",
      "zoom": "Te przyciski zmienić poziom zoomu na mapie. Alternatywnie można użyć kółka myszy do regulacji zoomu.",
      "features": "Dodatkowa funkcjonalność jest dostępna za pośrednictwem tych przycisków. Działają one jak przełączniki i włączyć lub wyłączyć daną funkcję.",
      "legend": "Legenda mogą być wykorzystane do zbadania dostępnych warstw map i włączyć lub wyłączyć ich aktualny wyświetlane na mapie. Nazwy pochodzą bezpośrednio z zewnątrz dostęp serwisu.",
      "transparency": "Możesz również ustawić przezroczystość warstwy.",
      "featureinfo": "Niektóre dane mogą być badane jeszcze bardziej szczegółowo. Gdy funkcja ta jest aktywna, możesz kliknąć na mapie, aby zapytać o dodatkowe informacje.",
      "donotshow": "Nie należy ponownie wyświetlić ekran powitalny.",
      "okButton": "Ok",
      "closeButton": "Blisko"
    }
  }
});