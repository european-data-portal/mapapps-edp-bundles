define({
  "ec_user_tutorial": {
    "bundleName": "ec-lietotājs-apmācība",
    "bundleDescription": "ec-lietotājs-apmācība",
    "externalResourceDisclaimer": {
      "introduction": "Šī karte skatītājs piekļūst dienestus un parāda savus datus. Šie ārējie pakalpojumi netiek uztur Eiropas Komisija, un tādēļ mums nav nekādas ietekmes uz to pieejamību / stabilitāti. Šādas problēmas var rasties:",
      "rotatingCircle": "Rotācijas apļa kartes skatā norāda, ka skatītājs gaida atbildi no datu avotu. Tas var izraisīt nepieejama servera vai tīkla jautājumos",
      "layerZoomLevel": "Daži slānis avoti var neparādīt atsevišķos pietuvināšanas līmeņi. Tas izriet no noteiktajiem ārpakalpojuma sniedzēja ierobežojumiem. Jums var būt nepieciešams, lai tuvinātu vai tālinātu, lai parādītu datus slāņa",
      "layerPanning": "Datu piekļūts no ārēja servera var atrasties ārpus pašreizējā kartes skatā. Šī karte skatītājs mēģina automātiski noteikt, uz apgabalu. Tomēr dažos gadījumos metadati nav pilnīga vai kļūdaina tā, ka jums ir, lai panna ar faktisko apjomā.",
      "detailInformation": "Ja jūs lūgt detalizētu informāciju par izvēlēto funkciju tas tiks apstrādāti ar īpašu lūgumu uz kartes serveri. Ja atbilde uz šo pieprasījumu, ir nepareizi vai tukšas (piemēram, sakarā ar servera kļūdas) nekāda informācija var apskatīt un tukšā logā parādīsies.",
      "technicalSupport": "Ja jums rodas kādi citas problēmas, nevilcinieties sazināties ar tehnisko atbalstu:"
    },
    "tutorial": {
      "welcome": "Laipni lūdzam kartē skatītājam European Data Portal",
      "familiarise": "Šis nelielais ievads ir mērķis iepazīstināt jūs ar elementiem un funkcionalitāti kartē skatītāju.",
      "navigation": "Noklikšķiniet un turiet peles kreiso pogu, lai pārvietotos kartē.",
      "zoom": "Šīs pogas mainīt tālummaiņas līmeni kartē. Alternatīvi jūs varat izmantot peles riteni, lai noregulētu tālummaiņu.",
      "features": "Papildus funkcionalitāte ir pieejama caur šīm pogām. Tās darbojas kā pārslēgšana un iespējot vai atspējot doto funkcionalitāti.",
      "legend": "Leģenda var izmantot, lai pārbaudītu pieejamos kartes slāņi un iespējot vai atspējot savu pašreizējo attēlot kartē. Šie vārdi ir tieši atvasināti no ārpuses piekļūt pakalpojumu.",
      "transparency": "Varat arī pielāgot to slāni pārskatāmību.",
      "featureinfo": "Daži dati var tikt izskatīts pat sīkāk. Kad šī funkcija ir aktivizēta, jūs varat noklikšķināt uz kartes, lai vaicājumu papildu informāciju.",
      "donotshow": "Nerādīt šo sveiciena ekrānu vēlreiz.",
      "okButton": "Labi",
      "closeButton": "Aizvērt"
    }
  }
});