define({
  "ec_user_tutorial": {
    "bundleName": "ec-usuario-tutorial",
    "bundleDescription": "ec-usuario-tutorial",
    "externalResourceDisclaimer": {
      "introduction": "Este visor de mapas accede a los servicios externos y muestra sus datos. Estos servicios externos no son mantenidos por la Comisión Europea y por lo tanto no tenemos ninguna influencia sobre su disponibilidad / estabilidad. Pueden aparecer los siguientes temas:",
      "rotatingCircle": "Un círculo que gira en la vista del mapa indica que el espectador está esperando una respuesta de una fuente de datos. Esto puede ser causado por un problemas de servidor o de red no disponibles",
      "layerZoomLevel": "Algunas fuentes de la capa no se muestren en determinados niveles de zoom. Esto es consecuencia de las restricciones establecidas por el proveedor de servicios externo. Puede que tenga que acercar o alejar con el fin de mostrar los datos de la capa",
      "layerPanning": "Los datos que se accede desde un servidor externo pueden estar situados fuera de la vista del mapa actual. Este visor de mapas intenta identificar automáticamente el área cubierta. Sin embargo, en algunos casos, los metadatos no están completos o erróneos para que tenga a la sartén en la medida real.",
      "detailInformation": "Si usted solicita información detallada para una característica seleccionada esta será manejado por una solicitud específica para el servidor de mapas. Si la respuesta a esta solicitud está mal formado o vacía (por ejemplo, debido a un error en el servidor) no hay información se puede mostrar y una ventana vacía aparecerá.",
      "technicalSupport": "Si tiene algún otro problema, no dude en ponerse en contacto con el soporte técnico:"
    },
    "tutorial": {
      "welcome": "Bienvenido al visualizador de mapas del European Data Portal",
      "familiarise": "Esta pequeña introducción tiene el objetivo de que se familiarice con los elementos y funcionalidades del visor de mapas.",
      "navigation": "Haga clic y mantenga pulsado el botón izquierdo del ratón para desplazarse en el mapa.",
      "zoom": "Estos botones cambian el nivel de zoom en el mapa. Alternativamente, puede utilizar la rueda del ratón para ajustar el zoom.",
      "features": "Funcionalidad adicional está disponible a través de estos botones. Ellos actúan como palancas y activar o desactivar la funcionalidad determinada.",
      "legend": "La leyenda se puede utilizar para examinar las capas de mapa y activar o desactivar su visualización actual en el mapa. Los nombres se derivan directamente del servicio de acceso externamente.",
      "transparency": "También puede ajustar la transparencia de una capa.",
      "featureinfo": "Algunos de los datos pueden ser examinados incluso en más detalle. Cuando se activa esta función puede hacer clic en el mapa para consultar información adicional.",
      "donotshow": "No mostrar esta pantalla de bienvenida de nuevo.",
      "okButton": "Ok",
      "closeButton": "Cerca"
    }
  }
});