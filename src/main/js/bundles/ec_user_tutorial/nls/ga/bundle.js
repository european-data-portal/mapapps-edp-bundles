define({
  "ec_user_tutorial": {
    "bundleName": "ec-faoi úsáideoir-teagaisc",
    "bundleDescription": "ec-faoi úsáideoir-teagaisc",
    "externalResourceDisclaimer": {
      "introduction": "rochtain seo breathnóir léarscáil sheirbhísí seachtracha agus taispeántais a gcuid sonraí na seirbhísí seachtracha nach bhfuil á gcothabháil ag an gCoimisiún Eorpach agus dá bhrí sin ní mór dúinn aon tionchar ar a n-infhaighteacht / cobhsaíocht Féadfaidh na saincheisteanna seo a leanas ann:",
      "rotatingCircle": "Léiríonn ciorcal rothlach i dtuairim léarscáil go bhfuil an lucht féachana ag fanacht le freagra ó fhoinse na sonraí D'fhéadfadh sé seo a ba chúis leis an freastalaí nach bhfuil ar fáil nó fadhbanna líonra",
      "layerZoomLevel": "Ní fhéadfadh roinnt foinsí ciseal taispeáint ag leibhéil áirithe súmáil. Mar thoradh air seo ó shrianta atá leagtha síos ag an soláthraí seirbhíse seachtrach. Seans go mbeidh ort súmáil isteach nó amach chun na sonraí a an ciseal thaispeáint",
      "layerPanning": "Is féidir sonraí a rochtain ó fhreastalaí seachtrach a bheith suite taobh amuigh den tuairim léarscáil atá ann faoi láthair déanann an breathnóir léarscáil a aithint go huathoibríoch ar an limistéar atá clúdaithe Fós, i gcásanna áirithe nach bhfuil na meiteashonraí iomlán nó earráideach ionas go bhfuil tú chun peanáil a mhéid iarbhír.",
      "detailInformation": "Má iarrann tú eolas mionsonraithe haghaidh gné roghnaithe beidh sé seo a láimhseáil trí iarratas sonrach leis an bhfreastalaí léarscáil. Má tá an fhreagairt ar an iarraidh seo míchumtha nó folamh (eg mar gheall ar earráid freastalaí) Is féidir aon fhaisnéis a chur ar taispeáint agus beidh fuinneog folamh thaispeáint suas.",
      "technicalSupport": "Má bhíonn tú nach bhfuil aon fadhbanna eile leisce ort a fháil i dteagmháil leis an tacaíocht theicniúil:"
    },
    "tutorial": {
      "welcome": "Fáilte go dtí an lucht féachana léarscáil an European Data Portal",
      "familiarise": "Tá an aidhm chun eolas tú leis na gnéithe agus feidhmiúlacht an lucht féachana léarscáil seo a thabhairt isteach beag.",
      "navigation": "Cliceáil agus coinnigh an cnaipe luiche ar chlé chun peanáil ar an léarscáil.",
      "zoom": "Athrú ar na cnaipí ar an leibhéal zúmála ar an léarscáil. Nó is féidir leat a bhaint as an roth luch a choigeartú an súmáil.",
      "features": "Tá feidhmiúlacht bhreise ar fáil trí na cnaipí. Feidhmíonn siad mar toggles agus cur ar chumas nó a dhíchumasú an fheidhmiúlacht ar leith.",
      "legend": "Is féidir leis an finscéal a úsáid chun scrúdú a dhéanamh ar na sraitheanna léarscáil atá ar fáil agus ar chumas nó a dhíchumasú ar a thaispeáint faoi láthair ar an léarscáil. Tá ainmneacha a dhíorthaítear go díreach ó na tseirbhís rochtain seachtrach.",
      "transparency": "Is féidir leat a choigeartú freisin ar an trédhearcacht ciseal.",
      "featureinfo": "Is féidir le cuid de na sonraí a scrúdú fiú níos mine. Nuair a bhíonn an ghné seo i ngníomh is féidir leat cliceáil ar an léarscáil chun ceist eolas breise.",
      "donotshow": "Ná taispeáin an scáileán fáilte roimh arís.",
      "okButton": "Ceart go leor",
      "closeButton": "Dún"
    }
  }
});