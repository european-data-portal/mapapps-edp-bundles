define({
  "ec_user_tutorial": {
    "bundleName": "ec-användaren-handledning",
    "bundleDescription": "ec-användaren-handledning",
    "externalResourceDisclaimer": {
      "introduction": "Denna karta betraktare åtkomst externa tjänster och visar sina data. Dessa externa tjänster är inte upprätthålls av Europeiska kommissionen och därför har vi inget inflytande på tillgången / stabilitet. Följande problem kan uppstå:",
      "rotatingCircle": "En roterande cirkel i kartvyn indikerar att tittaren väntar på ett svar från en datakälla. Detta kan orsakas av en otillgänglig server eller nätverksproblem",
      "layerZoomLevel": "Vissa skiktkällor kanske inte visas vid vissa zoomnivåer. Detta resulterar från begränsningarna av den externa tjänsteleverantören. Du kan behöva zooma in eller ut för att visa data i skiktet",
      "layerPanning": "Data nås från en extern server kan vara placerad utanför den aktuella kartvyn. Denna karta betraktaren försöker att automatiskt identifiera den täckta området. Fortfarande i vissa fall metadata är inte kompletta eller felaktiga så att du har att panorera till den faktiska omfattning.",
      "detailInformation": "Om du begär detaljerad information för en vald funktion kommer att hanteras av en särskild begäran till kartservem. Om svaret på denna begäran är missbildad eller tom (t.ex. på grund av ett serverfel) ingen information kan visas och ett tomt fönster kommer att dyka upp.",
      "technicalSupport": "Om du stöter på några andra problem tveka inte att ta kontakt med teknisk support:"
    },
    "tutorial": {
      "welcome": "Välkommen till kartan betraktaren av European Data Portal",
      "familiarise": "Denna lilla introduktion har målet att bekanta dig med elementen och funktionalitet på kartan betraktaren.",
      "navigation": "Klicka och håll nere vänster musknapp för att panorera på kartan.",
      "zoom": "Dessa knappar ändrar zoomnivån på kartan. Alternativt kan du använda mushjulet för att justera zoom.",
      "features": "Ytterligare funktioner är tillgängliga via dessa knappar. De fungerar som växlar och aktivera eller inaktivera den angivna funktionen.",
      "legend": "Legenden kan användas för att undersöka de tillgängliga kartlager och aktivera eller inaktivera sin nuvarande visning på kartan. Namnen är direkt härrör från den externt nås tjänsten.",
      "transparency": "Du kan också justera insyn i ett lager.",
      "featureinfo": "Vissa data kan undersökas även mer i detalj. När den här funktionen är aktiverad kan du klicka på kartan för att söka ytterligare information.",
      "donotshow": "Visa inte det här välkomstskärmen igen.",
      "okButton": "Ok",
      "closeButton": "Stäng"
    }
  }
});