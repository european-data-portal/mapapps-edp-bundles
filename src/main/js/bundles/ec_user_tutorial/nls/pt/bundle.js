define({
  "ec_user_tutorial": {
    "bundleName": "ec-usuário tutorial-",
    "bundleDescription": "ec-usuário tutorial-",
    "externalResourceDisclaimer": {
      "introduction": "O mapa telespectador acessa serviços externos e exibe seus dados. Estes serviços externos não são mantidas pela Comissão Europeia e, portanto, não temos qualquer influência sobre a sua disponibilidade / estabilidade. Os seguintes problemas podem ocorrer:",
      "rotatingCircle": "Um círculo de rotação na visualização do mapa indica que o espectador está aguardando por uma resposta de uma fonte de dados Isso pode ser causado por um servidor indisponível ou problemas de rede",
      "layerZoomLevel": "Algumas fontes camada pode não exibir em determinados níveis de zoom. Isto resulta de restrições definidas pelo prestador de serviços externo. Você pode precisar de mais ou menos zoom para exibir os dados da camada",
      "layerPanning": "Dados acessados ​​a partir de um servidor externo pode estar localizado fora da visualização do mapa atual. O mapa espectador tenta identificar automaticamente a área coberta. Ainda assim, em alguns casos, os metadados não estão completos ou errónea, de modo que você tem para se deslocar para a extensão real.",
      "detailInformation": "Se você solicitar informações detalhadas de um recurso selecionado, isso será tratado por um pedido específico ao servidor de mapas. Se a resposta a este pedido é mal formado ou vazio (por exemplo, devido a um erro no servidor) nenhuma informação pode ser exibida e uma janela vazia vai aparecer.",
      "technicalSupport": "Se você encontrar quaisquer outros problemas, não hesite em entrar em contato com o suporte técnico:"
    },
    "tutorial": {
      "welcome": "Bem-vindo ao visualizador de mapas do European Data Portal",
      "familiarise": "Esta pequena introdução tem o objetivo de familiarizá-lo com os elementos e funcionalidades do visualizador de mapas.",
      "navigation": "Clique e segure o botão esquerdo do mouse para se deslocar no mapa.",
      "zoom": "Estes botões mudar o nível de zoom no mapa. Alternativamente, você pode usar a roda do mouse para ajustar o zoom.",
      "features": "A funcionalidade adicional está disponível através destes botões. Eles agem como alterna e habilitar ou desabilitar a funcionalidade dada.",
      "legend": "A legenda pode ser usado para examinar as camadas de mapas disponíveis e ativar ou desativar sua exibição atual no mapa. Os nomes são diretamente derivadas do serviço acessado externamente.",
      "transparency": "Você também pode ajustar a transparência de uma camada.",
      "featureinfo": "Alguns dados podem ser analisados ​​com mais detalhe mesmo. Quando este recurso é ativado, você pode clicar no mapa para consultar informações adicionais.",
      "donotshow": "Não exibir esta tela de boas-vindas novamente.",
      "okButton": "Está bem",
      "closeButton": "Fechar"
    }
  }
});