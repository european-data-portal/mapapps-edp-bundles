define({
  "ec_user_tutorial": {
    "bundleName": "ec-käyttäjän opetusohjelma",
    "bundleDescription": "ec-käyttäjän opetusohjelma",
    "externalResourceDisclaimer": {
      "introduction": "Tämä kartta katsoja pääsee ulkopuoliset palvelut ja näyttää niiden tiedot. Nämä ulkoiset palvelut eivät ylläpitää Euroopan komissio, ja siksi meillä ei ole mitään vaikutusta niiden saatavuus / vakautta. Seuraavia ongelmia voi ilmetä:",
      "rotatingCircle": "Pyörivä ympyrä karttanäkymässä osoittaa, että katsoja odottaa vastausta tietolähteestä. Tämä voi johtua käytettävissä palvelimen tai verkko-ongelmat",
      "layerZoomLevel": "Jotkut kerros lähteitä ei ehkä näy tietyillä zoomaustasoa. Tämä johtuu asettamat rajoitukset ulkoinen palveluntarjoaja. Saatat joutua lähentää tai loitontaa, jotta voit nähdä tiedot kerroksen",
      "layerPanning": "Tiedot pääsee ulkoinen palvelin voi sijaita ulkopuolella nykyisen karttanäkymän. Tämä kartta katsoja yrittää automaattisesti tunnistaa peittämä alue. Silti joissakin tapauksissa metatiedot eivät ole täydellisiä tai virheellinen niin että sinulla on panoroida todellista laajuutta.",
      "detailInformation": "Jos pyydät yksityiskohtaiset tiedot valitun ominaisuus tämä tulee hoitaa erityisen pyynnön kartan palvelimelle. Jos vastaus tähän pyyntöön on epämuodostunut tai tyhjä (esim vuoksi palvelinvirhe) ei tietoa voidaan näyttää ja tyhjä ikkuna näkyy.",
      "technicalSupport": "Jos kohtaat muita ongelmia, älä epäröi ottaa yhteyttä teknistä tukea:"
    },
    "tutorial": {
      "welcome": "Tervetuloa kartta katsoja European Data Portal",
      "familiarise": "Tämä pieni esittely on tarkoitus tutustuttaa elementtejä ja toiminnallisuutta kartta katsoja.",
      "navigation": "Napsauta ja pidä hiiren vasenta painiketta panoroida kartalla.",
      "zoom": "Nämä painikkeet muuttaa zoomaustasoa kartalla. Vaihtoehtoisesti voit käyttää hiiren rullaa säätää zoom.",
      "features": "Lisätoimintoja on saatavissa näitä painikkeita. Ne toimivat vaihtaa ja käyttöön tai poistaa tietyn toiminnallisuuden.",
      "legend": "Legenda voidaan tutkia käytettävissä kartta kerrokset ja käyttöön tai poistaa niiden nykyinen näyttö kartalla. Nimet ovat suoraan peräisin ulkoisesti näytetty palvelu.",
      "transparency": "Voit myös säätää läpinäkyvyyttä kerroksen.",
      "featureinfo": "Joitakin tietoja voidaan tarkastella vielä yksityiskohtaisemmin. Kun tämä toiminto on käytössä voit klikata karttaa kyselyn lisätietoja.",
      "donotshow": "Älä näytä tätä tervetullut näytön uudelleen.",
      "okButton": "Ok",
      "closeButton": "Lähellä"
    }
  }
});