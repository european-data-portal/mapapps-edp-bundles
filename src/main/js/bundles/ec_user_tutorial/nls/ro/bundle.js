define({
  "ec_user_tutorial": {
    "bundleName": "ec-utilizator-tutorialul",
    "bundleDescription": "ec-utilizator-tutorialul",
    "externalResourceDisclaimer": {
      "introduction": "Această hartă vizualizator accesează servicii externe și afișează datele. Aceste servicii externe nu sunt menținute de către Comisia Europeană și, prin urmare, nu avem nici o influență asupra disponibilitatea lor / stabilitate. Pot apărea următoarele probleme:",
      "rotatingCircle": "Un cerc de rotație în opinia hartă indică faptul că privitorul este în așteptare pentru un răspuns de la o sursă de date. Acest lucru poate fi cauzat de o probleme cu serverul sau de rețea indisponibile",
      "layerZoomLevel": "Unele surse strat să nu se afișeze la anumite niveluri de zoom. Acest lucru rezultă din restricțiile stabilite de prestatorul extern de servicii. Posibil să aveți nevoie pentru a mări sau micșora, în scopul de a afișa datele din stratul",
      "layerPanning": "Date accesate de pe un server extern poate fi situat în afara vizualizarea hartă curent. Această hartă vizualizator încearcă să identifice automat zona de acoperire. Totuși, în unele cazuri, metadatele nu sunt complete sau eronate, astfel că trebuie să pan în măsura actuale.",
      "detailInformation": "Dacă solicitați informații detaliate pentru o caracteristică selectată acest lucru va fi gestionate de o cerere specifică la server hartă. În cazul în care răspunsul la această cerere este incorect sau gol (de exemplu, din cauza unei erori de server) nici o informație poate fi afișată și o fereastră de gol va apărea.",
      "technicalSupport": "Dacă întâmpinați orice alte probleme nu ezitați să luați legătura cu asistența tehnică:"
    },
    "tutorial": {
      "welcome": "Bine ați venit la privitorului harta a European Data Portal",
      "familiarise": "Această introducere mic are scopul de a vă familiariza cu elementele și funcționalitatea hartă privitorului.",
      "navigation": "Faceți clic și țineți apăsat butonul stânga al mouse pentru a panorama pe hartă.",
      "zoom": "Aceste butoane schimba nivelul de zoom pe hartă. Alternativ, puteți utiliza rotița mouse-ului pentru a regla zoom-ul.",
      "features": "Funcționalități suplimentare sunt disponibile prin intermediul acestor butoane. Ei acționează ca și comută activa sau dezactiva funcția dată.",
      "legend": "Legenda poate fi utilizat pentru a examina straturile harta disponibile și activa sau dezactiva afișajul lor curentă pe hartă. Numele sunt derivate direct din serviciul accesat extern.",
      "transparency": "Asemenea, puteți regla transparența unui strat.",
      "featureinfo": "Unele date pot fi examinate chiar mai în detaliu. Când este activată această caracteristică, puteți să faceți clic pe harta pentru a interoga informații suplimentare.",
      "donotshow": "Nu afișa din nou acest ecran de întâmpinare.",
      "okButton": "Bine",
      "closeButton": "Aproape"
    }
  }
});