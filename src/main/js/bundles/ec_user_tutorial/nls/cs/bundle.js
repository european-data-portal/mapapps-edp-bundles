define({
  "ec_user_tutorial": {
    "bundleName": "ec-uživatel-konzultace",
    "bundleDescription": "ec-uživatel-konzultace",
    "externalResourceDisclaimer": {
      "introduction": "Tato mapa prohlížeč přistupuje externí služby a zobrazí jejich data. Tyto externí služby nejsou udržovány Evropskou komisí, a proto nemají žádný vliv na jejich dostupnosti / stabilita. Může nastat následující problémy:",
      "rotatingCircle": "Otáčející se kruh v zobrazení mapy ukazuje, že je divák čeká na odpověď ze zdroje dat, může to být způsobeno tím, nedostupném serveru nebo problémy se sítí",
      "layerZoomLevel": "Některé zdroje vrstev se nemusí zobrazovat na určitých úrovních přiblížení. To má za následek od omezení stanovených externího poskytovatele služeb. Možná budete muset přiblížit nebo oddálit aby bylo možné zobrazit data vrstvy",
      "layerPanning": "Zpřístupněné údaje z externího serveru mohou být umístěny mimo aktuální zobrazení mapy. Tato mapa prohlížeč pokusí automaticky určit pokrytou oblast. Přesto se v některých případech metadata nejsou úplné nebo chybné, takže budete muset posunout ke skutečné míře.",
      "detailInformation": "Máte-li požádat o podrobnější informace o vybraného prvku toto bude zpracována zvláštní žádosti do mapového serveru. V případě, že odpověď na tuto žádost, je poškozený nebo prázdný (například v důsledku chyby serveru) žádné informace mohou být zobrazeny a prázdné okno se zobrazí.",
      "technicalSupport": "Pokud narazíte na jakékoli jiné problémy, neváhejte se dostat do kontaktu s technickou podporou:"
    },
    "tutorial": {
      "welcome": "Vítejte na mapě divákovi European Data Portal",
      "familiarise": "Tento malý úvod má za cíl seznámit vás s prvky a funkce z perspektivy diváka.",
      "navigation": "Klikněte a držte levé tlačítko myši posouvat na mapě.",
      "zoom": "Tato tlačítka změnit úroveň zvětšení na mapě. Případně můžete použít kolečko myši pro nastavení zoomu.",
      "features": "Další funkce je k dispozici prostřednictvím těchto tlačítek. Působí jako přepínače a povolit nebo zakázat dané funkce.",
      "legend": "Legenda lze zkoumat dostupné mapové vrstvy a povolit nebo zakázat jejich aktuální zobrazení na mapě. Jména jsou přímo odvozeny z vnějšku přístupné služby.",
      "transparency": "Můžete také nastavit průhlednost vrstvy.",
      "featureinfo": "Některé údaje mohou být zkoumány i podrobněji. Je-li aktivována tato funkce, můžete kliknout na mapě na dotaz další informace.",
      "donotshow": "Nezobrazovat uvítací obrazovku znovu.",
      "okButton": "Dobře",
      "closeButton": "Zavřít"
    }
  }
});