define({
  "ec_user_tutorial": {
    "bundleName": "ec-Benutzer-Tutorial",
    "bundleDescription": "ec-Benutzer-Tutorial",
    "externalResourceDisclaimer": {
      "introduction": "Diese Map Viewer greift auf externe Dienste zu und zeigt deren Daten an. Diese externen Dienste werden nicht von der Europäischen Kommission betrieben und daher haben wir keinen Einfluss auf ihre Verfügbarkeit/Stabilität. Die folgenden Probleme können auftreten:",
      "rotatingCircle": "Ein rotierender Kreis in der Kartendarstellung zeigt an, daß der Viewer auf eine Antwort von einer Datenquelle wartet. Dies kann durch einen nicht verfügbaren Server oder Netzwerkprobleme verursacht werden",
      "layerZoomLevel": "Einige Ebenen können bei bestimmten Zoomstufen nicht angezeigt wreden. Dies ergibt sich aus Einschränkungen durch den externen Dienstleister. Möglicherweise müssen sie, um die Daten der Ebene anzuzeigen, den Kartenausschnitt vergrößern oder verkleinern",
      "layerPanning": "Daten von einem externen Server können außerhalb der aktuellen Kartenansicht liegen. Dieser Map Viewer versucht, die abgedeckte Fläche automatisch zu identifizieren. Doch in einigen Fällen sind die Metadaten nicht vollständig oder fehlerhaft, so dass sie, den aktuellen Kartenausschnitt schwenken müssen.",
      "detailInformation": "Wenn Sie ausführliche Informationen für eine ausgewählte Funktion anzufordern, wird dies durch eine spezielle Anfrage an den Kartenserver behandelt. Wenn die Antwort auf diese Anfrage fehlerhaft oder leer ist (z.B. wegen eines Serverfehlers) werden keine Informationen angezeigt und ein leeres Fenster wird angezeigt.",
      "technicalSupport": "Wenn sie auf andere Probleme stoßen zögern Sie nicht den technischen Support zu kontaktieren:"
    },
    "tutorial": {
      "welcome": "Willkommen auf des Map Viewer des European Data Portal",
      "familiarise": "Diese kleine Einführung hat das Ziel sie mit den Elementen und Funktionalität des Map Viewer vertraut zu machen.",
      "navigation": "Klicken und halten sie die linke Maustaste, um die Karte zu verschieben.",
      "zoom": "Mit diesen Tasten ändern sie den Zoomfaktor auf der Karte. Alternativ können Sie das Mausrad verwenden, um den Zoom einzustellen.",
      "features": "Zusätzliche Funktionalität wird durch diese Tasten zur Verfügung gestellt. Sie fungieren als Schalter und aktivieren oder deaktivieren die angegebene Funktionalität.",
      "legend": "Die Legende kann verwendet werden, um zu sehen, welche Kartenebenen zur Verfügung stehen und um das Anzeigen auf der Karte zu aktivieren oder deaktivieren. Die Namen werden direkt aus der von externen Service übernommen.",
      "transparency": "Sie können auch die Transparenz einer Ebene einstellen.",
      "featureinfo": "Einige Daten können näher untersucht werden. Wenn diese Funktion aktiviert ist, können sie auf die Karte klicken, um weitere Informationen abzufragen.",
      "donotshow": "Soll der Willkommensbildschirm erneut angezeigt werden.",
      "okButton": "Ok",
      "closeButton": "Schliessen"
    }
  }
});