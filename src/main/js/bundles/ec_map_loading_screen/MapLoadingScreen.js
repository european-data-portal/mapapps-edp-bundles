define(["dojo/_base/declare","dijit/_Widget",
    "dojo/query",
    "dojo/_base/window",
    "dojo/dom-construct"],
    function(declare,_Widget, query, d_win, d_construct) {

        return declare([_Widget],
        {
            createInstance: function() {
                return this;
            },

            activate: function() {
            },
            _initialiseOnLayoutAvailabe: function() {
                if (this.loadingDiv) {
                    console.warn("MapLoadingScreen: already got a loadingDiv");
                    return;
                }
                
                if (this._initialExtent &&
                        (this._initialExtent.getInitialExtent() || this._initialExtent.isFailure())) {
                    console.warn("MapLoadingScreen: already got extent");
                    return;
                }

                console.warn("MapLoadingScreen: loading screen...");
                this.i18n = this._i18n.get();
                //set a loader div on the map panel
                var containerClass = this._properties._searchElement;
                var appRootNode = this._appCtx ? this._appCtx.getApplicationRootNode() : d_win.body();
                var queryResults = query("."+containerClass, appRootNode);

                if (queryResults && queryResults.length > 0) {
                    var mapNodes = query(".dijitContentPane", queryResults[0]);
                    if (mapNodes && mapNodes.length > 0) {
                        this._appendLoadingDiv(mapNodes[0]);
                    }
                    else {
                        console.warn("MapLoadingScreen: could not find ."+containerClass + " .dijitContentPane");
                    }
                }
                else {
                    console.warn("MapLoadingScreen: could not find ."+containerClass);
                }  
            },
            _appendLoadingDiv: function(targetElem) {
                this.mapNode = targetElem;
                this.loadingDiv = d_construct.create("div",
                    {
                        'class': "ec_map_loading",
                        innerHTML: "<div class=\"ec_ajax_loader\"></div><div>"+this.i18n.messages.initializingMap+"</div>"
                    },
                    this.mapNode);
            },

            /*
             * act on events to remove the loading container. otherwise the map
             * cannot initialize
             */
            handleEvent : function(event){
                var topic = event.getTopic();
                
                if (topic === "ct/template/RENDERED") {
                    console.warn("MapLoadingScreen: layout available, rendering!");
                    this._initialiseOnLayoutAvailabe();
                    return;
                }
                
                if (!this.loadingDiv) {
                    return;
                }

                console.warn("MapLoadingScreen: remove div: "+topic);

                if (topic === "ec/odp/extent/APPLIED") {
                    d_construct.destroy(this.loadingDiv);
                }

                else if (topic === "ec/odp/ON_EXTENT_ERROR") {
                    //TODO we might consider NOT loading the map, and provide
                    //another message to the user. see issue #45
                    d_construct.destroy(this.loadingDiv);
                }
            }
            
        });
    });