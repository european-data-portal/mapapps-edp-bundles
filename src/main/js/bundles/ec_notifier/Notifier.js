define([
    'dojo/_base/declare',
    'system/log/LogService',
    './LogEntry'
], function (declare, LogService, LogEntry) {
    return declare([LogService], {
        activate: function () {
            this._logReaderService = this._delegate._logReaderService;
            return this.inherited(arguments);
        },
        deactivate: function () {
            this._logReaderService = null;
            return this.inherited(arguments);
        },
        fatal: function () {
            var args = Array.prototype.slice.call(arguments);
            args = [LogEntry.levels.LOG_FATAL].concat(args);
            return this._log.apply(this, args);
        },
        _log: function (level, shortInfo, message, error, sr) {

            if (level !== LogEntry.levels.LOG_FATAL) {
                return this.inherited(arguments);
            }
            var options;
            if (typeof shortInfo === 'object') {
                options = shortInfo;
            } else {
                options = {
                    shortInfo: shortInfo,
                    message: message,
                    error: error,
                    serviceReference: sr
                };
            }
            options.level = level;
            options.bundle = this._usingBundle;
            this._logReaderService.addLogMessage(new LogEntry(options));
        }
    });
});