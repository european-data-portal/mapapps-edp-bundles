define(["dojo/_base/declare", "ct/tools/Tool", "dojo/_base/array"], function(
  declare,
  Tool,
  array
) {
  return declare([Tool], {
    resourceProvider: null,
    _hasQueryableLayers: false,

    activate: function() {
      console.log("FeatureInfoToggler: activated");
      this.inherited(arguments);

      if (this.resourceProvider && this.resourceProvider.getCKANResource()) {
        this._analyzeService(
          this.resourceProvider.getCKANResource().getPayload()
        );
      }

      this._updateButtonState();
    },
    deactivate: function() {
      console.log("FeatureInfoToggler: deactivated");
      this.inherited(arguments);
    },

    handleEvent: function(event) {
      switch (event.getTopic()) {
        case "ec/odp/NEW_WMS_LAYER":
          console.log("FeatureInfoToggler: new WMS layer event");
          this._analyzeService(event.getProperties());
          this._updateButtonState();
          break;
      }
    },

    _analyzeService: function(service) {
      if (service.declaredClass === "esri.layers.WMSLayer") {
        if (service.layerInfos && service.layerInfos.length > 0) {
          this._hasQueryableLayers = array.some(service.layerInfos, function(l) {
            return l.queryable;
          });
        }
      } else if (service._serviceType === "GeoJSON") {
        this._hasQueryableLayers = true;
      }
    },

    _updateButtonState: function() {
      this.set("enabled", this._hasQueryableLayers);
    }
  });
});
