define([
  "dojo/_base/declare",
  "dijit/_Widget",
  "dojo/_base/connect",
  "dojo/_base/lang",
  "ct/mapping/edit/Highlighter"
], function(declare, _Widget, d_connect, d_lang, Highlighter) {
  return declare([_Widget], {
    active: true,

    createInstance: function() {
      return this;
    },

    activate: function() {
      d_connect.connect(
        this.contentViewer,
        "showContentInfo",
        this,
        this.zoomToFeature
      );
      this.eventService.postEvent("ct/tool/DEACTIVATE/graphicinfotool");
    },

    zoomToFeature: function(content) {
      if (this.mobileChecker.isMobile()) {
        this.geom = content.geometry;
        content.mobile = true;
        var adjustedGeometry = this.adjustGeometry(
          content.geometry,
          this.mapState.getExtent()
        );
        this.mapState.centerAt(adjustedGeometry);
      }
    },

    adjustGeometry: function(geom, extent) {
      var delta = (extent.ymax - extent.ymin) / 4;
      var clone = d_lang.clone(geom);
      clone.y = geom.y - delta;
      return clone;
    },

    windowOpened: function() {
      if (this.active && this.geom) {
        this.createHighlighter(this.geom);
      }
    },

    windowClosed: function() {
      if (this._highlighter) {
        this._highlighter.clear();
      }
    },

    createHighlighter: function(geom) {
      if (!this._highlighter) {
        var highlighterSettings = this._properties.highlighter;
        var icon = highlighterSettings.icon;
        var url = this._componentContext.getResourceURL(icon.url);
        this._highlighter = new Highlighter({
          mapModel: this.mapModel,
          symbol: {
            url: url,
            width: icon.width,
            height: icon.height,
            xOffset: icon.xOffset,
            yOffset: icon.yOffset
          }
        });
      } else {
        this._highlighter.clear();
      }
      this._highlighter.highlight(geom);
      this._highlighter.bounce();
    },

    show: function() {
      this._activate();
    },

    hide: function() {
      this._deactivate();
    },

    _activate: function() {
      this.active = true;
      this.ctGraphicInfo.enable();
      this.eventService.postEvent("ct/tool/ACTIVATE/graphicinfotool");
    },

    _deactivate: function() {
      this.active = false;
      this.geom = null;
      this.ctGraphicInfo.hideContent();
      this.ctGraphicInfo.disable();
      this.eventService.postEvent("ct/tool/DEACTIVATE/graphicinfotool");
    }
  });
});
