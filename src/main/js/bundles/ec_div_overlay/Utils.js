define([
  "dojo/_base/declare",
  "dojo/dom-construct",
  "dojo/query",
  "dojo/_base/window"
], function(declare, domConstruct, query, win) {
  return declare([], {
    overlayCount: 0,

    addGlobalOverlay: function() {
      this.overlayCount++;

      console.info("overlay added. count: " + this.overlayCount);

      var overlay = query(".ec-overlay");
      if (overlay && overlay.length > 0) {
        console.log("got an overlay!");
        return true;
      }

      domConstruct.create(
        "div",
        {
          class: "ec-overlay",
          style: "top: 0;bottom: 0; left: 0;right: 0;position: fixed;"
        },
        win.body()
      );
    },

    removeGlobalOverlay: function() {
      if (this.overlayCount === 0) {
        console.info("Could not remove overlay. None existing.");
        return false;
      }

      this.overlayCount--;

      console.info("overlay removed. count: " + this.overlayCount);

      if (this.overlayCount === 0) {
        query(".ec-overlay").forEach(domConstruct.destroy);
      }

      return true;
    }
  });
});
